<?php 
$arr = array(array('image'=>"team/ankit.jpg",'name'=>"Ankit Rawat",'desig'=>"Strategy and Business Development"),
       
        array('image'=>"team/yogesh.jpg",'name'=>"Yogesh Singh", 'desig'=>"Technology"),
      
        );
shuffle($arr);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css" />
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700'
	rel='stylesheet' type='text/css' />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<title>Form Fly - About us</title>
</head>
<body>
	<div id="header3">
		<?php include('header.php'); ?>
	</div>
	<!--end of header--->

	<div id="container">
		<div class="center">
			<div id="vision">
				<h2 class="head">Leadership</h2>
				<p>"We believe the monotonous tasks are best left for machines,
					freeing the human mind for what it is really meant to do: dream,
					create and inspire!"</p>
			</div>
			<!---end of vision--->

			<div id="team">
				<?php foreach ($arr as $id=>$teamMember){?>
				<div
					class="<?php if(($id+1)%2 == 0) echo "right"; else echo "left";?>">
					<img src="<?php  echo $teamMember['image'];?>" alt="" /> <span
						class="spn1"><?php  echo $teamMember['name'];?> </span> <span
						class="spn2"><?php echo $teamMember['desig'];?> </span>
					<!--  <p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempor, mauris vitae lobortis commodo, eros enim congue elit, ut consectetur nibh eros at augue.
					</p>-->
				</div>
				<?php 
				if(($id+1)%2 == 0) echo '<div class="clear"></div>';
                }?>
                <div class="clear"></div>
			</div>
			<!---end of team--->

		</div>
		<!---end of container center--->
	</div>
	<!---end of container--->

	<div id="footer">
		<?php include("footer.php"); ?>
	</div>
	<!---end of footer--->
</body>

</html>
