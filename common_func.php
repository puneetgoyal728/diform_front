<?php
include_once("config.php");
include_once ('db_conn.php');
include_once ('resizeImage.php');
include_once 'scrypt.php';
require $phpmailer_path;
require $smtp_path;
//insert into tbl_snippet_process_data (tsm_id,tcj_id)
// select  tsm.tsm_id ,tcj.tcj_id from tbl_snippets_master
// tsm join tbl_job_details tjd join tbl_template_master
// ttm join tbl_crowd_jobs tcj  on tsm.job_id = tjd.job_id
// and tcj.job_type=ttm.field_type  and tsm.page_no=tjd.page_no
// and tjd.form_page_no=ttm.page_no and tjd.form_id=ttm.form_id
// and tsm.field_id=ttm.field_id where ttm.field_id!=0;

function createthumb($savePath,$location)
{
    global $thumnails_path;
    //     list($width, $height, $type, $attr) = getimagesize($savePath);
    $img=explode("/",$savePath);
    $index = count($img) - 1;
    //     if($width<$height){
    //         $nheight=640;
    //         $nwidth=($width/$height)*$nheight;
    //         $tmp=imagecreatetruecolor($nwidth, $nheight);
    //         $image = imagecreatefromjpeg($savePath);
    //         imagecopyresampled($tmp,$image,0,0,0,0, $nwidth, $nheight, $width, $height);
    //         imagejpeg($tmp,$location.$img[$index]);
    //     }
    //     echo $savePath.$location.$img[$index];exit;
    smart_resize_image($savePath,null,0,100,true,$location.$img[$index],false,true,50);
}

function csvListTSMUser($tcm_id,$tcj_id=null){
    global $conn;
    //TODO:implement memcache for list of TSM ids
    $allTsmForUser = "select distinct tsm_id
    from tbl_snippet_process_data
    where".(($tcj_id!==null)?" tcj_id=$tcj_id and":"")." tcm_id=$tcm_id and CURDATE() - interval '2' day < Recservedate";
//     echo $allTsmForUser;exit;
    $allTsmArr = array();
    $allTsmForUserRes = $conn->dbh->query($allTsmForUser);
    $allTsmForUserResult = $allTsmForUserRes->fetchAll(PDO::FETCH_ASSOC);
    foreach($allTsmForUserResult as $val){
        array_push($allTsmArr, $val['tsm_id']);
    }
    $returnVal = array();
    $obtainJobQry = "";
    if(count($allTsmArr)>0){
        $notInclude = implode(',', $allTsmArr);
    }else{
        $notInclude = 0;
    }
    return $notInclude;
}


function serveimage($tcj_id, $tcm_id)
{
    global $conn,$displayToCrowd;
    
    $notInclude = csvListTSMUser($tcm_id,$tcj_id);
    
    $obtainJobQry = "SELECT tspd.tspd_id tspd_id, tspd.tsm_id tsm_id, tspd.tcj_id tcj_id,
    tsm.job_id job_id, tsm.page_no page_no, tsm.field_id field_id, tjd.form_page_no form_page_no, tjd.form_id
    from tbl_snippet_process_data tspd join tbl_snippets_master tsm join tbl_job_details tjd
    on tspd.tsm_id = tsm.tsm_id and tsm.job_id = tjd.job_id and tsm.page_no=tjd.page_no where
    tspd.tcj_id=$tcj_id and tspd.tspd_status=0 and tspd.tsm_id not in (".$notInclude.") 
    group by tspd.tsm_id order by tsm.field_id, tjd.form_page_no, tjd.form_id limit $displayToCrowd";
    $obtainJobRes = $conn->dbh->query($obtainJobQry);
    $jobResult = $obtainJobRes->fetchAll(PDO::FETCH_ASSOC);
//     print_r($jobResult);exit;
    if(count($jobResult) >= 1)
    {
        $instructQry = "select ttm.instructions instruct, ttm.field_details fields, ttm.dict_status sts 
        from tbl_template_master ttm where ttm.page_no = ".$jobResult[0]['form_page_no']." 
        and ttm.form_id = ".$jobResult[0]['form_id']." and ttm.field_id = ".$jobResult[0]['field_id'];
        $instructRes = $conn->dbh->query($instructQry);
        $instructResult = $instructRes->fetch(PDO::FETCH_NUM);
        $returnVal['desc'] = $instructResult[0];
        $returnVal['tasks'] = array();
        if($instructResult[2]==1){
            $returnVal['auto'] = getUniqueValuesYet($jobResult[0]['form_id'],$jobResult[0]['field_id'],$jobResult[0]['form_page_no']);
        }else{
            $returnVal['auto'] = null;
        }
        foreach ($jobResult as $jobs){
            if($jobs['form_id']!=$jobResult[0]['form_id'] || $jobs['form_page_no']!=$jobResult[0]['form_page_no'] || $jobs['field_id']!=$jobResult[0]['field_id']){
                break;
            }
            $job_id = $jobs['job_id'];
            $tsm_id=$jobs['tsm_id'];
            $serveImageName = create_temp_image($job_id,$tsm_id.".jpg");
            $updateDataQry = "update tbl_snippet_process_data set
            serve_name = '".$serveImageName."', tcm_id = $tcm_id, tspd_status =1 ,
            Recservedate=CURDATE() ,Recservetime=CURTIME()
            where tspd_id =".$jobs['tspd_id']." and tspd_status =0";
            $countRows = $conn->dbh->exec($updateDataQry);
            if($countRows==0){
                return null;
            }
            $tasksArr["serveImg"]=$serveImageName;
            $tasksArr["jobid"]=$job_id;
            $tasksArr['tspdid'] = $jobs['tspd_id'];
            if(strcmp($tcj_id,"2")==0 || strcmp($tcj_id,"3")==0)
            {
                    if($instructResult[1]!=NULL && $instructResult[1]!=""){
                        $optionsForField=json_decode($instructResult[1]);
                        if(is_array($optionsForField)){
                            $tasksArr['isWithoptions'] = true;
                            $tasksArr['options'] = $optionsForField;
                        }
                    }else{
                        $tasksArr['isWithoptions'] = false;
                    }
            }
            array_push($returnVal['tasks'],$tasksArr);
        }
        return $returnVal;
    }else{
        return null;
    }
}

function createRandom($len){
    $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $randChars = substr(str_shuffle($chars),10,$len);
    return $randChars;
}

function create_temp_image($jobid,$img)
{
    global $snippets_path;
    $gen='temp/'.createRandom(10).date("Ymd").time().'.jpg';
    copy($snippets_path.$jobid."/".$img, 'temp/'.$img);
    rename('temp/'.$img, $gen);
    return $gen;
}

function getNewTask($tcj_id,$tcm_id) {
    global $key;
    $sendValues = serveimage($tcj_id,$tcm_id);
    if($sendValues == null){
        $sendback["status"] = false;
    }else{
        $sendback["status"] = true;
        $salt=createRandom(10);
        $skey=$key.$salt;
        $text=$tcm_id."||".$tcj_id;
        $hash=encode($salt,$text);
        $sendback["hash"]=$hash;
        $sendback["salt"]=$salt;
        $sendback["desc"] = $sendValues["desc"];
        $sendback["tasks"] = $sendValues["tasks"];
        $sendback["type"]=$tcj_id;
        $sendback["auto"] = $sendValues["auto"];
    }
    return $sendback;
}

function getNewSquadrunTask($tcj_id,$requestedNum){
    global $conn,$basePath;
    $returnArr = array();
    $taskArr = array();
    $taskQry = "select tst.tsm_id tsm_id, tst.serve_copies_no serve_copies_no,
    tst.tst_trxn_id tst_trxn_id, tst.tst_status tst_status, tsm.job_id job_id
    from tbl_squadrun_tasks tst join tbl_snippets_master tsm
    on tst.tsm_id = tsm.tsm_id where tst.tst_status = 0 and tst.tcj_id = $tcj_id limit $requestedNum";
    foreach ($conn->dbh->query($taskQry) as $resultData){
        $serveImageName = create_temp_image($resultData['job_id'],$resultData['tsm_id'].".jpg");
        $updateStatusQuery = $conn->dbh->prepare("update tbl_squadrun_tasks
                set tst_status = 1, img_serve_name = '".$serveImageName."'
                where tsm_id = '".$resultData['tsm_id']."' and tst_trxn_id = '".$resultData['tst_trxn_id']."' and tst_status = 0");
        $updateStatusQuery->execute();
        $count = $updateStatusQuery->rowCount();
        //         echo $count;exit;
        if($count==1){
            $instructQry = "select ttm.instructions instruct from tbl_snippets_master tsm
            join tbl_job_details tjd join tbl_template_master ttm on tsm.page_no=tjd.page_no
            and tjd.form_page_no=ttm.page_no
            and tjd.form_id=ttm.form_id and tsm.field_id=ttm.field_id where tsm.tsm_id=".$resultData['tsm_id'];
            $instructRes = $conn->dbh->query($instructQry);
            $instructResult = $instructRes->fetch(PDO::FETCH_NUM);
            $optionsForField = array();
            $isWithOptions = false;
            if(strcmp($tcj_id,"2")==0 || strcmp($tcj_id,"3")==0)
            {
                $optionsQuery = "select ttm.field_details fields from tbl_snippets_master tsm
                join tbl_job_details tjd join tbl_template_master ttm on tsm.page_no=tjd.page_no
                and tjd.form_page_no=ttm.page_no
                and tjd.form_id=ttm.form_id and tsm.field_id=ttm.field_id where tsm.tsm_id=".$resultData['tsm_id'];
                //          echo $optionsQuery;exit;
                $optionsRes = $conn->dbh->query($optionsQuery);
                $optionsResult = $optionsRes->fetchAll(PDO::FETCH_ASSOC);
                if(count($optionsResult)==1){
                    $field_details=$optionsResult[0]['fields'];
                    $optionsForField=json_decode($field_details);
                    $isWithOptions = true;
                }
            }
            array_push($taskArr,array('task_id'=>$resultData['tst_trxn_id'],
                    'no_copies'=>$resultData['serve_copies_no'],'url'=>($basePath.$serveImageName),
                    'with_options'=>$isWithOptions, 'options'=>$optionsForField, 'desc'=>$instructResult[0]));
        }else{
            @unlink($serveImageName);
        }
    }
    if(count($taskArr)>0){
        $returnArr['status'] = 1;
        $returnArr['data'] = $taskArr;
        $returnArr['served_no'] = count($taskArr);
        $salt=createRandom(10);
        $returnArr['salt'] = $salt;
        $key = $squadTaskFetchKey.$salt;
        $data = $returnArr['status']."||".$returnArr['served_no'];
        $hash = hash_hmac ( "sha256" , $data , $key);
        $returnArr['hash'] = $hash;
        $returnArr['msg'] = "successful";
    }else{
        $returnArr['status'] = 0;
        $returnArr['msg'] = "no more jobs left";
    }
    return $returnArr;

}

function encode($salt,$text)
{
    global $key;
    $skey=$key.$salt;
    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
    $hash=base64_encode(trim(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $skey, $text, MCRYPT_MODE_ECB, $iv)));
    return $hash;
}
function decode($salt,$hash)
{
    global $key;
    $skey=$key.$salt;
    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
    $text = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $skey, base64_decode($hash), MCRYPT_MODE_ECB, $iv));
    return $text;
}

function sendmail($to,$from,$message,$subject)
{
    global $mailids;
    //Create a new PHPMailer instance
    $mail = new PHPMailer();
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug  = 0;
    //Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';
    //Set the hostname of the mail server
    $mail->Host       = 'smtp.zoho.com';
    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port       = 587;
    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'tls';
    //Whether to use SMTP authentication
    $mail->SMTPAuth   = true;
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username   = $mailids[$from]['email'];
    //Password to use for SMTP authentication
    $mail->Password   = $mailids[$from]['pass'];
    //Set who the message is to be sent from
    $mail->SetFrom($mailids[$from]['email'], $mailids[$from]['name']);
    //Set an alternative reply-to address
    //$mail->AddReplyTo('replyto@example.com','First Last');
    //Set who the message is to be sent to
    $mail->AddAddress($to, '');
    //Set the subject line
    $mail->Subject = $subject;
    //Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
    $mail->MsgHTML($message);
    //Replace the plain text body with one created manually
    $mail->AltBody = '';
    //Attach an image file
    //$mail->AddAttachment('images/phpmailer_mini.gif');
    $mail->ContentType = 'text/html';
    //Send the message, check for errors
    if(!$mail->Send()) {
        return false;
    } else {
        return true;
    }
}

function getExtension ($fileName) {
    $filetype=explode(".",$fileName);
    $indexOfExtn = count($filetype) - 1;
    $fileExtn = strtolower($filetype[$indexOfExtn]);
    return $fileExtn;

}

function getfilename ($fileName) {
    $filetype=strrev($fileName);
    $filetype=explode(".",$filetype,2);
    return strrev($filetype[1]);
}

function auto_version($file)
{
    global $staticPath;
    if(strpos($file, '/') !== 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file))
        return $file;

    $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] ."/". $file);
    return $staticPath.preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
}

function curl_post($url,$post,$options)
{
    //     print_r($options);exit;
    $defaults = array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 0,
            CURLOPT_TIMEOUT => 4,
            CURLOPT_POSTFIELDS => json_encode($post)
    );

    $ch = curl_init();
    //     print_r (($options + $defaults));exit;
    curl_setopt_array($ch, ($options + $defaults));
    $result = curl_exec($ch);
    $response = curl_getinfo( $ch );
    //     var_dump($result);
    //     var_dump($response);
    if( ! $result )
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return $result;
}


function getUniqueValuesYet($form_id,$field_id,$form_page_no){
    global $conn;
    $uniqValQry = "select distinct tsm.verified_data data 
    from tbl_snippets_master tsm join tbl_job_details tjd 
    on tjd.page_no = tsm.page_no and tjd.job_id = tsm.job_id 
    where tsm.field_id = $field_id and tjd.form_id = $form_id and tjd.form_page_no =".$form_page_no." and tsm.verified_data is not null";
    $uniqRes = $conn->dbh->query($uniqValQry);
    $uniqResult = $uniqRes->fetchAll(PDO::FETCH_NUM);
    $returnArr = array();
    foreach ($uniqResult as $dictdata){
        array_push($returnArr, $dictdata[0]);
    }
    return $returnArr;
}

function createsession($uid,$flag,$emailid){
    $hash = Password::hash($uid);
    $_SESSION["username"] = $emailid;
    $_SESSION["uid"]= $uid;
    if($flag==1){
        setcookie("hash", $hash, time()+(86400*30), "/", "",  0);
        setcookie("uid", $uid, time()+(86400*30), "/", "",  0);
        setcookie("username", $emailid, time()+(86400*30), "/", "",  0);
    }
    else{
        setcookie("hash", $hash, time()+(86400), "/", "",  0);
        setcookie("uid", $uid, time()+(86400), "/", "",  0);
        setcookie("username", $emailid, time()+(86400), "/", "",  0);
    }
}

function checksession(){
if(!isset($_SESSION["uid"])){
    if( isset($_COOKIE["uid"])){
        if(Password::check($_COOKIE["uid"],$_COOKIE["hash"]) )
        {
            $_SESSION["uid"]=$_COOKIE["uid"];
            $_SESSION["username"]=$_COOKIE["username"];
        }
        else {
            header("location:index.php");
        }
    }
    else {
        header("location:index.php");
    }
}
}

function valid_email($str)
 {
  return ( ! preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $str)) ? FALSE : TRUE;
 }
 
 function alpha_dash($str)
 {
  return ( ! preg_match("/^([-a-z0-9_-])+$/i", $str)) ? FALSE : TRUE;
 }
 
 function min_length($str, $val)
 {
  if (preg_match("/[^0-9]/", $val))
  {
   return FALSE;
  }

  if (function_exists('mb_strlen'))
  {
   return (mb_strlen($str) < $val) ? FALSE : TRUE;
  }

  return (strlen($str) < $val) ? FALSE : TRUE;
 }
 function alpha($str)
 {
  return ( ! preg_match("/^([a-z])+$/i", $str)) ? FALSE : TRUE;
 }
?>
