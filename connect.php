<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
include_once 'common_func.php';
include_once 'scrypt.php';
checksession();
$j= $_POST['job'];
$id= $_POST['id'];


//print_r($_POST);exit;
function getMultiPagePref($id){
    global $conn;
    $rangeQry = "select type,rangefrom,rangeto from tbl_template_details where form_id =$id";
    $rangeRes = $conn->dbh->query($rangeQry);
    $rangeResult = $rangeRes->fetch(PDO::FETCH_ASSOC);
    if($rangeResult){
        if($rangeResult['type']==0){
            return 0;
        }else{
            return $rangeResult['rangefrom']."|".$rangeResult['rangeto'];
        }
    }else{
        return 0;
    }
}

function getTotalFormPageNo($form_id) {
    global $conn;
    $pages = 0;
    foreach( $conn->dbh->query("SELECT count(page_no) as 'maxform'
            FROM tbl_image_master where form_id=$form_id and active = 1 and user_id = ".$_SESSION['uid'])  as $row) {
            $pages =$row['maxform'];
    }
    return $pages;
}

function fillJobDetailsFromUpload($jobId, $form_id){
    global $conn, $jobs_path;
    $formPages = getTotalFormPageNo($form_id);
    $jobDir = $jobs_path.$jobId.'/';
    $jobDataQuery = "select upload_name from tbl_job_upload_details where job_id = $jobId order by upload_order;";
    //    echo $jobDataQuery;exit;
    $jobDataRes = $conn->dbh->query($jobDataQuery);
    $jobDataResult = $jobDataRes->fetchAll(PDO::FETCH_ASSOC);
    $useFilesForTemplate = getMultiPagePref($form_id);
    $currPageNo = 1;
    $countAsFormPage = 0;
    foreach ($jobDataResult as $eachUplod) {
        $totalPages = 1;
        $filePath = $jobDir.$eachUplod['upload_name'];
        $extn = getExtension($eachUplod['upload_name']);
        if(strcmp($extn,"tiff")==0 || strcmp($extn,"tif")==0){
            $totalPages = trim(exec("identify -format \"%n\" '".$filePath."'"));
        }
        if(strcmp($extn,"pdf")==0 ){
            $totalPages = trim(exec("pdfinfo '".$filePath."' | grep Pages: | awk '{print $2}'"));
        }
        if($totalPages>1 || strcmp($extn,"pdf")==0 ){
            //             echo "pdfinfo '".$filePath."' | grep Pages: | awk '{print $2}'<br />";
            $filePath = str_replace(" ","\ ",$filePath);
            $pdfPages = $totalPages;
            if($useFilesForTemplate == 0){
                
                $execCmd = "convert -quality 00 -density 300x300 -scene $currPageNo ".($filePath)." ".$jobDir.$_SESSION['uid']."_".$jobId."_%d.png";
                exec($execCmd);
            }else{
                $arrFormSettings = explode('|', $useFilesForTemplate);
                $from = $arrFormSettings[0]-1;
                $to = $arrFormSettings[1]-1;
                $totalPages = $to - $from + 1;
                if($pdfPages>=$arrFormSettings[1]){
                    $execCmd = "convert -quality 00 -density 300x300 -scene $currPageNo ".($filePath)."[".$from."-".$to."] ".$jobDir.$_SESSION['uid']."_".$jobId."_%d.png";
                }else{
                    $execCmd = "convert -quality 00 -density 300x300 -scene $currPageNo ".($filePath)."[".$from."-".($pdfPages-1)."] ".$jobDir.$_SESSION['uid']."_".$jobId."_%d.png";
                }
                exec($execCmd);
            }
            $i = 0;
            for (; $i <$totalPages;$i++){
                $countAsFormPage = (($countAsFormPage)%$formPages == 0?1:($countAsFormPage+1));
                if($i>=$pdfPages){
                    continue;
                }
                $conn->dbh->query("INSERT into tbl_job_details
                        (job_id, page_no, upload_name, saved_name,form_page_no, processed, form_id,active)
                        values ($jobId, ".($currPageNo+$i).",'".$eachUplod['upload_name']."',
                        '".$_SESSION['uid']."_".$jobId."_".($currPageNo+$i).".png', ".($countAsFormPage).",0, $form_id,1)");

            }
            $currPageNo += $i;
        }else{
            $countAsFormPage = (($countAsFormPage)%$formPages == 0?1:($countAsFormPage+1));
            //             $extension = getExtension($eachUplod['upload_name']);
            //             $dest = $_SESSION['uid']."_".$jobId."_".($currPageNo).".".$extension;
            //             rename ($eachUplod['upload_name'] , $dest);
            $conn->dbh->query("INSERT into tbl_job_details
                    (job_id, page_no, upload_name, saved_name,form_page_no, processed, form_id,active)
                    values ($jobId, ".($currPageNo).",'".$eachUplod['upload_name']."',
                    '".$eachUplod['upload_name']."', ".($countAsFormPage).",0, $form_id,1)");
            $currPageNo++;
        }
    }

    $updateTPgForTJM = "update tbl_job_master set no_pages = ".($currPageNo - 1)." where job_id=$jobId";
    $conn->dbh->query($updateTPgForTJM);
}

try{
    $uid=$_SESSION["uid"];
    fillJobDetailsFromUpload($j,$id);
    $query="update tbl_job_master set form_id = :formid where job_id = :jobid and usr_id= :uid";
    $sth = $conn->dbh->prepare($query);
    $sth->execute(array(':formid'=> $id,':jobid'=> $j,':uid'=>$uid)) or die(var_dump($sth->errorInfo()));
    $updatequery="update tbl_template_details set lock_status = 1 where form_id = :formid";
    $sth = $conn->dbh->prepare($updatequery);
    $sth->execute(array(':formid'=> $id)) or die(var_dump($sth->errorInfo()));
     $hash = Password::hash($uid);
//     $ip = $_SERVER['REMOTE_ADDR'];
//     $url=$dashboard_basepath.'usersC/addUserActivityLog/'.urlencode($hash).'/'.$uid.'/'.$ip.'/jobandtemplateconnected/jobid_'.$j.'_form_id_'.$id;
    // $rt=file_get_contents($url);
    $command = escapeshellcmd($python_code_path.'driverCode.py '.$j);
    $output = shell_exec($command." > /dev/null 2>/dev/null &");
    echo 1;exit;
}
catch (PDOException $e) {
    //     print "Error!: " . $e->getMessage() . "<br/>";
    //     die();
    echo 2;exit;
}


?>