<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'/>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<title>Form Fly - Contact us</title>
</head>
<body>
	<div id="header3">
		<?php include('header.php'); ?>
	</div><!--end of header--->
	
	<div id="contact">
		<div class="center">
			<div id="vision">
				<h2 class="head">We'd love to hear from you !</h2>			
				<p style="font-style:italic; font-size:18px;">
					Want to know more, got feedback or just want to strike a converstation.
				</p>
			</div><!---end of vision--->
			
			<div class="left">
				<ul>
					<li>
						<p>PHONE</p>
						<span>+91-87696-21346</span>
												
						
					</li>
					
					<li>
						<p>Email</p>
						<span>contact@11techsquare.com</span>						
					</li>
				</ul>
			</div>
			
			<div class="right">
				<form>
					<ul>
						<li>
							<input type="text" placeholder="Name *" class="inp"/>
						</li>
						
						<li>
							<input type="text" placeholder="Email *" class="inp"/>
						</li>
						
						<li>
							<input type="text" placeholder="Contact No." class="inp"/>
						</li>
						
						<li>
							<textarea placeholder="Message" class="textarea"></textarea>
						</li>
						
						<li>
							<input type="submit" value="Get in touch" class="submit"/>
						</li>
					</ul>
				</form>
			</div>
			<div class="clear"></div>
		</div><!---end of contact center--->
	</div><!---end of contact--->
	
	<div id="footer">
		<?php include("footer.php"); ?>
	</div><!---end of footer--->
</body>

</html>
