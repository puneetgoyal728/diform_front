<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
include_once 'common_func.php';
include_once('referenceArrays.php');
//checksession();
$formid= $_GET['g'];
if(isset($_GET['j']))
    $jobid=$_GET['j'];
else
    $jobid=0;

foreach( $conn->dbh->query("SELECT lock_status status FROM tbl_template_details where form_id = $formid " ) as $row)
     {
         if($row['status']==1)
         header("location:template.php");
     }
    
if(!isset($_GET["p"]))
{
 foreach( $conn->dbh->query("SELECT data_img_name ,min(page_no) page FROM tbl_image_master where form_id = $formid and active=1" ) as $row)
     {
         $wert =$row['page'];
         $l=$row['data_img_name'];
     }
}
else 
{
 $wert=$_GET["p"];
 foreach( $conn->dbh->query("SELECT data_img_name  FROM tbl_image_master where form_id = $formid and page_no=$wert" ) as $row)
     {
         $l=$row['data_img_name'];
     }
}

list($width, $height, $type, $attr) = getimagesize($l);
$pages[]=0;
$index=0;
$pageButtons = "<div class='center' style='padding:20px 0px 20px 0px; width:600px; text-align:right;'>";
foreach( $conn->dbh->query("SELECT *  FROM tbl_image_master where  form_id=$formid and active = 1") as $arr)
{
    $pages[$index]=$arr['page_no'];
    ++$index;
}

if($index==1)
{
    $pageButtons .= "<a class = 'button' style='opacity: 0.3; '>prev</a>";
    $pageButtons .= "<a class = 'button' style='opacity: 0.3; '>next</a>";

}
else {

    for($df=0;$df<$index;++$df)
    {
        if($pages[$df]==$wert)
        {
            if($df==0)
            {
                $next=$pages[$df+1];
                $prev=$pages[$df];
                $pageButtons .= "<a class = 'button' style='opacity: 0.3; ' >prev</a>";
                $pageButtons .= "<a class = 'button' href=createtemplate.php?g=".$formid."&p=".$next."&j=".$jobid.">next</a>";
            }
            else if($df==$index-1)
            {
                $next=$pages[$df];
                $prev=$pages[$df-1];
                $pageButtons .= "<a class = 'button' href=createtemplate.php?g=".$formid."&p=".$prev."&j=".$jobid.">prev</a>";
                $pageButtons .= "<a class = 'button' style='opacity: 0.3;'>next</a>";
            }
            else
            {
                $next=$pages[$df+1];
                $prev=$pages[$df-1];
                $pageButtons .= "<a class = 'button' href=createtemplate.php?g=".$formid."&p=".$prev."&j=".$jobid.">prev</a>";
                $pageButtons .= "<a class = 'button' href=createtemplate.php?g=".$formid."&p=".$next."&j=".$jobid.">next</a>";
            }
        }

    }
}

$pageButtons .= '<a class = "button" id ="finishbutton" onclick="sendtemp('.$jobid.','.$formid.');";>finish</a>';
$pageButtons .= "</div>";
//previous data from databaase
$ark=array();
foreach(  $conn->dbh->query("SELECT * FROM tbl_template_master where form_id =$formid and page_no=$wert") as $ro)
{

 $data=array("id"=>$ro['field_id'],"coordinates"=>$ro['field_cord'],"name"=>$ro['field_name'],"type"=>$ro['field_type'],"sub_type"=>$ro['sub_field_type'],"details"=>$ro['field_details'],"instructions"=>$ro['instructions'],"istable"=>$ro['istable']); 
 array_push($ark, $data)  ;
}
$currentHead = "TEMPLATE";


?>
<!doctype html>
<html lang="en" >
<head>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="<?php echo auto_version('/css/default.css');?>" />
<title>Movable and Re-sizable Raphael JS Shape</title>
</head>
<style>
#left_area {
    background-color: white;
    border: 1px #FB8C2D solid;
    width: 290px;
    position: fixed;
    right: 0;
    padding: 0px 0px 20px 0px;
    top: 208px;
    border-right: 0px;
    border-top: 0px;
}

.multiOp0 {
    width: 212px;
}

.tabtype{
    cursor:pointer;
    padding:13px;
    float: left;
    width: 110px;
    border: none;
    border-bottom: 1px #FB8C2D solid;
    background-color:white;
    color: black;
}

#fieldbtn {
    position: relative;
    left: -1px;
}
#fieldbtn.selected {
    left: 0px;
}
.selected{
    border-top: 1px #FB8C2D solid;
    border-left: 1px #FB8C2D solid;
    border-right: 1px #FB8C2D solid;
    border-bottom: 0px;
    margin-left: -1px;
    color: black;
    background-color:white;
    margin-top: 0px;
    left: 0px;
    
}

.result_pane {
    margin-left: 10px;
    margin-top: 10px;
    height: 350px;
    overflow-y: auto;
    width: 280px;
}

</style>
<body>
<?php include('jobsheader.php');  echo $pageButtons;?>
<div id="paper"></div>
<div id="left_area">
<!-- <button id="fieldbtn">field</button><button id="tablebtn" >table</button> -->
<div id='fieldbtn' class="tabtype selected" >Field</div><div class="tabtype" id='tablebtn'>Table</div><div style="clear: both;"></div>
<div id="result" class="result_pane"></div></div>
<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/raphael.min.js"></script>
<script src="<?php echo auto_version('/js/createtemplate.js');?>"></script>
<script src="<?php echo auto_version('/js/sidebar.js');?>"></script>
<script src="/js/jsrender.min.js"></script>
<script id="theTmpl" type="text/x-jsrender">
<h3 id= "msg_pane">{{:title}}</h3>
        <div class = "buttons_pane">
            <input type="text" class="pos_fixed inp_side" placeholder="Enter Field Name" id="fname{{:id}}"  />
            <div id="dropdown{{:id}}" >
                <select id="dropselect{{:id}}" class="select_side"></select>
            </div>
            <div id="sub_title{{:id}}" >
                <select id="sub_title_option{{:id}}" class="select_side"></select>
                <div class="moptions multiple_options{{:id}}"></div>
            </div>
            <div id="specialinstructions{{:id}}" >
                <p>Special Instructions</p>
                <textarea rows="5" cols="22" id="instructions{{:id}}"></textarea>
            </div>
        </div>
</script>
<script>
var fieldtype= <?php echo json_encode($fieldtype ); ?>;
var refsubtype= <?php echo json_encode($subtype ); ?>;
var fieldSubtypeMap= <?php echo json_encode($fieldSubtypeMap); ?>;

//variables
var delimage ="images/close.png";
var addcol ="images/add.png";
var delcol ="images/dele.png";
var count=1;
var tablecount=0;
var tableflag=0;
var curobj;
var formid=<?php echo $formid?>;
var page=<?php echo $wert?>;
//Create drawing area
var paper = Raphael("paper", 915, 1500);
var offx = $('#paper').offset().left;
var offy = $('#paper').offset().top;
var php_var = "<?php echo $l; ?>";
var  imgwid = "<?php echo $width;?>"; 
var  imghei = "<?php echo $height;?>"; 
var zr=imageratio(imghei,imgwid);
var img1 = paper.image(php_var,0,0,imgwid,imghei);
$(img1.node).on('dragstart',function(event){event.preventDefault();});
var prev_data=<?php echo json_encode($ark ); ?>;
var fields=[];
fields.push('');
//

function sendtemp(jobid,formid)
{

        if(jobid!=0)
        {
                window.location.href='sessiontemplate.php?name='+jobid+'&formid='+formid;
        }
        else
        {
            window.location.href='template.php';
        }
    
}
//
tableflag=0;
function tabFieldSelect(elem){
	$(".tabtype").removeClass('selected');
	$(elem).addClass('selected');
}

$("#tablebtn").click(function() {
	tableflag=1;
	tabFieldSelect(this);
	$("#msg_pane").html("drag to create table");
	$('.buttons_pane').hide();
});

$("#fieldbtn").click(function() {
	tableflag=0;
	tabFieldSelect(this);
	$("#msg_pane").html("drag to create field");
	$('.buttons_pane').hide();
});
//starting panel
startpanel();

//draw previous fields
for(i in prev_data)
{
    if(prev_data[i].istable==1)
    {
        var g=JSON.parse(prev_data[i].coordinates);
        var cord=g.rec[0]+","+(g.rec[1])+","+g.rec[2]+","+(g.rec[3]);
        var  newfield={"prevflag":1,"id" : prev_data[i].id,"coordinates" :cord,"name" : prev_data[i].name,"type" : prev_data[i].type,"sub_type" :  prev_data[i].sub_type,"details" : prev_data[i].details,"instructions" : prev_data[i].instructions,"istable" : 1};
        var f = new table(newfield,zr,1);
         fields[prev_data[i].id]=f;
         for(var j in g.col)
        	    fields[prev_data[i].id].addnewcolumn((g.col[j])/zr);
         for(var j in g.row)
             fields[prev_data[i].id].addnewrow((g.row[j])/zr);
         fields[prev_data[i].id].delicon.hide();
         count=parseInt(prev_data[i].id)+1;
         ++tablecount;
    }
    else{
    	var resx = prev_data[i].coordinates.split(",");
        var cord=resx[0]+","+resx[1]+","+resx[2]+","+resx[3];
        var  newfield={"prevflag":1,"id" : prev_data[i].id,"coordinates" :cord,"name" : prev_data[i].name,"type" : prev_data[i].type,"sub_type" :  prev_data[i].sub_type,"details" : prev_data[i].details,"instructions" :  prev_data[i].instructions,"istable" : 0};
        var f = new normalfield(newfield,zr,1);
        fields[prev_data[i].id]=f;
        fields[prev_data[i].id].delicon.hide();
        count=parseInt(prev_data[i].id)+1;
        }
}

//field creation by draging
var x,y,flag=0,tw;
img1.mousedown(function (event, a, b)
 {
    x=a;
    y=b;
    flag=1;
    tw=0;
  });
img1.mousemove(crtemp);
img1.mouseup(createrect);

function crtemp (e,a,b){
    if(flag==1 && ((tableflag==1 && tablecount<1)||tableflag==0))
    {
        if(x-a !=0 && y-b !=0 && a-x !=0 && b-y!=0 && x-a !=-1 && y-b !=-1 && a-x !=-1 && b-y!=-1 )
        {
            if(tw==1)
            req.remove();
            var t=a;
            var u=b;
            if(curobj!=undefined)
            curobj.delicon.hide();
            if(u>=y && t>=x)
                req = paper.rect(x-offx, y-offy, t-x-1, u-y-1);
            else if (t>x && u<y)
                req = paper.rect(x-offx, u-offy, t-x-1, y-u-1);
            else if (x>t && y>u)
                req = paper.rect(t-offx-2, u-offy-2, x-t+1, y-u+1);
            else if (t<x && u>y)
                req = paper.rect(t-offx, y-offy, x-t-2, u-y-2);
            tw=1;
        }
    }
}

function createrect(event, a, b) 
{
 if(tw==1)
 req.remove();
 tw=0;
 var c=a;
    var d=b;
    if((c-x > 10 || x-c > 10) && flag==1)
    {
        flag=0;
        if(d>=y && c>=x)
        	var cord=x+","+(y-offy)+","+c+","+(d-offy);
        else if (c>x && d<y)
        	var cord=x+","+(d-offy)+","+c+","+(y-offy);
        else if (x>c && y>d)
        	var cord=c+","+(d-offy)+","+x+","+(y-offy);
        else if (c<x && d>y)
        	var cord=c+","+(y-offy)+","+x+","+(d-offy);
    	
    	var  newfield=[{"id" : count,"coordinates" :cord,"name" :"field"+count,"type" : "text","sub_type" : "small","details" : "","instructions" : "","istable" : tableflag}];
    	 if(tableflag==1 && tablecount <1)
    	  {
    		  var f = new table(newfield[0],1,0);
    		  fields[count]=f;
              fields[count].addnewcolumn(0);
              fields[count].addnewrow(0);
              savedata(fields[count]);
              curobj=fields[count];
              fields[count].showdetails();
              ++tablecount;
              ++count;
          }
    	  else if(tableflag==0)
    	  {
    		  var f = new normalfield(newfield[0],1,0);
    	         fields[count]=f;
    	         fields[count].showdetails();
    	         savedata(fields[count]);
    	         curobj=fields[count];
    	         fields[count].showdetails();
              ++count;
           }
    	    
       }
    else
        flag=0;
}

</script>
</body>
</html>