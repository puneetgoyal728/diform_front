<?php
include_once 'config.php';
include_once 'db_conn.php';
include_once 'common_func.php';

$mailQuery = "select tjm.job_id jid, tjm.usr_id uid, tjm.job_name jname, tjm.no_pages npages, tum.email_id eid, tum.name name from 
    tbl_job_master tjm JOIN tbl_user_master tum 
    on tjm.usr_id = tum.usr_id where tjm.completed = 1 and mail_sent = 0";

$subject = "Your __JOBNAME__ is complete";
$body = "Dear __USERNAME__<br /><br />Your __JOBNAME__ is finished and available for review. 
Visit <a href='".$basePath."inboxresult.php?j=__JOBID__'>this</a> link to see the completed job.<br /><br />
Yours Sincerely<br /><br />
11TechSquare Team";

$fromMailArrIdx = 0;
foreach ($conn->dbh->query($mailQuery) as $mailData){
    $subjectUser =    str_replace("__JOBNAME__", $mailData['jname'],$subject);
    $bodyUser    =    str_replace("__USERNAME__", $mailData['name'], $body);
    $bodyUser    =    str_replace("__JOBNAME__", $mailData['jname'], $bodyUser);
    $bodyUser    =    str_replace("__JOBID__", $mailData['jid'], $bodyUser);
    $tomail  =    $mailData['eid'];
    sendmail($tomail, $fromMailArrIdx, $bodyUser, $subjectUser);
}

