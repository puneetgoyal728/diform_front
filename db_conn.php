<?php
include_once("config.php");
//global $conn;
class dbConnection {
	public function __construct( ) {
		global $host,$db,$user,$pass;
		$this->dbh = null;
		$this->initConnection ($host,$db,$user,$pass);
	}
	public function initConnection($host,$db,$user,$pass) {
		if($this->dbh == null){
			$this->dbh = new PDO('mysql:host='.$host.';dbname='.$db, $user, $pass);
		}
	}
	
	public function closeConnection () {
		$this->dbh = null;
	}
	public $dbh;
}

$conn = new dbConnection();
?>