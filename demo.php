<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
include_once 'common_func.php';
include_once('referenceArrays.php');
checksession();
$l="images/demo.jpg";

list($width, $height, $type, $attr) = getimagesize($l);

//previous data from databaase
$ark=array();
foreach(  $conn->dbh->query("SELECT * FROM tbl_demotemplate where form_id =1 ") as $ro)
{

 $data=array("id"=>$ro['field_id'],"coordinates"=>$ro['field_cord'],"name"=>$ro['field_name'],"type"=>$ro['field_type'],"sub_type"=>$ro['sub_field_type'],"details"=>$ro['field_details'],"instructions"=>$ro['instructions'],"istable"=>$ro['istable']); 
 array_push($ark, $data)  ;
}



?>
<!doctype html>
<html lang="en" >
<head>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="<?php echo auto_version('/css/default.css');?>" />
<title>Movable and Re-sizable Raphael JS Shape</title>
</head>
<style>
#left_area {
    background-color: white;
    border: 1px #cccccc solid;
    width: 270px;
    position: fixed;
    right: 0;
    padding: 20px 10px;
    border-right: 0px;
    top: 208px;
    min-height: 300px;
    overflow-y: auto;
    height: 300px;
}

</style>
<body>
<?php include('jobsheader.php');  ?>
<div id="paper"></div>
<div id="left_area">

<div id="result"></div></div>
<script src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>
<script src="<?php echo auto_version('/js/raphael.min.js');?>"></script>
<script src="<?php echo auto_version('js/demo.js');?>"></script>
<script src="<?php echo auto_version('/js/sidebar.js');?>"></script>
<script src="<?php echo auto_version('/js/jsrender.min.js');?>"></script>
<script id="theTmpl" type="text/x-jsrender">
<h3 id= "msg_pane">{{:title}}</h3>
        <div id = "buttons_pane">
            <input type="text" class="pos_fixed inp_side" placeholder="Enter Field Name" id="fname{{:id}}"  />
            <div id="dropdown{{:id}}" >
                <select id="dropselect{{:id}}" class="select_side"></select>
            </div>
            <div id="sub_title{{:id}}" >
                <select id="sub_title_option{{:id}}" class="select_side"></select>
                <div class="moptions multiple_options{{:id}}"></div>
            </div>
            <div id="specialinstructions{{:id}}" >
                <p>Special Instructions</p>
                <textarea rows="5" cols="40" id="instructions{{:id}}"></textarea>
            </div>
        </div>
</script>
<script>
var fieldtype= <?php echo json_encode($fieldtype ); ?>;
var refsubtype= <?php echo json_encode($subtype ); ?>;
var fieldSubtypeMap= <?php echo json_encode($fieldSubtypeMap); ?>;

//variables
var delimage ="images/close.png";
var addcol ="images/add.png";
var delcol ="images/dele.png";
var count=1;
var tablecount=0;
var tableflag=0;
var cur;
var formid=1;
var page=1;
//Create drawing area
var paper = Raphael("paper", 915, 1500);
var offx = $('#paper').offset().left;
var offy = $('#paper').offset().top;
var php_var = "<?php echo $l; ?>";
var  imgwid = "<?php echo $width;?>"; 
var  imghei = "<?php echo $height;?>"; 
var zr=imageratio(imghei,imgwid);
var img1 = paper.image(php_var,0,0,imgwid,imghei);
$(img1.node).on('dragstart',function(event){event.preventDefault();});
var prev_data=<?php echo json_encode($ark ); ?>;
var fields=[];
fields.push('');

//starting panel
startpanel();

//draw previous fields
for(i in prev_data)
{
    if(prev_data[i].istable==1)
    {
        var g=JSON.parse(prev_data[i].coordinates);
        var cord=g.rec[0]+","+(g.rec[1])+","+g.rec[2]+","+(g.rec[3]);
        var  newfield=[{"prevflag":1,"id" : prev_data[i].id,"coordinates" :cord,"name" : prev_data[i].name,"type" : prev_data[i].type,"sub_type" :  prev_data[i].sub_type,"details" : prev_data[i].details,"instructions" : "","istable" : 1}];
        var f = new table(newfield[0],zr,1);
     fields.push(f);
     for(var j in g.col)
     fields[count].addnewcolumn((g.col[j])/zr);
     for(var j in g.row)
         fields[count].addnewrow((g.row[j])/zr);
     savedata(fields[count]);
     ++count;
     ++tablecount;
    }
    else{
        var resx = prev_data[i].coordinates.split(",");
        var cord=resx[0]+","+resx[1]+","+resx[2]+","+resx[3];
        var  newfield=[{"prevflag":1,"id" : prev_data[i].id,"coordinates" :cord,"name" : prev_data[i].name,"type" : prev_data[i].type,"sub_type" :  prev_data[i].sub_type,"details" : prev_data[i].details,"instructions" : "","istable" : 0}];
        var f = new normalfield(newfield[0],zr,1);
     fields.push(f);
     ++count;

        }
    
}



</script>
</body>
</html>