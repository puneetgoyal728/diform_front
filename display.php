<link type="text/css" rel="stylesheet" href="css/default.css" />
<link
	href="css/googlefonts.css" rel='stylesheet' type='text/css' />

<style>
#para {
	background-color: lightgrey;
	width: 500px;
	padding: 25px;
	border: 3px solid navy;
	margin: 25px;
	position: absolute;
	left: 100px;
	top: 150px;
}
</style>
<script
	src="js/jquery-1.11.1.min.js"></script>
<script src="js/raphael.min.js"></script>
<?php

$f=0;
$p=0;
$arr=array();

$listFormsQuery = "select tim.page_no mpg,tim.form_id form_id,tim.data_img_name data_img_name,ttd.lock_status lock_status
from tbl_image_master tim join tbl_template_details ttd where(tim.form_id,tim.page_no) in (select form_id, min(page_no) from tbl_image_master where user_id = $uid and active = 1 group by form_id) and tim.form_id=ttd.form_id";

foreach($conn->dbh->query($listFormsQuery) as $row) {
    	
    $formid=$row['form_id'];
    $name="";
    $lock_status= $row['lock_status'];
    $te =$row['data_img_name'];
    $file=explode("/", $te);
    $tag = $thumnails_path.$file[1];
    foreach(  $conn->dbh->query("SELECT temp_name
            FROM tbl_template_details where form_id=$formid") as $row) {
            $name = $row['temp_name'];
    }

    if (file_exists($tag)==false)
    {
        createthumb($te,$thumnails_path);
    }
    $tag = $file[1];
    array_push($arr, array('thumb'=>$tag,'name'=>$name,'id'=>$formid,'lock_status' =>$lock_status));
}

?>

<div id="container">
	<div class="center" id="der"
		style="width: 499px; margin-left: 60px; border: 1px black dotted; overflow: auto; height: 600px;"></div>
</div>


<script>

var loop=0;
var count=0;
var maxpages=3;
var imgname = [];
duplicate=[];
settings=[];
var imgn =[];
var img = [];
var del = [];
var t = [];
var recs = [];
var idel = [];
var cirmage ="images/close.png";
var blank="digits/blank.gif";
var cnct ="checked.jpg";
var jay= <?php echo json_encode($arr ); ?>;
var jobid = "<?php echo $t;?>";
var len = jay.length;
var paper = Raphael("der",450,230*((len/maxpages)+1));
for(var i = 0; i < len;++i)
{	
	if(jobid==0){
		create(jay[i]);
	}
	else{
		createforselect(jay[i]);
	}	

}

function createforselect(im)
{

	imgname[loop]=im.thumb;
	yas =(50*( ((Math.floor(count/maxpages))*5)+1));
	xas = (150*(count%maxpages))+10;
	recs[loop]= paper.rect(xas,yas,110,170);
	imgn[loop] = im.name;
	idel[loop] =im.thumb;
	t[loop] = paper.text(xas+40, yas+160,im.name);
	del[loop] = paper.image(blank,xas,yas-10,20,20);
	img[loop] = paper.image("<?php echo $localhost_thumnails;?>"+im.thumb,xas+2,yas+10,95,145);
	img[loop].mousemove(function ()
	{
    	this.attr('cursor', 'pointer');
    });
	img[loop].click(function()
	{

 		var x=  this.attr('x')+38;
		var y=  this.attr('y')+158;
  		var ty;
	
    	for(ty=0;ty <count;++ty)
    	{
        	var zz= t[ty].attr('x');
		    if(zz==x)
        	{
                var jobid = "<?php echo $t;?>";
				$.ajax({
					type: "POST",
					url: "connect.php",
					data: { 'job': jobid ,'id' : im.id}
				}).done(function(data){
					if(data == 1){
						window.location.href='inbox.php';
						}else{
							alert("Some error occured. Please try again");
						}
					});

				del[ty].attr("src","images/checked.png");
                //setTimeout(function () {    }, 3000);
		        break;
        	}
    	}
	});
	++loop;
	++count; 
}

function create(im)
{
// var im =xhr.responseText;
	imgname[loop]=im.thumb;
	yas =(50*( ((Math.floor(count/maxpages))*5)+1));
	xas = (150*(count%maxpages))+10;
	recs[loop]= paper.rect(xas,yas,110,170);
	imgn[loop] = im.name;
    idel[loop] =im.thumb;
  	t[loop] = paper.text(xas+40, yas+160,im.name);
	del[loop] = paper.image(cirmage,xas,yas-10,20,20);
	duplicate[loop] = paper.image(cirmage,xas,yas+110,20,20);
	settings[loop] = paper.image(cirmage,xas+20,yas+110,20,20);
	img[loop] = paper.image("<?php echo $localhost_thumnails;?>"+im.thumb,xas+2,yas+10,95,145);
	img[loop].mousemove(function ()
	{
		this.attr('cursor', 'pointer');
	});
	if(im.lock_status==0)
    {
   	img[loop].click(function()
   	{
          
   		var x=  this.attr('x')+38;
   		var y=  this.attr('y')+150;
   		var ty;
   		for(ty=0;ty <count;++ty)
   		{
   			var zz= t[ty].attr('x');
   	        var zy= t[ty].attr('y');
   			if(zz==x && zy==y)
   			{
   				
   				window.location.href='tech.php?g='+im.id+"_0";
   				break;
   			}
   
   		}
          
   	});
       del[loop].click(function()
   	{
        
    		if (confirm("you want to delete the complete template") == true) 
   		{
   			var x=  this.attr('x');
   			var y=  this.attr('y')+10;
     			var ty;
   			for(ty=0;ty <count;++ty)
   			{
               	var zz= recs[ty].attr('x');
   				if(zz==x)
   				{
   					shiftx(ty);
   					break;
   				}
   			}
    		}
   	});
    }
  ++loop; ++count; 
}


function shiftx(xy)
{
	var ty,xs,ys;
	$.ajax({
	    url: "deletetemp.php",
	    type: "POST",
	    data: { 'name': idel[xy] }
	});
	for( ty =xy;ty<count-1;++ty)
	{

		xs = img[ty].attr('x');
		ys = img[ty].attr('y');
		img[ty].remove();
		t[ty].remove();


		t[ty] = paper.text(xs+38, ys+150,imgn[ty+1]);
		imgn[ty]=imgn[ty+1];
		idel[ty]=idel[ty+1];
		imgname[ty]=imgname[ty+1];
		var gh="<?php echo $upload_path ;?>";
		img[ty] = paper.image(gh+imgname[ty+1]+".jpg",xs,ys,95,145);




		img[ty].attr("opacity", "1");
		img[ty].mousemove(function ()
        {
    		this.attr('cursor', 'move');
    	});
		img[ty].click(function()
		{

			var x=  this.attr('x')+38;
					   
			var y=  this.attr('y')+150;
			var tz;
			for(tz=0;tz <count;++tz)
			{
                var zz= t[tz].attr('x');
                var zy= t[tz].attr('y');
				if(zz==x && zy==y)
				{
					var f = imgname[tz];
					var d =f.split("_");
		            var fg = d[1]+"_"+d[2];
		            fg.trim();
		            window.location.href='tech.php?g='+fg;
					break;
				}
			}
		});
	}


	recs[count-1].remove();
	img[count-1].remove();
	del[count-1].remove();
	//img1[count-1].remove();
	t[count-1].remove();
	--loop;
	--count;

}

</script>

</html>
