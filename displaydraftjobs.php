<?php
include_once('common_func.php');

$arr=array();

foreach(  $conn->dbh->query("select tjud.job_id job_id,tjud.upload_name name ,tjm.job_name job_name from tbl_job_upload_details tjud join tbl_job_master tjm where (tjud.job_id,tjud.upload_order) in (select job_id, min(upload_order) from tbl_job_upload_details group by job_id) and tjm.job_id=tjud.job_id and tjm.form_id is null and tjm.usr_id = $uid") as $row)
{

    $job_name=$row['job_name'];
    $job_id=$row['job_id'];
    $n=$job_id."/".$row['name'];

    $org_path=$jobs_path.$n;

    $img=$localhost_job_thumnails.$n;
     
    if(!is_dir($localhost_job_thumnails.$job_id))
    {
        mkdir($localhost_job_thumnails.$job_id);
    }


    if (file_exists($img)==false)
    {
        $fileExtn = getExtension($img);
        if($fileExtn=="pdf")
            $img="images/pdf.png";
        else
            createthumb($org_path,$job_thumnails_path.$job_id."/");

    }

    array_push($arr, array('img'=>$img,'jobname'=>$job_name,'id'=>$job_id));
}



/*

$queryForFieldtype = "select min(tjd.saved_name) as name from tbl_job_master tjm join tbl_job_details tjd where tjm.usr_id=7 and tjm.job_id=tjd.job_id and tjd.active=1 group by tjd.job_id";
echo $queryForFieldtype."<br />";
$fieldtypeResult = $conn->dbh->query($queryForFieldtype);
$results = $fieldtypeResult->fetchAll(PDO::FETCH_ASSOC);
echo $results['name'][1];
*/
?>
<html>
<style>
.border {
	background-color: lightgrey;
	width: 300px;
	padding: 25px;
	border: 1px solid navy;
	margin: 25px;
}
</style>
<body>
	<script src="js/jquery-1.11.1.min.js"></script>


	<?php 
	foreach ($arr as $value) {
	    echo '
	    
	    <div style="margin-bottom:100px;" id="border'.$value["id"].'">
	    	<div id="cont">
	    	<div class="job_name" style="padding:10px;"> 
	    		<h3 style="float:left;">'.$value["jobname"].'<h3/>
	    		<div class="right" style="padding:0px; margin-top:-2px;">
		    	<a href = "javascript:void(null);" class="btn" onclick="deleteJob('.$value["id"].');">Delete job</a>
		    	<a class="btn_rit" href="jobs.php?jid='.$value["id"].'" >Process Job</a>
		    	</div>
		   		<div class="clear"></div>
	    	</div>
	    	<div class="clear"></div>
		    <div class="left" id="border'.$value["id"].'" >
		  	  <img src="'.$value["img"].'" height="80" width="62" />
			</div>
		
	    	<div class="clear"></div>
	    </div>';
	}

	?>
</body>
<script>

function deleteJob (id) {
	$.ajax({
        type: "POST",
        url: "deletejobs.php",
        data: { 'name': id},
        datatype: JSON		    
      }).done(function(){
    	  $('#border'+id).remove();
      });
	
}

</script>
</html>
