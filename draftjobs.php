<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('jobclass.php');
include_once('common_func.php');
checksession();
$uid =$_SESSION["uid"];
$currentHead = "JOBS";
?>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>Multiple File upload with PHP</title>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/raphael.min.js"></script>
</head>
<link type="text/css" rel="stylesheet" href="css/default.css" />
<body style="background-color:#eeeeee;">
<?php include("jobsheader.php");?>
<div id="inhead">
	<div class="center">
		<div class="left">
			<h3>Draft Job</h3>
			<a href="jobs.php" class="right button">Start New Job</a>
		</div>
		<div class="right">
		</div>
		<div class="clear"></div>
	</div>
</div><!---end of inhead--->
<div id="draft" style="background-color:#eeeeee; width:100%;">
	<div class="center">
		<?php  include_once('displaydraftjobs.php'); ?>
		<div class="clear"></div>
</div>
<!---end of draft---></div>
<!---end of container--->
</body>
</html>