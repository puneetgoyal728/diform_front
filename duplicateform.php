<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
checksession();
$uid=$_SESSION['uid'];
$form= $_POST['form'];
$temp_name= $_POST['temp_name'];
$prevdata=array();
foreach( $conn->dbh->query("SELECT max(form_id) as 'maxform' FROM tbl_image_master")  as $row) {
    $nextformid =$row['maxform']+1;
}
foreach( $conn->dbh->query("SELECT * FROM tbl_image_master where form_id =$form and active = 1")  as $row) {

    $duplicateimg=$upload_path.$uid."_".$nextformid."_".$row['page_no'].".".getExtension($row['data_img_name']);
    copy($row['data_img_name'],$duplicateimg);
    $imagequery="insert into tbl_image_master (user_id,form_id,page_no,user_image_name,data_img_name,width,height,active) values(:uid,:nextformid,:page_no,:user_image_name,:duplicateimg,:width,:height,1)";
    $sth = $conn->dbh->prepare($imagequery);
    $sth->execute(array(':uid'=>$uid,':nextformid'=>$nextformid,':page_no'=>$row['page_no'],'user_image_name'=>$row['user_image_name'],':duplicateimg'=>$duplicateimg,':width'=>$row['width'],':height'=>$row['height'])) or die(var_dump($sth->errorInfo()));
    
}
foreach( $conn->dbh->query("SELECT * FROM tbl_template_master where form_id =$form ")  as $row) {
$templatequery="insert into tbl_template_master ( user_id , form_id , page_no , field_id , field_name , field_cord ,field_details ,rc_add_date, rc_add_time , rc_modi_date , rc_modi_time , field_type , sub_field_type , instructions ) values( :uid , :nextformid , :page_no,:field_id,:field_name,:field_cord,:field_details, now(),now(),now(),now() ,:field_type,:sub_field_type,:instructions)";
$sth = $conn->dbh->prepare($templatequery);
$sth->execute(array(':uid'=>$uid ,':nextformid'=> $nextformid , ':page_no'=>$row['page_no'],':field_id'=>$row['field_id'],':field_name'=>$row['field_name'],':field_cord'=>$row['field_cord'],':field_details'=>$row['field_details'],':field_type'=>$row['field_type'],':sub_field_type'=>$row['sub_field_type'],':instructions'=>$row['instructions'])) or die(var_dump($sth->errorInfo()));

}
foreach( $conn->dbh->query("SELECT min(page_no) page ,data_img_name FROM tbl_image_master where form_id =$form and active = 1")  as $row) {
    $img=explode("/",$row['data_img_name']);
    $img=$img[1];
}
$detailsquery="insert into tbl_template_details (form_id , name,type, rangefrom , rangeto , rc_add_time , rc_add_date , rc_modi_date , rc_modi_time ) values (:nextformid,:temp_name,1,null,null,now(),now(),now(),now())";
$sth = $conn->dbh->prepare($detailsquery);
$sth->execute(array(':nextformid'=>$nextformid,':temp_name'=>$temp_name)) or die(var_dump($sth->errorInfo()));

if (file_exists($thumnails_path.$img)==false)
{
    createthumb($upload_path.$img,$thumnails_path);
}

array_push($prevdata, array('thumb'=>$localhost_thumnails.$img,'name'=>$temp_name,'id'=>$nextformid,'lock_status' =>'0'));
echo json_encode($prevdata);
?>