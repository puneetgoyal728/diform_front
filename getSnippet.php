<?php
session_start();
include_once("config.php");
include_once ("db_conn.php");
include_once "jobResultClass.php";
include_once('common_func.php');
checksession();
if(isset($_SESSION["uid"])){
	$uid = $_SESSION["uid"];
	$jrc = new JobResultClass;
	$jid = $_REQUEST["jid"];
	$jpage = $_REQUEST["jpage"];
	$field = $_REQUEST["fid"];
	
	$isQueryValid = $jrc->isJobIdValid($jid,$uid);
	if($isQueryValid["status"]==1){
	    if($field==0){
	        $imgName = $jrc->getJobPageImageName($jid, $jpage);
	        $imgPath = $jobs_path.$jid.'/'.$imgName;
	    }else{
    		$imgName = $jrc->getImageNameFromJobPg($jid,$jpage,$field);
    		$imgPath = $snippets_path.$jid.'/'.$imgName.'.jpg';
	    }
		$type = 'image/jpeg';
		header('Content-Type:'.$type);
		header('Content-Length: ' . filesize($imgPath));
		readfile($imgPath);
	}
}
?>