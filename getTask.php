<?php
include_once 'config.php';
include_once 'common_func.php';
if(isset($_GET['job_type']) && isset($_GET['qty']) && isset($_GET['salt']) && isset($_GET['hash'])){
    $jobType = $_GET['job_type'];
    $requestedNum = $_GET['qty'];
    $salt = $_GET['salt'];
    $hash = $_GET['hash'];
    $key = $squadTaskFetchKey.$salt;
    $data = $jobType."||".$requestedNum;
    $genHash = hash_hmac ( "sha256" , $data , $key);
//     echo $genHash;exit; 
    if(strcmp($hash, $genHash) == 0){
        echo json_encode(getNewSquadrunTask($jobType,$requestedNum));
    }else{
        echo json_encode(array('status'=>-1,'msg'=>'key mismatch'));
    }
}else{
    echo json_encode(array('status'=>-5,'msg'=>'param error'));
}