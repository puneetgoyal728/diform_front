
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css" />
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700'
	rel='stylesheet' type='text/css' />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<title>Form Fly - How We Do</title>
</head>
<body>
	<div id="header3">
		<?php include('header.php'); ?>
	</div>
	<!--end of header--->

	<div id="container">
		<div class="center">
			<p align = "center" style = " font-size: 30px; font-wieght: 20px; color: #000 "> Form Fly uses a unique combination of Technology and Human Intelligence to quickly digitalize handwritten forms. </p>
</div>
</br> </br>	
<div class = "abcd" style = "background: #f1f1f1; padding: 30px; 	">
<div id="layer_work">
		<div class="center">
			<h2 class="head">The science behind it!</h2> </br>
			<p>Once the user submits his documents, here's what we do! </p>
			<ul>
				<li>
					<span id="idxPhw1"></span>
					<h3 style="margin-top:30px;">Break Down</h3>
					<p>We break down the files into snippets.</p>
				</li>
				
				<li>
					<span id="idxPhw2"></span>
					<h3>Data Selection</h3>
					<p>Our intelligent recognition technology reads the handwritten data.</p>				
				</li>
				
				<li>
					<span id="idxPhw3"></span>
					<h3>Human Correction</h3>
					 <p>Human correction is done for correcting any errors.</p> 				
				</li>		
			</ul>
			<div class="clear"></div>
			
			
		</div><!---end of layer_work center--->
	</div><!---end of layer_work--->


</div>

</br> </br>	
<!--<p align = "center" style = "font-family: 'Raleway', sans-serif; font-size: 40px; font-wieght: 20px; color: #000 "> Amazing, isn't it?</p>-->
</br> <center> <!--<a href="signup.php" class="anc">Ready to work?</a> --></center>
			</div>
				

	<div id="footer">
		<?php include("footer.php"); ?>
	</div>
	<!---end of footer--->
</body>

</html>
