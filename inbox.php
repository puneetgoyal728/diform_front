<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
include_once ('jobResultClass.php');
if(!isset($_SESSION["uid"])){
    header("location:logx.php");
}
$uid = $_SESSION['uid'];

$jrcObj = new JobResultClass;
$jobListArr = $jrcObj->getUserJobList($uid);
$incomplete = array();
$complete = array();
foreach ($jobListArr as $eachjob){
    $body = "<ul class = 'job_row' id='job_row_".$eachjob['jobid']."'>
            <li class='jobname' ><h4>".$eachjob['jobname']."</h4></li>";
    if($eachjob['iscomplete']){
        $body .= "<li><a href='inboxresult.php?j=".$eachjob['jobid']."' >
        <div class='progressbar'>
        <div class='progressbarcom'>
        <span class='value'>Completed</span></div></div></a>";
    }else{
        $body .= "<li>
        <div class='progressbar'>
        <div class='progressbarin' style='width: ".(($eachjob['num_processed_pages']/$eachjob['numpages'])*800)."px;'>
        <span class='value'>".number_format((float)(($eachjob['num_processed_pages']/$eachjob['numpages'])*100), 2, '.', '')."%</span></div></div>";
        $body .="<div class='id_data'>".$eachjob['jobid']."</div>";
    }
    $body .= "</li></ul><div class='clear'></div>";
    if($eachjob['iscomplete']){
        array_push($complete, $body);
    }else{
        array_push($incomplete, $body);
    }
}
$currentHead = "INBOX";
?>
<html>

<link type="text/css" rel="stylesheet" href="css/default.css" />
<link href="css/googlefonts.css" rel='stylesheet' type='text/css' />
<style>
div.fg {
	background-color: lightgrey;
	width: 1000px;
	padding: 25px;
	border: 5px solid navy;
	margin: 25px;
}

.id_data {
	display: none;
}

button.clr {
	color: red;
}

.job_row {
	padding: 5px 0;
}

.jobname {
	float: left;
}

.progress {
	float: left;
}

.status {
	float: left;
}

.clr {
	clear: both;
}
</style>
<body>

	<?php include("jobsheader.php");?>
	<div id="container" style="background-color: #eeeeee;">
		<div class="center">
			<div id="running">
				<h3 class="headnew">Jobs Running</h3>

				<?php foreach ($incomplete as $job){
				    echo $job;
				}?>

			</div>
			<!-- end of running -->
			<div style="margin-top: 100px;">
				<h3 class="headnew">Jobs Completed</h3>
				<?php foreach ($complete as $job){
				    echo $job;
				}?>
			</div>
			<!-- end of running -->
		</div>
		<!-- end of container center -->
	</div>
	<!-- end of container -->
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">

setInterval(function() {
    var idList = [];
	$('.id_data').each(function(){
		idList.push($(this).text());
	});
	if(idList.length>0){
    $.ajax({
        type: 'POST',
        url: 'jobStatus.php',
        data: {
            data: JSON.stringify(idList)
        },
        dataType: "json",
        success: function(result) {
            if (result == "0"){
            	location.reload(false);
            }else{
                for (var i = 0; i < result.length; i++) {
					var jobelem = result[i];
					var percent = (jobelem.num_processed_pages/jobelem.no_pages)*100;
					var width = ((jobelem.num_processed_pages/jobelem.no_pages).toFixed(2))*800;
					var editDom =$('#job_row_'+jobelem.jobid).find('.progressbarin'); 
					editDom.text(percent.toFixed(2)+'%');
					editDom.css('width',width);
					/*if(jobelem.iscomplete == '1'){
						var filledData = $('#job_row_'+jobelem.jobid).find('.progress').html();
						$('#job_row_'+jobelem.jobid).find('.progress').replaceWith("<a href='inboxresult.php?j="+jobelem.jobid+"' >"+filledData+"</a>");
						$('#job_row_'+jobelem.jobid).find('.id_data').remove();
					}*/
				}
            }
        }
    });
    }
}, 100000);

</script>
</html>
