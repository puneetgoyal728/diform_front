<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
include_once ('common_func.php');
include_once ('jobResultClass.php');
include_once ('resultHelper.php');
checksession();
if(!isset($_GET['j'])){
    header("location:inbox.php");
}
$uid = $_SESSION['uid'];
$jid = $_GET['j'];
$jrcObj = new JobResultClass;
$JobName = $jrcObj->getJobNameForJobId($jid);
$jobListArr = $jrcObj->isJobIdValid($jid, $uid);
//print_r($jobListArr);exit;
if($jobListArr['status']!==1){
    header("location:inbox.php");
}

$jobArr = $jrcObj->getJobProcessedDetails($jid);
// print_r($jobArr);
// print_r(rawDataToTabularFormat($jobArr));exit;
$readableData = rawDataToTabularFormat($jobArr);
$heading = $readableData['heading'];
$values = $readableData['data'];
$formPageLen = count($heading);
$currFormPg = 0;
$body = "<table class='table' cellspacing='0'>
<tr>";
while ($currFormPg < $formPageLen) {
    foreach ($heading[$currFormPg] as $fieldLbl){
        $body .= "<th class='th'>$fieldLbl</th>";
    }
    $currFormPg++;
}
$body .= "</tr> <tr>";
$currInpPg = 0;

$arrKeys = array_keys($values);

while ($currInpPg <= $arrKeys[count($arrKeys)-1]){
    foreach ($values[$currInpPg] as $fieldId=>$fieldVal){
        if(($currInpPg%$currFormPg) == 0 || $fieldId != 0){
            $body .= "<td ".("onClick = \"var event = arguments[0] || window.event;getImage(event,'$jid','".
                    ($currInpPg+1)."','$fieldId')\"")." id='text_".$jid."_".
                    ($currInpPg+1)."_".$fieldId."'>$fieldVal</td>";
        }
    }
    $currInpPg++;
    if(($currInpPg%$currFormPg) == 0){
        $body .= "</tr><tr>";
    }
}
$body .= "</tr></table>";
?>

<html>
<head>
<title>Data for Job <?php echo $JobName;?>
</title>
<link type="text/css" rel="stylesheet"
	href="<?php echo auto_version('/css/default.css');?>" />
<link href="css/googlefonts.css" rel='stylesheet' type='text/css' />
<script src="js/jquery-1.11.1.min.js"></script>
<link type="text/css" rel="stylesheet"
    href="<?php echo auto_version('/css/inboxResult.css');?>">
<script>
function utf8_to_b64( str ) {
	  return window.btoa(unescape(encodeURIComponent( str )));
	}
var img = new Image();
function getImage(e,jid,jpage,fid){
	if (e.pageX || e.pageY) { 
	  x = e.pageX;
	  y = e.pageY;
	}
	else { 
	  x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
	  y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
	}
	$("#value_editor").hide();
	var bubbleDom = $("#res_bubble");
	bubbleDom.removeClass();
	bubbleDom.addClass('bubble');
	if(fid == 0) {
	    bubbleDom.addClass('page');
	    $("#value_editor input[name='edited_value']").attr("readonly","true");
	    $("#update_btn").hide();
	}else{
		$("#value_editor input[name='edited_value']").removeAttr("readonly");
		$("#update_btn").show();
	}
	$(".resultImg img").css("max-width","");
	$(".resultImg img").css("max-height","");
	if(y < 400){
		if(x<700){
    		bubbleDom.css({'top':(y+10),'left':(x-72)});
		}else{
			bubbleDom.addClass('rt');
			bubbleDom.css({'top':(y+10),'left':(x - bubbleDom.width() + 14)});
		}
	}else{
		if(x<700){
    		bubbleDom.addClass('lb');
    		bubbleDom.css({'top':(y- bubbleDom.height() - 47),'left':(x-72)});
		}else{
			bubbleDom.addClass('rb');
			bubbleDom.css({'top':(y- bubbleDom.height() - 47),'left':(x - bubbleDom.width() + 14)});
		}
	}
	$("#snpimage").attr("src","images/loader_trans.gif");
	bubbleDom.show();
	img.onload = null;
	img.onload = function() {
	  //alert(this.width + 'x' + this.height);
		$("#snpimage").replaceWith(img);
		$("#imglnk").attr("href","getSnippet.php?jid="+jid+"&jpage="+jpage+"&fid="+fid);
		img.id = "snpimage";
		
	    if(this.width> this.height){
	        $("#snpimage").css("max-width",445);
	        $("#snpimage").css("max-height",120);
	    }else{
        	$("#snpimage").css("max-height",300);
	    }
	    $("#value_editor input[name='edited_value']").val($("#text_"+jid+"_"+jpage+"_"+fid).text());
	    $("#value_editor input[name='for_field']").val(fid);
	    $("#value_editor input[name='for_page']").val(jpage);
	    $("#value_editor input[name='for_jobid']").val(jid);
	    $("#value_editor").show();
	};
	img.src = "getSnippet.php?jid="+jid+"&jpage="+jpage+"&fid="+fid;
	$("#imglnk").attr("href","javascript:void(null);");
}

function targetUpdate(data,jid,jpage,val,fid){
	if(data.status){
    	$("#notifDiv").text("value "+data.data+" updated").show().delay(1000).fadeOut("slow");
    	$("#text_"+jid+"_"+jpage+"_"+fid).text(data.data);
	}
}

function updateVal(){
	var jid = $("#value_editor input[name='for_jobid']").val();
	var jpage = $("#value_editor input[name='for_page']").val();
	var val = $("#value_editor input[name='edited_value']").val();
	var fid = $("#value_editor input[name='for_field']").val();
	$.ajax({
		type: "POST",
		url: "updateSnippetValue.php",
		dataType: "json",
		data: { 'jid':jid,
			'jpage':jpage,
			'val': val,
			'fid':fid}
	}).done(function(data) {
		targetUpdate(data,jid,jpage,val,fid);
	});
	return false;
}
</script>
</head>
<body>
	<div style="background-color: #eeeeee;">

		<?php include("jobsheader.php");?>

		<div class="job_res_head">
			<h3 class="headin">
				<?php echo $JobName;?>
			</h3>
			<a href="jobDownloadAsCSV.php?j=<?php echo $jid;?>" target="_blank"
				class="dwld">Download as csv</a>
			<div class="clear"></div>
		</div>

		<div class="dataDiv">
			<?php 
			echo $body;
			?>
		</div>
		<div id="res_bubble" class="bubble">
			<div id="image_holder" class="resultImg">
				<a id="imglnk" href="javascript:void(null);" target="_blank"> <img
					id="snpimage" title="Click on image to view larger" src=""
					alt="snippet image" />
				</a>
			</div>

			<div id="value_editor" class="edit_box">
				<form action="javascript:void(null);" id="editValue" onsubmit="return updateVal();">
					<input name="edited_value" type="text"
						class="showVal" /> 
					<input name="for_field" type="hidden" /> 
					<input name="for_page" type="hidden" /> 
					<input name="for_jobid" type="hidden" /> 
					<input type="submit" value="Update" id="update_btn" class="edit_button" />
				</form>
				<div id="notifDiv" class="notifArea" ></div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
$(document).keyup(function(e) 
{
  
    if(e.keyCode==27)
    {
        $("#res_bubble").hide();
    }
});
</script>
</html>
