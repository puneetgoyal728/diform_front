<?php 
session_start();
include_once 'common_func.php';
if(isset($_COOKIE["uid"])){
    header("location:jobs.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta name="google-site-verification" content="Hw5cOlIVP39UwJpu-luRdlo9iH5r7Kmlre3It7qHibE" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css"/>

<link type="text/css" rel="stylesheet" href="css/jquery.custombox.css"/>
 <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'/>
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="js/carousels.js"></script>
<script type="text/javascript" src="js/hdsw2.js"></script>

  <script type="text/javascript" src="js/carousels.js"></script>


<script type="text/javascript">
	$(window).scroll(function () {
    $("#ban").css("background-position","50% " + Math.round($(this).scrollTop() / 2) + "px");
});
</script>

<title>Form Fly - Home Page</title>
</head>
<body>

<div id="ban">
	<div class="des_header">
		<?php include("header.php"); ?>
	</div><!---end of header--->
	
	<div id="banner">
		<div class="center" style="text-align:center; ">
			<div style =  "padding: 15px; border-radius: 10px; " >
			<p style = "font-family: 'Raleway', sans-serif; font-size: 60px; font-wieght: 20px; color: #fff">Get your paper working for you <br/></p>
			<p class="small_para">We extract data from your documents, at a never before speed and accuracy,<br/> while you go about doing everything else.</p>
			<!--<a href="signup.php" class="anc">Start now</a>-->
		</div></div><!---end of banner center--->
	</div><!---end of banner--->
</div><!---end of ban--->
	
	<div id="layer_work">
		<div class="center">
			<h2 class="head">Benefits</h2>
			<ul>
				<li>
					<span id="idxPg1"></span>
					<h3 style="margin-top:30px;">Speed</h3>
					<p>Get weeks of work done in hours.</p>
				</li>
				
				<li>
					<span id="idxPg2"></span>
					<h3>Accuracy</h3>
					<p>99% + Accuracy.</p>				
				</li>
				
				<li>
					<span id="idxPg3"></span>
					<h3>Privacy</h3>
					 <p>Complete data privacy.</p> 				
				</li>		
			</ul>
			<div class="clear"></div>
			
			<!--<a href="#" class="anc">Try for free</a>-->
		</div><!---end of layer_work center--->
	</div><!---end of layer_work--->
	
	<div id="layer_steps">
		<div class="center">
			<h2 class="head">Simple 3-step process</h2>
			<div class="left" style = " border: 1px solid rgba(0, 0, 0, 0.13);">
				<center> <img src="images/1.jpg" alt="" id="one_img" style = "width: 500px; height: 250px;"/> </center>
				<center> <img src="images/2.jpg" alt="" id="two_img" style = "width: 500px; height: 250px;" /> </center>
				<center> <img src="images/3.jpg" alt="" id="three_img" style = "width: 500px; height: 250px;"/> </center>
			</div>
			
			<div class="right">
				<ul>
					<li>
						<a id="one" class="current">
							<img src="images/step1.png" alt=""/>
							<span>Upload file</span>
							<p>Choose files you want to be digitalized.</p>
						</a>
					</li>
					
					<li>
						<a id="two" class="hover">
							<img src="images/step2.png" alt=""/>
							<span>Select Areas</span>
							<p>Specify fields you want to be read.</p>					
						</a>
					</li>
					
					<li>
						<a id="three" class="hover">
							<img src="images/step3.png" alt=""/>
							<span>Relax</span>					
							<p>Now sit back and relax. We're on it</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="clear"></div>
		</div><!---end of layer_steps center--->
	</div><!---end of layer_steps--->
	
	<div id="layer_clients">
		<h2 class="head">Testimonials</h2>
		<br> 
		<br>


		<?php include("testimonials.php"); ?>
	
	</div><!---end of layer_clients--->
	
	<div id="footer">
		<?php include("footer.php"); ?>
	</div><!---end of footer--->	
</body>
<script type="text/javascript">
function hover(id) {
    $('#idxPg'+id).attr('src', 'images/icn'+id+'.png');
}
function unhover(id) {
	$('#idxPg'+id).attr('src', 'images/icn1'+id+'.png');
}
</script>
</html>
