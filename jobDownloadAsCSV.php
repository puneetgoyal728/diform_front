<?php
session_start();
include_once 'config.php';
include_once 'db_conn.php';
include_once 'jobResultClass.php';
include_once 'resultHelper.php';
include_once('common_func.php');
checksession();
if(!isset($_SESSION['uid'])|| !isset($_GET['j'])){
    exit();
}
header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=file.csv");
// Disable caching
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: 0"); // Proxies

$jrcObj = new JobResultClass;
$jobListArr = $jrcObj->isJobIdValid($_GET['j'], $_SESSION['uid']);
if($jobListArr['status']!==1){
    echo "";exit;
}
function outputCSV($data) {
    $output = fopen("php://output", "w");
    foreach ($data as $row) {
        fputcsv($output, $row); // here you can change delimiter/enclosure
    }
    fclose($output);
}
$jobArr = $jrcObj->getJobProcessedDetails($_GET['j']);
$readableData = rawDataToTabularFormat($jobArr);
$csvData = tabularDataToCSVFormat($readableData);
outputCSV($csvData);

?>