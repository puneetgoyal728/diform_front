<?php
include_once ('config.php');
include_once ('db_conn.php');
class JobResultClass{

	public function getUserJobList ($uid) {
		global $conn;
		$returnArr = array();
		$jobsForUserQuery = "SELECT sum(tjd.processed=2) num_processed_pages, 
		tjm.job_name jobname, tjm.job_id jobid, tjm.no_pages numpages,
		tjm.completed iscomplete
		FROM tbl_job_master tjm JOIN tbl_job_details tjd ON tjm.job_id=tjd.job_id where
		tjm.active = 1 and tjd.active = 1 and tjm.usr_id = '".$uid."' group by jobid";
		foreach( $conn->dbh->query($jobsForUserQuery) as $row) {
			array_push($returnArr, $row);
		}
		return $returnArr;
	}
	
	public function getJobNameForJobId($jid) {
	    global $conn;
	    $jobNameExtractQry = "select job_name from tbl_job_master where job_id = ".$jid;
	    $jobNameRes = $conn->dbh->query($jobNameExtractQry);
	    $jobNameResult = $jobNameRes->fetch(PDO::FETCH_ASSOC);
	    return $jobNameResult['job_name']; 
	}

	public function getJobProcessedDetails ($jid) {
		global $conn;
		$returnArr = array();
		$jobProcessedDetailsQuery = "SELECT tsm.tsm_id snippet_id, tsm.job_id jid, tsm.page_no pgnum, ttm.page_no fpgnum, ttm.form_id formid,
		tsm.field_id fldid, tsm.verified_data val, ttm.field_name fldname, tjd.upload_name uin
		FROM tbl_snippets_master tsm join tbl_job_details tjd join tbl_template_master ttm join tbl_image_master tim
		ON tsm.job_id=tjd.job_id and tsm.page_no = tjd.page_no and tjd.form_id = ttm.form_id
		and tjd.form_page_no = ttm.page_no and tjd.form_id = tim.form_id and tjd.form_page_no = tim.page_no
		where tsm.field_id = ttm.field_id and tsm.job_id = '".$jid."' order by pgnum, fldid";
// 		echo $jobProcessedDetailsQuery;exit;
		foreach( $conn->dbh->query($jobProcessedDetailsQuery) as $row) {
			array_push($returnArr, $row);
		}
		return $returnArr;
	}
	
	public function isJobIdValid ($jid, $uid) {
		global $conn;
		$returnArr = array();
		$validateJobIdQuery = "select count(job_id) from tbl_job_master where usr_id='".$uid."' and active = 1 and job_id='".$jid."'";
//		echo $validateJobIdQuery;
		$validateResult = $conn->dbh->query($validateJobIdQuery);
		$results = $validateResult->fetchAll(PDO::FETCH_NUM);
		if(count($results)==1 && $results[0][0] == 1){
			$returnArr['status'] = 1;
		}else{
			$returnArr['status'] = 0;
		}
		return $returnArr;
	}
	
	public function getImageNameFromJobPg ($jid, $pgno, $fldid) {
		global $conn;
		$tsmQuery = "select tsm_id from tbl_snippets_master where job_id ='".$jid."' and page_no ='".$pgno."' and field_id ='".$fldid."'";
		$tsmQueryResult = $conn->dbh->query($tsmQuery);
		$results = $tsmQueryResult->fetchAll(PDO::FETCH_NUM);
		if(count($results)==1){
			return $results[0][0];
		} else{
			return 0;
		}
	}
	
	public function getJobPageImageName ($jid,$pgno) {
	    global $conn;
	    $pgQry = "select saved_name from tbl_job_details where job_id=$jid and page_no = $pgno";
	    $pgRes = $conn->dbh->query($pgQry);
	    $pgResult = $pgRes->fetch(PDO::FETCH_ASSOC);
	    return $pgResult['saved_name'];
	}
	
	public function getJobDetailsForId ($jid) {
	    global $conn;
	    $jobsDetailQuery = "SELECT sum(tjd.processed=2) num_processed_pages,
	    tjm.job_id jobid,
	    tjm.completed iscomplete,
	    tjm.no_pages no_pages
	    FROM tbl_job_master tjm JOIN tbl_job_details tjd ON tjm.job_id=tjd.job_id where
	    tjm.active = 1 and tjd.active = 1 and tjm.job_id = '".$jid."'";
	    $jobsDetailRes = $conn->dbh->query($jobsDetailQuery);
	    $jobsDetailResult = $jobsDetailRes->fetch(PDO::FETCH_ASSOC);
	    return $jobsDetailResult; 
	} 
	
	public function updateValueOfSnippet($user_id,$tsm_id,$val){
	    global $conn;
	    $getPrevVal = "select verified_data from tbl_snippets_master where tsm_id=$tsm_id";
	    $getPrevRes = $conn->dbh->query($getPrevVal);
	    $getPrevResult= $getPrevRes->fetch(PDO::FETCH_ASSOC);
	    
	    $valUpdateQry = "update tbl_snippets_master set verified_data='".htmlentities($val,ENT_QUOTES)."' where tsm_id=$tsm_id";
	    $count = $conn->dbh->exec($valUpdateQry);
	    if($count>0){
	        $logQry = "Insert into tbl_snippetval_update_log (tsm_id,user_id,prev_val,new_val,RecAddDate,RecAddTime) 
	        values ($tsm_id,$user_id,'".$getPrevResult['verified_data']."', '".htmlentities($val,ENT_QUOTES)."',CURDATE(),CURTIME())";
	        $logQryRes = $conn->dbh->query($logQry);
	        return true;
	    }else{
	        return false;
	    }
	}
};