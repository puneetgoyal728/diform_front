<?php
session_start();
include_once ('config.php');
include_once ('db_conn.php');
include_once ('jobResultClass.php');
include_once('common_func.php');
checksession();
if(isset($_SESSION["uid"]) && isset($_POST['data'])){
    $allJobId = json_decode($_POST['data']);
    $returnVal = array();
    $jrc = new JobResultClass;
    foreach ($allJobId as $jobId){
        if($jrc->isJobIdValid($jobId,$_SESSION["uid"])){
            array_push($returnVal,$jrc->getJobDetailsForId($jobId)); 
        }
    }
    echo json_encode($returnVal);
}else{
    echo '0';
}