<?php
include_once 'config.php';
include_once 'db_conn.php';
include_once 'common_func.php';

include_once 'scrypt.php';
class jobclass{

    public function getnewpageno ($jobid) {
        global $conn;
        $p = 1;
        foreach( $conn->dbh->query("SELECT MAX(upload_order) m_np FROM tbl_job_upload_details where job_id = $jobid")  as $row) {
            $p =$row['m_np']+1;
        }
        return $p;
    }

    public function updatejobs ($uploadname,$uid,$f,$p,$flag) {
        global $conn;
        $filetype = getExtension($uploadname);
    foreach( $conn->dbh->query("SELECT count(*) as 'count' FROM tbl_job_master where usr_id=$uid and job_id=$f")  as $row) {$count =$row['count'];}
        if($count==0 && $flag==1)
        {
            $jobquery="INSERT INTO tbl_job_master(usr_id,no_pages,completed,active,recadd_date,recadd_time) VALUES (:uid,1,0,1,CURDATE(),CURTIME())";
            $sth = $conn->dbh->prepare($jobquery);
            $sth->execute(array(':uid'=>$uid)) or die(var_dump($sth->errorInfo()));
            $f = $conn->dbh->lastInsertId();
            $job_name="job ".$f;
            $namefile = $uid."_".$f."_".$p.".".$filetype;
            $updatequery="update tbl_job_master set job_name = :job_name where job_id=:jobid";
            $sth = $conn->dbh->prepare($updatequery);
            $sth->execute(array(':job_name'=>$job_name,':jobid'=>$f)) or die(var_dump($sth->errorInfo()));
            $detailsquery="INSERT INTO tbl_job_upload_details(job_id,upload_order,upload_name) VALUES (:f,:p,:uploadname)";
            $sth = $conn->dbh->prepare($detailsquery);
            $sth->execute(array(':uploadname'=>$uploadname,':f'=>$f,':p'=>$p)) or die(var_dump($sth->errorInfo()));       
        }
        else
        {
            $namefile = $uid."_".$f."_".$p.".".$filetype;
            $updatePrep = $conn->dbh->prepare("update tbl_job_master set no_pages = no_pages+1 where usr_id = $uid and job_id = $f");
            $updatePrep->execute();
            if($updatePrep->rowCount()>0){
                $detailsquery="INSERT INTO tbl_job_upload_details(job_id,upload_order,upload_name) VALUES (:f,:p,:uploadname)";
                $sth = $conn->dbh->prepare($detailsquery);
            $sth->execute(array(':uploadname'=>$uploadname,':f'=>$f,':p'=>$p)) or die(var_dump($sth->errorInfo()));       
        
            }
        }
        return $f;
    }

    public function deletejobs ($jobid,$name) {
        global $conn;
        $conn->dbh->exec("update tbl_job_master set no_pages = no_pages-1 where job_id =$jobid ");
        $query="delete from tbl_job_upload_details where job_id = $jobid and upload_name = '".$name."'";
        $conn->dbh->exec($query);
    }

    public function deletejob ($jobid,$u) {
        global $conn,$dashboard_basepath;
        $conn->dbh->exec("update tbl_job_master set no_pages =0 ,active= 0  where job_id =$jobid ");
        $conn->dbh->exec("delete from tbl_job_upload_details where job_id = $jobid ");
         $hash = Password::hash($u);
         $ip = getenv('HTTP_CLIENT_IP') ? : getenv('HTTP_X_FORWARDED_FOR')?: getenv('HTTP_X_FORWARDED')?: getenv('HTTP_FORWARDED_FOR')?: getenv('HTTP_FORWARDED')?: getenv('REMOTE_ADDR');
         $data='jobid_'.$jobid;
         $url=$dashboard_basepath.'usersC/addUserActivityLog/'.urlencode($hash).'/'.$u.'/'.$ip.'/jobdeleted/'.$data;
         //$rt=file_get_contents($url);
        
    }
};
?>