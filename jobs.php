<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('jobclass.php');
include_once('common_func.php');
checksession();
$uid =$_SESSION["uid"];
$currentHead = "JOBS";

$ark=array();
$flag=1;
if(isset($_GET["jid"]))
{
    $flag=0;
    $jobid=$_GET["jid"];
    $listFormsQuery = "select * from tbl_job_upload_details where job_id=$jobid";
    foreach($conn->dbh->query($listFormsQuery) as $row) {
    $id=$row["upload_order"];
    $img=$jobs_path.$jobid."/".$row["upload_name"];
    $data=array("id"=>$row["upload_order"],"path"=>$jobs_path.$jobid."/".$row["upload_name"],"name"=>$row["upload_name"]); 
    array_push($ark, $data)  ;
    }
    $deg = json_encode( $ark );
}
?>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>Multiple File upload with PHP</title>
</head>
<link type="text/css" rel="stylesheet" href="<?php echo auto_version('/css/default.css');?>"/>
<script type="text/javascript" src="<?php echo auto_version('/js/raphael.min.js');?>"></script>
<script type="text/javascript" src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>
<style>

#popup {
    z-index: 9;
    opacity: 0.92;
    position: absolute;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    background: #F0F0F0;
    display: none;
}

form.pos_fixed {
    width: 400px;
    color: #92AAB0;
    position: fixed;
    top: 30%;
    left: 40%;
    background: #D0D0D0;
    border-radius: 4px;
        box-shadow: 0 0 5px rgba(0,0,0,0.9);
}
</style>

<body>
<?php include("jobsheader.php");?>
<div id="inhead">
    <div class="center">
        <div class="left">
            <h3>Start New Job</h3>
            <a href="draftjobs.php" class="right orgbutton">View Draft Jobs</a>
        </div>
        
        <div class="right">
            
        </div>
        <div class="clear"></div>
    </div>
</div><!---end of inhead--->

<div id="popup">
    <form class="pos_fixed" action="#" id="contact">
    <h3>Enter Job Name</h3>
    <img src="images/close.png" height="40" width="40"  id="cancel" style="position:relative;left: 350px;top:-150px" />
    <textarea id="c_az"></textarea> <input type="button" id="ok" value="Save" /> <br />
    </form>
</div>

<form id="data" style="background-color: #eeeeee;"><input id="clickk"
 name="profileImg[]" type="file" multiple="multiple"
 accept="image/x-png, image/gif, image/jpeg, application/pdf"
 style="visibility: hidden; display: none;" /><br />
</form>

<div id="container" style="background-color: #eeeeee; padding: 0px 0;">
    <div id="job">
        <div id="heading">
           
            <div class="right">
                <button class="button" onclick="deleteselected()">delete selected</button>
                    <button class="button" onclick="deleteall()">delete all</button>
            </div>
            <div class="clear"></div>
        </div>
        <div id="jobin">
            <div class="left">
                <div id="der" style="border: 1px black dotted; margin-top: 10px; overflow: auto; height: 430px;">
                <img src="images/drag.jpg" id="files" height="180" width="700"  style="position:relative;left: 100px;border: 1px black dotted; margin-top: 10px;">
                    <div id="uploadedfiles"></div>
                </div>
            </div>
            <div class="right">

<button class="next" onclick="sendtemp();">
    <span class="pos">Next</span>
    <span class="arrow-next"></span>
</button>
</div>
            <div class="clear"></div>
        </div>
    </div>
  
</div>
<script type="text/javascript">

var counter=0;
var totalcount=0;
var selected=[];
var jobname;
var jobpagelimit=<?php echo $jobpagelimit?>;
var pdfimage ="images/pdf.png";
var jay= <?php echo json_encode($ark ); ?>;
for(i in jay)
{
	prev_data(jay[i]);
}
function getExtension(filename){
    var fileNameArr = filename.split(".");
    var extensionIdx = fileNameArr.length -1;
    return fileNameArr[extensionIdx].toLowerCase();
}

function isAllowedToUpload(fileName){ 
    var allowedFiles = ["jpeg","jpg","png","bmp","pdf"];
    if($.inArray(getExtension(fileName), allowedFiles)>-1){
        return true;
    }else{
        return false;
    }
}

$("#popup #cancel").click(function() {
    $(this).parent().parent().hide();
});

$("#popup #ok").click(function() {
    var n= $('#c_az').val();
    $.ajax({
     type: "POST",
      url: "jobname.php",
      data: { 'name': n, 'job_id':$("#setjobId").val() }
    }).done(function(data) {
    var elem = document.getElementById("mytext");
    window.location.href='sessiontemplate.php?name='+$("#setjobId").val();
    });
});

function sendtemp()
{
 
    if(counter!=0 && <?php echo $flag?> !=0)
    {
        $("#popup").css("display", "block");
        document.getElementById("c_az").value ="job"+jobname;
        
    }
    else if(counter!=0 && <?php echo $flag?> ==0)
    	window.location.href='sessiontemplate.php?name='+$("#setjobId").val();
}
var dropbox = document.getElementById("files");
dropbox.addEventListener("dragenter", dragenter, false);
dropbox.addEventListener("dragover", dragover, false);
dropbox.addEventListener("drop", drop, false);
dropbox.addEventListener("click", oninput, true);

function oninput(){
    document.getElementById("clickk").click();
}
window.addEventListener("dragover",function(e){
 e = e || event;
 e.preventDefault();
},false);
window.addEventListener("drop",function(e){
  e = e || event;
  e.preventDefault();
},false);
function dragenter(e) {
  e.stopPropagation();
  e.preventDefault();
}
function dragover(e) {
  e.stopPropagation();
  e.preventDefault();
}
function drop(e) {
    e.stopPropagation();
    e.preventDefault();
    var filesArray = e.dataTransfer.files;
    sendFile(filesArray,0);
}

$("#data").change(function(e){

    e.stopPropagation();
    e.preventDefault();
    var filesArray = document.getElementById('clickk').files;
    sendFile(filesArray,0);
});

function sendFile(fileArray,idx) {
 if(totalcount<jobpagelimit){
    var uri = "upjobs.php";
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    if(isAllowedToUpload(fileArray[idx].name)){
        showUploadingImage(fileArray[idx]);
        xhr.open("POST", uri, true);
        xhr.onreadystatechange = (function(fileArray,idx) {
            return function(){
                if (xhr.readyState == 4 && xhr.status == 200) {
                   
                    if(xhr.responseText=='0')
                    {
                       window.location.href='logx.php';
                    } 
                    var im =xhr.responseText;
                    im = im.trim();
                    im = JSON.parse(im);
                    if(im.value==-1){ alert(im.msg)}
                    else{
                    jobname=im.count;
                    $("#setjobId").val(im.jobid);
                    $('#loader').remove();
                    ++totalcount;
                    }
                    if((fileArray.length-1) > idx){
                      sendFile(fileArray,++idx);
                    }
                }
            };
        })(fileArray,idx);
        fd.append('myFile', fileArray[idx]);
        var jid = $("#setjobId").val();
        fd.append('jid', jid);
        fd.append('flag',<?php echo $flag;?>);
        xhr.send(fd);
    }
 }
}

function prev_data(file){
    ++counter;
    $("#uploadedfiles").append('<div id="border'+counter+'"  style="float:left;width: 130px; margin-left: 5px;  height: 180px;"><br/><img id="img'+counter+'" src="'+file.path+'" height="130" width="90"  onclick="select('+counter+');"><p id="name'+counter+'" style=" font-size: 15px;">'+file.name+'</p><img src="images/close.png" height="20" width="20"  style="position:relative;left: 80px;top: -160px;" onclick="deleteJob('+counter+');"></div>');
    if(getExtension(file.name)=="pdf")
    	$("#img"+counter).attr({'src':pdfimage});
 }

	   
function showUploadingImage(file){
 ++counter;
 $("#uploadedfiles").append('<div id="border'+counter+'"  style="float:left;width: 130px; margin-left: 5px;  height: 180px;"><br/><img id="img'+counter+'" src="images/blank.png" height="130" width="90"  onclick="select('+counter+');"><img src="images/close.png" height="20" width="20"  style="position:relative;left: -10px;top: -120px;" onclick="deleteJob('+counter+');"><img id="loader" src="images/ajax-loader.gif" height="90" width="65"  style="position:relative;top: -108px;left:15px"><p  id="name'+counter+'" style=" font-size: 15px;">'+file.name+'</p></div>');
 if(getExtension(file.name)!="pdf"){
    	  var reader = new FileReader();
    	     reader.onload = (function (currloop){
    	         var loopIdx = currloop;
    	         return  function (e) {
    	             src= e.target.result;
    	             
    	             $("#img"+counter).attr({'src':src});
    	            
    	         };
    	     })(counter);
    	     reader.readAsDataURL(file);
     }else{
    	 $("#img"+counter).attr({'src':pdfimage});
     }
 
}

function select(id)
{
 if($('#img'+id).css('opacity')==1)
 {
	 $('#img'+id).css('opacity','0.3');
  selected.push(id);
 }
 else
 {
	 $('#img'+id).css('opacity','1');
	 var index = selected.indexOf(id);
	 selected.splice(index, 1);
 }

     }

function deleteselected() { 
 for (var i= 0; i < selected.length; i++) {
      if('#border'+selected[i]!=undefined){
    	  deleteJob(selected[i])
       --totalcount;
      }
 }
	 
}

function deleteJob(id)
{
	var name=$('#name'+id).text();
	var jobid=$("#setjobId").val();
    $.ajax({
        url: "deletejob.php",
        type: "POST",
        data: { 'name': name,'jobid':jobid }
    });
$('#border'+id).remove();
--totalcount;
	 }

</script>
<div class="jobData"><input type="hidden" id="setjobId"
 value="<?php if (isset($_GET['jid'])) {
    echo $_GET['jid'];
}else{
    echo "-1";
}?>" /></div>
</body>
</html>
