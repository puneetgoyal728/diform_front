//default panel
function startpanel () {
	var data = {"title":"Select a field for preview" };
	var template = $.templates("#theTmpl");
	var htmlOutput = template.render(data);
	$("#result").html(htmlOutput);
	$('#dropdown').hide();
	$('#fname').hide();
	$('#sub_title').hide();
	$('#specialinstructions').hide();
}
//
function imageratio(hr, wr) {
	var zr;
	var hr = hr/ 1433;
	var wr = wr/900;

	if (wr > hr) {
		if (hr >= 1 && wr >= 1) {
			imgwid = imgwid / wr;
			imghei = imghei / wr;
			zr = wr;
		} else if (hr <= 1 && wr >= 1) {
			imgwid = imgwid / wr;
			imghei = imghei / wr;
			zr = wr;

		} else {
			imgwid = imgwid / wr;
			imghei = imghei / wr;
			zr = wr;
		}
	} else if (wr < hr) {
		if (hr >= 1 && wr >= 1) {
			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;
		} else if (hr >= 1 && wr <= 1) {
			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;
		} else {
			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;
		}

	}

	else if (hr == wr) {

		if (hr > 1) {
			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;
		}

		else if (hr < 1) {

			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;

		}

	}

	return zr;
}
var d,f;
//ajax requests
function savedata(obj){

	
}


function deletefield(obj){

$.ajax({
	type: "POST",
	url: "deletefield.php",
	data: { 'formid': formid,'field':obj.recid }
	 }).done(function() {
		obj.remove();
		    
		    });
	
}
//left panel data
function showdata(obj,fields) {
	var data=[];
	for(var k in fields)
	{
	var name=obj.field[k].name;
    data.push({id:k});
	}
    var template = $.templates("#theTmpl");
    var htmlOutput = template.render(data);
    $("#result").html(htmlOutput);
    for(var l in fields)
    {
	for(var i in fieldtype)
    	    {
    	   $('#dropselect'+l).append('<option value="'+i+'">'+fieldtype[i]+'</option>');
    	   }
     $('#dropselect'+l).val(obj.field[l].type);
     $('#fname'+l).val(obj.field[l].name);  
     $('#instructions'+l).val(obj.field[l].instructions);
     var datatype=$( "#dropselect"+l ).val();
     document.getElementById('sub_title_option'+l).innerHTML = "";
     $("#sub_title_option"+l).hide();
     for(refindex in fieldSubtypeMap[datatype])
     {
         var index=fieldSubtypeMap[datatype][refindex];
         $('#sub_title_option'+l).append('<option value="'+index+'">'+refsubtype[index]+'</option>');
         $("#sub_title_option"+l).show();
     }
     $('#sub_title_option'+l).val(obj.field[l].sub_type);
     if(datatype=="multiple")
     {
    	 $(".textinput").remove();
         var ar = JSON.parse(obj.field[l].details);

         for( var loi=0;loi<=ar.length ;++loi)
          {
            var wrapper  = $(".multiple_options"+l); 
            var value=ar[loi];
            if(ar[loi]==undefined)
                value="";
            $(wrapper).append('<div  class="options'+l+'" id="textinput'+l+loi+'" ><input id="init'+l+loi+'"  type="text" class="multiOp'+l+' inp_side"   placeholder="Next option"  value="'+value+'"   /><img src = "images/close.png" href="#" id="del'+l+loi+'" class="remove_field'+l+'" img></div>'); //add input box
         
                        $('#del'+l+loi).click( function(e)
                            {
                              e.preventDefault(); $(this).parent('div').remove();
                              savedata(obj);
                           }); 
                        $('#init'+l+loi).bind( "input properychange", { value: l },function(event)
                                {
                        	var k=[];
                            $.each( $('.multiOp'+event.data.value), function( i, objk )
                            {
                              if($.trim(objk.value)!=="")
                                  {
                                   k.push($.trim(objk.value));
                                  }
                            });
                            obj.field[event.data.value].details=JSON.stringify(k);
                            savedata(obj);
                               
                                   }); 
          }
         
         $('#del'+l+(loi-1)).hide();
         $('#init'+l+(loi-1)).bind( "keypress", { value: l },function(event)
                 {
                    if(event.keyCode!=32)
                  applyAddFunct(loi-1,obj.field[event.data.value],event.data.value);
                 });
         } 
     
     $('#dropselect'+l).bind( "change", { value: l },function(event){
         obj.field[event.data.value].type=  $('#dropselect'+event.data.value).val();
    	 var datatype=obj.field[event.data.value].type;
    	 $(".multiple_options"+event.data.value).hide();
    	 $( "#specialinstructions" +event.data.value).show();
    	 $( "#specialinstructions" +event.data.value).val('');
    	 obj.field[event.data.value].details=null;
         document.getElementById('sub_title_option'+event.data.value).innerHTML = "";
         $("#sub_title_option"+event.data.value).hide();
         for(refindex in fieldSubtypeMap[datatype])
         {
             var index=fieldSubtypeMap[datatype][refindex];
             $('#sub_title_option'+event.data.value).append('<option value="'+index+'">'+refsubtype[index]+'</option>');
             $("#sub_title_option"+event.data.value).show();
         }  
         obj.field[event.data.value].sub_type=$( "#sub_title_option"+event.data.value ).val();
         savedata(obj);
         
         if(datatype == "multiple")
         {
        	 obj.field[event.data.value].sub_type=$( "#sub_title_option"+event.data.value ).val();
             $(".multiple_options"+event.data.value).show();
             $( "#specialinstructions" +event.data.value).hide();
             var wrapper = $(".multiple_options"+event.data.value);
             $(wrapper).append('<div class="options'+event.data.value+'" id="textinput'+event.data.value+'0" ><input type="text" name="mytext" id="init'+event.data.value+'0"  class="multiOp'+event.data.value+' inp_side"   placeholder="Option" /><img src="images/close.png" href="#" id="del'+event.data.value+'0"  class="remove_field'+event.data.value+'" img></div>');
             $('#del'+event.data.value+'0').hide();
             $('#init'+event.data.value+'0').keypress( function(e)
              {
                 if(e.keyCode!=32)
               applyAddFunct(0,obj.field[event.data.value],event.data.value);
              });
             $('#init'+event.data.value+'0').bind('input propertychange', function()
                     {
             	var k=[];
                 $.each( $('.multiOp'+event.data.value), function( i, objk )
                 {
                   if($.trim(objk.value)!=="")
                       {
                        k.push($.trim(objk.value));
                       }
                 });
                 obj.field[event.data.value].details=JSON.stringify(k);
                 savedata(obj);
                    
                        }); 
             
         }
     });
     $('#sub_title_option'+l).bind( "change", { value: l },function(event){
         obj.field[event.data.value].sub_type=  $('#sub_title_option'+event.data.value).val();  
         savedata(obj);
     });
     $('#fname'+l).bind('input propertychange', { value: l },function(event){
         obj.field[event.data.value].name=  $('#fname'+event.data.value).val(); 
         savedata(obj);
     });
     $('#instructions'+l).bind( "change", { value: l },function(event){
         obj.field[event.data.value].instructions=  $('#instructions'+event.data.value).val();  
         savedata(obj);
     });
    
    }

}

//add multiple data
function applyAddFunct(id,objfield,key)
{
    var wrapper         = $(".multiple_options"+key);
    $('#init'+key+id).unbind( "keypress" );
    $('#del'+key+id).show();
    id++;
    $(wrapper).append('<div class="options'+key+'" id="textinput'+key+id+'" ><input type="text" name="mytext" id="init'+key+id+'"  class="multiOp'+key+' inp_side"   placeholder="Next option" /><img  href="#" src = "images/close.png" id="del'+key+id+'"  class="remove_field'+key+'" img></div>');
    $('#del'+key+id).hide();
    $('#init'+key+id).keypress( function(e)
        {
              if(e.keyCode!=32)
              applyAddFunct(id,objfield,key);
              
        });
    $('#init'+key+id).bind('input propertychange', function()
            {
    	var k=[];
        $.each( $('.multiOp'+key), function( i, obj )
        {
          if($.trim(obj.value)!=="")
              {
               k.push($.trim(obj.value));
              }
        });
        objfield.details=JSON.stringify(k);
           
               }); 
    $('#del'+key+id).click( function(e)
        {
          e.preventDefault(); $(this).parent('div').remove();
          var k=[];
          $.each( $('.multiOp'+key), function( i, obj )
          {
            if($.trim(obj.value)!=="")
                {
                 k.push($.trim(obj.value));
                }
          });
          objfield.details=JSON.stringify(k);
        });
        
}
//field details object 
function fielddetails(arr) {
	this.name=arr["name"];
    this.type=arr["type"];
    this.sub_type=arr["sub_type"];
    this.details=arr["details"];
    this.instructions=arr["instructions"];
 }
//table object
function table (arr,ratio,flag) {
    var resx = arr["coordinates"].split(",");
    this.recs=paper.rect((resx[0].trim())/ratio,(resx[1].trim())/ratio,((resx[2])/ratio)-(resx[0]/ratio),((resx[3]/ratio))-(resx[1]/ratio));
    this.delicon=paper.image(delimage,this.recs.attr('x')+this.recs.attr('width')-10,this.recs.attr('y')-10,20,20);
    this.recs.attr("fill", "BLUE");
    this.recs.attr("fill-opacity", "0.10");
    this.istable=arr["istable"];
    this.recid=arr["id"];
    this.field=[];
    if(arr["prevflag"]==1){
    	var u=JSON.parse(arr["type"]);
    	var v=JSON.parse(arr["sub_type"]);
    	var w=JSON.parse(arr["name"]);
    	var d=JSON.parse(arr["details"]);
    	for(var i in u )
    		{
    		
    		 var  defaultfield=[{"name":w[i],"type" : u[i],"sub_type" : v[i],"details" : d[i],"instructions" : ""}];
        var f = new fielddetails(defaultfield[0]);
        this.field.push(f);
    		}
    }
    else{
    var  defaultfield=[{"name":"field "+this.field.length,"type" : "text","sub_type" : "small","details" : "","instructions" : ""}];
    var f = new fielddetails(defaultfield[0]);
    this.field.push(f);
    }
    var id=count;
    this.col = paper.set();
    this.row = paper.set();
    this.addcol=paper.image(addcol,resx[0],160,20,20);
    this.addrow=paper.image(addcol,resx[0],190,20,20);
    this.delcol=paper.image(delcol,resx[0],210,20,20);
    this.delrow=paper.image(delcol,resx[0],240,20,20);

    this.showdetails = function()
    {
        showdata(fields[id],this.field);
        }
    this.addnewcolumn = function(prevcol)
    {
        var recx = fields[id].recs.attr('x');
        var recy = fields[id].recs.attr('y');
        var recw = fields[id].recs.attr('width');
        var rech = fields[id].recs.attr('height');
        var last_col=recx;
        
        for(var j = 0; j < fields[id].col.length; j++) 
        {
            
            fields[id].col[j].attr({opacity:30});
            if(fields[id].col[j].attr('x')>last_col)
            last_col= fields[id].col[j].attr('x');
        }
        if(last_col+30<recx+recw-10)
        {
		   if(prevcol!=0)
			   last_col=prevcol-30;
		   var elem=paper.rect(last_col+30,recy,1,rech);
		   elem.attr({opacity:1});
		   elem.mousemove(changeCursorcol);
		   elem.drag(dragMove, dragStart, dragEnd);
		   fields[id].col.push(elem);
		   fields[id].addcol.attr({x:last_col+30,y:recy});
		   fields[id].delcol.attr({x:last_col+30,y:recy+20});
		   if(prevcol==0){
			   var  defaultfield=[{"name":"field "+this.field.length,"type" : "text","sub_type" : "small","details" : "","instructions" : ""}];
			   var f = new fielddetails(defaultfield[0]);
			   this.field.push(f);
			   fields[id].showdetails();
			   savedata(fields[id]);
			   }
	   
        }
        else
            alert('not enough space');
   }
  
    this.addnewrow = function(prevrow)
    {
        var recx = fields[id].recs.attr('x');
        var recy = fields[id].recs.attr('y');
        var recw = fields[id].recs.attr('width');
        var rech = fields[id].recs.attr('height');
        var last_row=recy;
        
        for(var j = 0; j < fields[id].row.length; j++) 
        {
            
            fields[id].row[j].attr({opacity:30});
            if(fields[id].row[j].attr('y')>last_row)
                
            last_row= fields[id].row[j].attr('y');
        }
        
        if(last_row<recy+rech-20)
        {
        	if(prevrow!=0)
        		last_row=prevrow-20;
       var elem=paper.rect(recx,last_row+20,recw,1);
       elem.mousemove(changeCursorrow);
       elem.drag(dragMove, dragStart, dragEnd);
       elem.attr({opacity:1});
       fields[id].row.push(elem);
       fields[id].addrow.attr({x:recx+3,y:last_row+20});
       fields[id].delrow.attr({x:recx+23,y:last_row+20});
       if(prevrow==0)
       savedata(fields[id]);
        }
        else
            alert('not enough space');
        }

    this.deletecolumn = function()
    {
        var elem =fields[id].col.pop();
        elem.remove();
        var last_col=fields[id].recs.attr('x');
       
        for(var j = 0; j < fields[id].col.length; j++) 
        {
            
            if(fields[id].col[j].attr('x')>last_col)
            last_col= fields[id].col[j].attr('x');
        }
        
       fields[id].addcol.attr({x:last_col,y:fields[id].recs.attr('y')});
       fields[id].delcol.attr({x:last_col,y:fields[id].recs.attr('y')+20});
       fields[id].field.pop();
       fields[id].showdetails();
        }
    this.deleterow = function()
    {
        var elem =fields[id].row.pop();
        elem.remove();
        var last_row=fields[id].recs.attr('y');
        for(var j = 0; j < fields[id].row.length; j++) 
        {
            if(fields[id].row[j].attr('y')>last_row)
                
            last_row= fields[id].row[j].attr('y');
        }
       fields[id].addrow.attr({x:fields[id].recs.attr('x')+3,y:last_row});
       fields[id].delrow.attr({x:fields[id].recs.attr('x')+23,y:last_row});

        }


    
    this.addcol.click(function (event, a, b) {
        fields[id].addnewcolumn(0);
     });
    this.addrow.click(function (event, a, b) {
        fields[id].addnewrow(0);
     });
    this.delcol.click(function (event, a, b) {
        fields[id].deletecolumn();
     });
    this.delrow.click(function (event, a, b) {
        fields[id].deleterow();
     });
    this.remove=function(){
    	this.recs.remove();
		 this.delicon.remove();
		 this.addcol.remove();
        this.addrow.remove();
        this.delcol.remove();
        this.delrow.remove();
        this.col.remove();
        this.row.remove();
        }
    this.recs.click(function () {
        showdata(fields[id],fields[id].field);
        savedata(fields[id]);
       });
    this.delicon.click(function () {
    	deletefield(fields[id]);
       });
    var rec_details;
    var col_details=[];
    var row_details=[];
    var dragStart = function() {
        rec_details=[{'x' : fields[id].recs.attr('x'),'y' : fields[id].recs.attr('y'),'w' : fields[id].recs.attr('width'),'h': fields[id].recs.attr('height')}];
           for(var j = 0; j < fields[id].col.length; j++) 
                   col_details[j]=[{'x' : fields[id].col[j].attr('x'),'y' : fields[id].col[j].attr('y'),'w' : fields[id].col[j].attr('width'),'h' : fields[id].col[j].attr('height')}];
                  
           for(var j = 0; j < fields[id].row.length; j++) 
                   row_details[j]=[{'x' : fields[id].row[j].attr('x'),'y' : fields[id].row[j].attr('y'),'w' : fields[id].row[j].attr('width'),'h' : fields[id].row[j].attr('height')}];
           rox=this.attr('x');
           roy=this.attr('y');
           delicon=fields[id].delicon;
           addcol_details=[{'x' :fields[id].addcol.attr('x'),'y' :fields[id].addcol.attr('y')}];
           addrow_details=[{'x' :fields[id].addrow.attr('x'),'y' :fields[id].addrow.attr('y')}];
       this.dragging = true;
    };


    function dragMove(dx, dy) {
        // Inspect cursor to determine which resize/move process to use
        switch (this.attr('cursor')) {

            case 'nw-resize' :
                if(this.oh-dy > 20) 
                this.attr({y: this.oy + dy, height: this.oh - dy});
                if( this.ow -dx >20)
                    this.attr({x: this.ox + dx, width: this.ow - dx}); 
                var wid =this.ow;
                delicon.attr({x: this.ox -5+wid, y: this.oy + dy-15});
                break;

            case 'ne-resize' :
                if(this.ow +dx >20){ 
                this.attr({width: this.ow + dx });
                var wid =this.ow;
                delicon.attr({x: this.ox + dx-5+wid});
                }
                if(this.oh-dy > 20 ){
                    this.attr({y: this.oy + dy , height: this.oh - dy});
                var wid =this.ow;
                delicon.attr({y: this.oy + dy-15});
                }
                break;

            case 'se-resize' :
            	for(var j = 0; j < fields[id].col.length; j++) 
                {
            		var lastcol=fields[id].col[j].attr('x');
                }
            	
            		
            	
               if(rec_details[0]['x'] +rec_details[0]['w'] +dx-20>lastcol)
           		{
	                fields[id].recs.attr({ width:rec_details[0]['w'] + dx,height:rec_details[0]['h'] + dy});
	                for(var j = 0; j < fields[id].col.length; j++) 
	                {
	                    fields[id].col[j].attr({height:col_details[j][0]['h'] + dy});
	                }
	                for(var j = 0; j < fields[id].row.length; j++) 
	                {
	                    fields[id].row[j].attr({ width:row_details[j][0]['w'] + dx});
	                }  
	                
           		}
                break;

            case 'sw-resize' :
            	if(fields[id].col[0]!=undefined)
                	 var firstcol= fields[id].col[0].attr('x');
                if(rec_details[0]['x'] + dx+10<firstcol)
            		{
	                fields[id].recs.attr({x:rec_details[0]['x'] + dx, width:rec_details[0]['w'] - dx,height:rec_details[0]['h'] + dy});
	                for(var j = 0; j < fields[id].col.length; j++) 
	                {
	                    fields[id].col[j].attr({height:col_details[j][0]['h'] + dy});
	                }
	                for(var j = 0; j < fields[id].row.length; j++) 
	                {
	                    fields[id].row[j].attr({x:row_details[j][0]['x'] + dx, width:row_details[j][0]['w'] - dx});
	                }  
	                fields[id].addrow.attr({x:addrow_details[0]['x']+dx});  
	                fields[id].delrow.attr({x:addrow_details[0]['x']+dx+20});
            		}
                break;

             case 's-resize' :
            	 for(var j = 0; j < fields[id].row.length; j++) 
                 {
             		var lastrow=fields[id].row[j].attr('y');
                 }
                if(rec_details[0]['y'] +rec_details[0]['h']+dy-20>lastrow)
            		{
	                fields[id].recs.attr({height:rec_details[0]['h'] + dy});
	                for(var j = 0; j < fields[id].col.length; j++) 
	                {
	                    fields[id].col[j].attr({height:col_details[j][0]['h'] + dy});
	                }
            		}
                break;
                        
             case 'n-resize' :
                 if(this.oh-dy > 20){
                    this.attr({ y: this.oy + dy, height: this.oh - dy});
                    var wid =this.ow;
                    delicon.attr({y: this.oy + dy-15,x: this.ox -5+wid});
                    }
                 break;

             case 'w-resize' :
                 if(this.ow-dx > 20){
                     this.attr({ x: this.ox + dx,width: this.ow - dx});
                    var wid =this.ow;
                    delicon.attr({x: this.ox -5+wid,y:this.oy-15});
                 }
                break;
                
              case 'e-resize' :
                  var wid =this.ow;
                 if( this.ow +dx >20){
                this.attr({ width: this.ow + dx});
                delicon.attr({y: this.oy -15,x: this.ox + dx-5+wid });
                }
                break;

              case 'col-resize' :
                  this.attr({x:rox+dx}); 
                  if(this.attr('opacity')==1)
                	  {
                	  fields[id].addcol.attr({x:addcol_details[0]['x']+dx}); 
                	  fields[id].delcol.attr({x:addcol_details[0]['x']+dx}); 
                	  }
                break;


              case 'row-resize' :
                 this.attr({y:roy+dy}); 
                 if(this.attr('opacity')==1)
                	 {
                 fields[id].addrow.attr({y:addrow_details[0]['y']+dy}); 
                 fields[id].delrow.attr({y:addrow_details[0]['y']+dy}); 
                	 }
                break;
                

             default :
                 fields[id].recs.attr({x:rec_details[0]['x'] + dx, y:rec_details[0]['y'] + dy});
             for(var j = 0; j < fields[id].col.length; j++) 
             {
                 fields[id].col[j].attr({x:col_details[j][0]['x'] + dx, y:col_details[j][0]['y'] + dy});
             }
             for(var j = 0; j < fields[id].row.length; j++) 
             {
                 fields[id].row[j].attr({x:row_details[j][0]['x'] + dx, y:row_details[j][0]['y'] + dy});
             }
             
             fields[id].addcol.attr({x:addcol_details[0]['x']+dx,y:addcol_details[0]['y']+dy});   
             fields[id].addrow.attr({x:addrow_details[0]['x']+dx,y:addrow_details[0]['y']+dy});  
             fields[id].delcol.attr({x:addcol_details[0]['x']+dx,y:addcol_details[0]['y']+dy+20});   
             fields[id].delrow.attr({x:addrow_details[0]['x']+dx+20,y:addrow_details[0]['y']+dy}); 
             fields[id].delicon.attr({x:rec_details[0]['x']+dx+rec_details[0]['w'],y:rec_details[0]['y']+dy-10});
             break;
            }
    }
    var dragEnd = function() {
        this.dragging = false;
        savedata(fields[id]);
       
    };

    var changeCursorcol = function(e, mouseX, mouseY) {
        this.attr('cursor', 'col-resize');
      };

    var changeCursorrow = function(e, mouseX, mouseY) {
        this.attr('cursor', 'row-resize');
    };

    var changeCursor = function(e, mouseX, mouseY) {
        
        // Don't change cursor during a drag operation
        if (this.dragging === true) {
            return;
        }

        // X,Y Coordinates relative to shape's orgin
        var relativeX = mouseX - $('#paper').offset().left - this.attr('x');
        var relativeY = mouseY - $('#paper').offset().top - this.attr('y');

        var shapeWidth = this.attr('width');
        var shapeHeight = this.attr('height');

        var resizeBorder = 5;

        // Change cursor
        if (relativeX < resizeBorder && relativeY < resizeBorder) { 
            this.attr('cursor', 'nw-resize');
                       }
                     
        else if (relativeX > shapeWidth-10 && relativeY < 10 )  
            this.attr('cursor', 'ne-resize');
        else if (relativeX < resizeBorder +5   && relativeY < resizeBorder+5) 
             this.attr('cursor', 'nw-resize');
        else if (relativeX > shapeWidth-resizeBorder -5 && relativeY > shapeHeight-4) { 
            this.attr('cursor', 'se-resize');
        } else if (relativeX < resizeBorder +5 && relativeY > shapeHeight-4) { 
            this.attr('cursor', 'sw-resize');
                     } else if (relativeY < resizeBorder &&  relativeX < shapeWidth-resizeBorder ) { 
            this.attr('cursor', 'n-resize');
                     } else if (relativeY > shapeHeight-resizeBorder ) { 
            this.attr('cursor', 's-resize');
                     } else if ( relativeX < resizeBorder) { 
            this.attr('cursor', 'w-resize');
                     } else if (relativeX > shapeWidth-resizeBorder && relativeY > 10 ) { 
            this.attr('cursor', 'e-resize');
        }else { 
            this.attr('cursor', 'move');
        }
    };

    this.recs.mousemove(changeCursor);
    this.recs.drag(dragMove, dragStart, dragEnd);
 
    
}
//field object
function normalfield (arr,ratio,flag) {
	var zr=ratio;
    var resx = arr["coordinates"].split(",");
    this.recs=paper.rect((resx[0].trim())/zr,(resx[1].trim())/zr,((resx[2])/zr)-(resx[0]/zr),((resx[3]/zr))-(resx[1]/zr));
    this.recs.attr("fill", "BLUE");
    this.recs.attr("fill-opacity", "0.10");
    this.istable=arr["istable"];
    this.recid=arr["id"];
    this.field=[];
    var f=new fielddetails(arr);
    this.field.push(f);
    this.delicon=paper.image(delimage,this.recs.attr('x')+this.recs.attr('width')-10,this.recs.attr('y')-10,20,20);
    var id=count;
    var delicon=this.delicon;
   
    
    this.remove=function(){
        this.recs.remove();
        this.delicon.remove();
        }
    this.recs.click(function () {
    	showdata(fields[id],fields[id].field);
    	savedata(fields[id]);
       });
    this.delicon.click(function () {
       deletefield(fields[id]);
       });
    this.showdetails = function(){
    	showdata(fields[id],fields[id].field);
        }

//field drag function
var dragStart = function() {
	// Save some starting values
	this.ox = this.attr('x');
	this.oy = this.attr('y');
	this.ow = this.attr('width');
	this.oh = this.attr('height');
	this.dragging = true;
};

function dragMove(dx, dy) {
	// Inspect cursor to determine which resize/move process to use
	switch (this.attr('cursor')) {

	case 'nw-resize':
		if (this.oh - dy > 20)
			this.attr({y : this.oy + dy,height : this.oh - dy});
		if (this.ow - dx > 20)
			this.attr({x : this.ox + dx,width : this.ow - dx});
		var wid = this.ow;
		delicon.attr({x : this.ox - 5 + wid,y : this.oy + dy - 15});
		break;

	case 'ne-resize':
		if (this.ow + dx > 20) {
			this.attr({width : this.ow + dx});
			var wid = this.ow;
			delicon.attr({x : this.ox + dx - 5 + wid});
		}
		if (this.oh - dy > 20) {
			this.attr({y : this.oy + dy,height : this.oh - dy});
			var wid = this.ow;
			delicon.attr({y : this.oy + dy - 15});
		}
		break;

	case 'se-resize':
		if (this.oh + dy > 20) {
			this.attr({height : this.oh + dy});
		}
		if (this.ow + dx > 20)
			this.attr({width : this.ow + dx});
		var wid = this.ow;
		delicon.attr({x : this.ox + dx - 5 + wid});
		break;

	case 'sw-resize':
		if (this.ow - dx > 20)
			this.attr({x : this.ox + dx,width : this.ow - dx});
		if (this.oh + dy > 20)
			this.attr({height : this.oh + dy});
		var wid = this.ow;
		delicon.attr({x : this.ox - 5 + wid});
		break;

	case 's-resize':
		if (this.oh + dy > 20) {
			this.attr({height : this.oh + dy});
		}
		var wid = this.ow;
		delicon.attr({x : this.ox - 5 + wid,y : this.oy - 15});
		break;

	case 'n-resize':
		if (this.oh - dy > 20) {
			this.attr({y : this.oy + dy,height : this.oh - dy});
			var wid = this.ow;
			delicon.attr({y : this.oy + dy - 15,x : this.ox - 5 + wid});
		}
		break;

	case 'w-resize':
		if (this.ow - dx > 20) {
			this.attr({x : this.ox + dx,width : this.ow - dx});
			var wid = this.ow;
			delicon.attr({x : this.ox - 5 + wid,y : this.oy - 15});
		}
		break;

	case 'e-resize':
		var wid = this.ow;
		if (this.ow + dx > 20) {
			this.attr({width : this.ow + dx});
			delicon.attr({y : this.oy - 15,x : this.ox + dx - 5 + wid});
		}
		break;

	default:

		this.attr({x : this.ox + dx,y : this.oy + dy});
		var wid = this.ow;
		delicon.attr({x : this.ox + dx - 5 + wid,y : this.oy + dy - 15});
		break;
	}
};

var dragEnd = function() {
	this.dragging = false;
	savedata(fields[id]);
};
var changeCursor = function(e, mouseX, mouseY) {

	if (this.dragging === true) {
		return;
	}

	// X,Y Coordinates relative to shape's orgin
	var relativeX = mouseX - $('#paper').offset().left - this.attr('x');
	var relativeY = mouseY - $('#paper').offset().top - this.attr('y');

	var shapeWidth = this.attr('width');
	var shapeHeight = this.attr('height');

	var resizeBorder = 4;

	// Change cursor
	if (relativeX < resizeBorder && relativeY < resizeBorder) {
		this.attr('cursor', 'nw-resize');
	} else if (relativeX > shapeWidth - resizeBorder
			&& relativeY < resizeBorder) {
		this.attr('cursor', 'delete');
	} else if (relativeX > shapeWidth - 10 && relativeY < 10)
		this.attr('cursor', 'ne-resize');
	else if (relativeX < resizeBorder + 5 && relativeY < resizeBorder + 5)
		this.attr('cursor', 'nw-resize');
	else if (relativeX > shapeWidth - resizeBorder - 5
			&& relativeY > shapeHeight - 4) {
		this.attr('cursor', 'se-resize');
	} else if (relativeX < resizeBorder + 5 && relativeY > shapeHeight - 4) {
		this.attr('cursor', 'sw-resize');
	} else if (relativeY < resizeBorder
			&& relativeX < shapeWidth - resizeBorder) {
		this.attr('cursor', 'n-resize');
	} else if (relativeY > shapeHeight - resizeBorder) {
		this.attr('cursor', 's-resize');
	} else if (relativeX < resizeBorder) {
		this.attr('cursor', 'w-resize');
	} else if (relativeX > shapeWidth - resizeBorder && relativeY > 10) {
		this.attr('cursor', 'e-resize');
	} else {
		this.attr('cursor', 'move');
	}
};
this.recs.mousemove(changeCursor);
this.recs.drag(dragMove, dragStart, dragEnd);

}