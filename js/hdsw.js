$(document).ready(function() {
	$("#tabpro").click(function() {
		$("#account").hide();
		$("#profile").show();
		$(this).css("border-bottom", "1px #eeeeee solid");
		$("#tabacc").css("border-bottom", "1px #cccccc solid");
	});
	$("#tabacc").click(function() {
		$("#profile").hide();
		$("#account").show();
		$(this).css("border-bottom", "1px #eeeeee solid");
		$("#tabpro").css("border-bottom", "1px #cccccc solid");
	});
});