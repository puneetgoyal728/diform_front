$(document).ready(function() {
	$("#one").click(function() {
		$("#one_img").show();
		$("#two_img").hide();
		$("#three_img").hide();
		$(this).addClass("current");
		$(this).removeClass("hover");
		$("#two").addClass("hover");
		$("#three").addClass("hover");
	});

	$("#two").click(function() {
		$("#one_img").hide();
		$("#two_img").show();
		$("#three_img").hide();
		$(this).addClass("current");
		$(this).removeClass("hover");
		$("#one").addClass("hover");
		$("#three").addClass("hover");
	});
	$("#three").click(function() {
		$("#one_img").hide();
		$("#two_img").hide();
		$("#three_img").show();
		$(this).addClass("current");
		$(this).removeClass("hover");
		$("#one").addClass("hover");
		$("#two").addClass("hover");
	});
});