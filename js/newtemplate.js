//common functions
function getExtension(filename){
    var fileNameArr = filename.split(".");
    var extensionIdx = fileNameArr.length -1;
    return fileNameArr[extensionIdx].toLowerCase();
}

//Events on dropbox

function oninput(){
  document.getElementById("clickk").click();
}

function dragenter(event) {
  event.stopPropagation();
  event.preventDefault();
}

function dragover(event) {
  event.stopPropagation();
  event.preventDefault();
}

function drop(event) {
  event.stopPropagation();
  event.preventDefault();
  var filesArray = event.dataTransfer.files;
  sendFile(filesArray,0);
}
window.addEventListener("dragover",function(event){
    event.preventDefault();
},false);
window.addEventListener("drop",function(event){
    event.preventDefault();
},false);

//Sending files to server

function sendFile(fileArray,idx) 
{
    var uri = "uploadpages.php";
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    create(fileArray[idx]);
    xhr.open("POST", uri, true);
    xhr.onreadystatechange = (function(fileArray,idx)
    {
        return function()
        {
            if (xhr.readyState == 4 && xhr.status == 200)
    	    {
                   var im =xhr.responseText;
                   im = im.trim();
                   im = JSON.parse(im);
                   if(im.value=="-1")
                	   alert(im.name+" is not valid file");
            	   else{
            		   if(im.ispdf==1){
            			   $('#pdffile').remove();
            			   for(i in im.images){
            				   $("#uploadedfiles").append('<div id="file'+counter+'" style="float:left;width: 130px; margin-left: 5px;  height: 180px;"><br/><img id="img'+counter+'" src="'+im.images[i].dbimg+'" height="130" width="90" /><p id="pagefont'+counter+'" style=" color:black;opacity:0.5;font-size: 80px;position:relative;left: 20px;top:-104px">'+(netc+1)+'</p><img src="images/close.png" height="20" width="20"  style="position:relative;left: 80px;top: -230px;" onclick="deletepage('+counter+');"/><p style=" color:black;opacity:0.5;font-size: 13px;position:relative;left: 0px;top:-104px">'+im.images[i].userimg+'</p></div>');   
            				   ++counter;
            				   ++netc;
            				   formid=im.formid;
                   		       $('#countervalue').text(netc+1);
            			   }
            			   
            			   
            		   }
            		   else {
            		   testvar =im;
            		   $('#imgname'+counter).text(im.images[0].userimg);
            		    ++counter;
            		    ++netc;
            		    formid=im.formid;
            		    $('#countervalue').text(netc+1);
            		    $('#loader').remove();
            		   }
            	   }
                   if((fileArray.length-1) > idx)
                   {
                	    sendFile(fileArray,++idx);
            	    }
                 }
                };
             })(fileArray,idx);
   
   fd.append('myFile', fileArray[idx]);
   fd.append('formid', formid);
   xhr.send(fd);
   

}

//Generating Image for files


function create(file)
{
	if(getExtension(file.name)!="pdf"){
		$("#uploadedfiles").append('<div id="file'+counter+'" style="float:left;width: 130px; margin-left: 5px;  height: 180px;"><br/><img id="img'+counter+'" src="'+file+'" height="130" width="90" /><p id="pagefont'+counter+'" style=" color:black;opacity:0.5;font-size: 80px;position:relative;left: 20px;top:-104px">'+(netc+1)+'</p><img src="images/close.png" height="20" width="20"  style="position:relative;left: 80px;top: -230px;" onclick="deletepage('+counter+');"/><p id="imgname'+counter+'" style=" color:black;opacity:0.5;font-size: 13px;position:relative;left: 0px;top:-104px"></p><img  id="loader" src="images/ajax-loader.gif" height="80" width="60" style="position:relative;left: 10px;top:-225px"/></div>');
		var reader = new FileReader();
		reader.onload = (function (currloop){
			var loopIdx = currloop;
			return  function (e) {
            src= e.target.result;
            $("#img"+counter).attr('src',src);
        };
    })(counter);
    reader.readAsDataURL(file);
	}
	else{
		 $("#uploadedfiles").append('<div id="pdffile" style="float:left;width: 130px; margin-left: 5px;  height: 180px;"><br/><img  src="'+pdfimage+'" height="130" width="90" /><img  src="images/ajax-loader.gif" height="80" width="60" style="position:relative;left: 13px;top:-104px"/></div>');
	}
	

}

//Assiging properties to images

function addname(name)
{
	
}

//
function deletepage(id){
	
	
	var i=id;
	$('#file'+id).remove();
	--netc;
	$('#countervalue').text(netc+1);
	for(;i<=counter;++i){
		var j=$('#pagefont'+(i+1)).text();
		$('#pagefont'+(i+1)).text(j-1)
	}
		
	  $.ajax({
	         type: "POST",
	         url: "deletepage.php",
	         data: { 'formid': formid ,'page' : (id+1)}
	     }).done(function(data){
	         if(data == 1){
	           
	             }
	         });
	
}

//
