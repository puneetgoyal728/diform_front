//default panel
function startpanel () {
	var data = {"title":"Select a field for preview" };
	var template = $.templates("#theTmpl");
	var htmlOutput = template.render(data);
	$("#result").html(htmlOutput);
	$('#dropdown').hide();
	$('#fname').hide();
	$('#sub_title').hide();
	$('#specialinstructions').hide();
}
//
function imageratio(hr, wr) {
	var zr;
	var hr = hr/ 1433;
	var wr = wr/900;

	if (wr > hr) {
		if (hr >= 1 && wr >= 1) {
			imgwid = imgwid / wr;
			imghei = imghei / wr;
			zr = wr;
		} else if (hr <= 1 && wr >= 1) {
			imgwid = imgwid / wr;
			imghei = imghei / wr;
			zr = wr;

		} else {
			imgwid = imgwid / wr;
			imghei = imghei / wr;
			zr = wr;
		}
	} else if (wr < hr) {
		if (hr >= 1 && wr >= 1) {
			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;
		} else if (hr >= 1 && wr <= 1) {
			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;
		} else {
			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;
		}

	}

	else if (hr == wr) {

		if (hr > 1) {
			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;
		}

		else if (hr < 1) {

			imgwid = imgwid / hr;
			imghei = imghei / hr;
			zr = hr;

		}

	}

	return zr;
}
var d,f;
//ajax requests
function savedata(obj){

	
}


function deletefield(obj){

$.ajax({
	type: "POST",
	url: "deletefield.php",
	data: { 'formid': formid,'field':obj.recid }
	 }).done(function() {
		obj.remove();
		    
		    });
	
}
//left panel data
function showdata(obj,fields) {
	var data=[];
	for(var k in fields)
	{
	var name=obj.field[k].name;
    data.push({id:k});
	}
    var template = $.templates("#theTmpl");
    var htmlOutput = template.render(data);
    $("#result").html(htmlOutput);
    for(var l in fields)
    {
	for(var i in fieldtype)
    	    {
    	   $('#dropselect'+l).append('<option value="'+i+'">'+fieldtype[i]+'</option>');
    	   }
     $('#dropselect'+l).val(obj.field[l].type);
     $('#fname'+l).val(obj.field[l].name);  
     $('#instructions'+l).val(obj.field[l].instructions);
     var datatype=$( "#dropselect"+l ).val();
     document.getElementById('sub_title_option'+l).innerHTML = "";
     $("#sub_title_option"+l).hide();
     for(refindex in fieldSubtypeMap[datatype])
     {
         var index=fieldSubtypeMap[datatype][refindex];
         $('#sub_title_option'+l).append('<option value="'+index+'">'+refsubtype[index]+'</option>');
         $("#sub_title_option"+l).show();
     }
     $('#sub_title_option'+l).val(obj.field[l].sub_type);
     if(datatype=="multiple")
     {
    	 $('#specialinstructions'+l).hide();
    	 $(".textinput").remove();
         var ar = JSON.parse(obj.field[l].details);
         var wrapper  = $(".multiple_options"+l); 
         $(wrapper).append('<p>options</p>');
         for( var loi=0;loi<ar.length ;++loi)
          {
            var value=ar[loi];
            if(ar[loi]==undefined)
                value="";
            $(wrapper).append('<div  class="options'+l+'" id="textinput'+l+loi+'" ><input id="init'+l+loi+'"  type="text" class="multiOp'+l+' inp_side"   placeholder="Next option"  value="'+value+'"   disabled/></div>'); //add input box
         
          }
         
      
         } 

    
    }

}


//field details object 
function fielddetails(arr) {
	this.name=arr["name"];
    this.type=arr["type"];
    this.sub_type=arr["sub_type"];
    this.details=arr["details"];
    this.instructions=arr["instructions"];
 }
//table object
function table (arr,ratio,flag) {
    var resx = arr["coordinates"].split(",");
    this.recs=paper.rect((resx[0].trim())/ratio,(resx[1].trim())/ratio,((resx[2])/ratio)-(resx[0]/ratio),((resx[3]/ratio))-(resx[1]/ratio));
    
    this.recs.attr("fill", "BLUE");
    this.recs.attr("fill-opacity", "0.10");
    this.istable=arr["istable"];
    this.recid=arr["id"];
    this.field=[];
    if(arr["prevflag"]==1){
    	var u=JSON.parse(arr["type"]);
    	var v=JSON.parse(arr["sub_type"]);
    	var w=JSON.parse(arr["name"]);
    	var d=JSON.parse(arr["details"]);
    	for(var i in u )
    		{
    		
    		 var  defaultfield=[{"name":w[i],"type" : u[i],"sub_type" : v[i],"details" : d[i],"instructions" : ""}];
        var f = new fielddetails(defaultfield[0]);
        this.field.push(f);
    		}
    }
    else{
    var  defaultfield=[{"name":"field "+this.field.length,"type" : "text","sub_type" : "small","details" : "","instructions" : ""}];
    var f = new fielddetails(defaultfield[0]);
    this.field.push(f);
    }
    var id=count;
    this.col = paper.set();
    this.row = paper.set();
  

    this.showdetails = function()
    {
        showdata(fields[id],this.field);
        }
    this.addnewcolumn = function(prevcol)
    {
        var recx = fields[id].recs.attr('x');
        var recy = fields[id].recs.attr('y');
        var recw = fields[id].recs.attr('width');
        var rech = fields[id].recs.attr('height');
        var last_col=recx;
        
        for(var j = 0; j < fields[id].col.length; j++) 
        {
            
            fields[id].col[j].attr({opacity:30});
            if(fields[id].col[j].attr('x')>last_col)
            last_col= fields[id].col[j].attr('x');
        }
        if(last_col+30<recx+recw-10)
        {
		   if(prevcol!=0)
			   last_col=prevcol-30;
		   var elem=paper.rect(last_col+30,recy,1,rech);
		   elem.attr({opacity:1});
		  
		   fields[id].col.push(elem);
		   
		   if(prevcol==0){
			   var  defaultfield=[{"name":"field "+this.field.length,"type" : "text","sub_type" : "small","details" : "","instructions" : ""}];
			   var f = new fielddetails(defaultfield[0]);
			   this.field.push(f);
			   fields[id].showdetails();
			   savedata(fields[id]);
			   }
	   
        }
        else
            alert('not enough space');
   }
  
    this.addnewrow = function(prevrow)
    {
        var recx = fields[id].recs.attr('x');
        var recy = fields[id].recs.attr('y');
        var recw = fields[id].recs.attr('width');
        var rech = fields[id].recs.attr('height');
        var last_row=recy;
        
        for(var j = 0; j < fields[id].row.length; j++) 
        {
            
            fields[id].row[j].attr({opacity:30});
            if(fields[id].row[j].attr('y')>last_row)
                
            last_row= fields[id].row[j].attr('y');
        }
        
        if(last_row<recy+rech-20)
        {
        	if(prevrow!=0)
        		last_row=prevrow-20;
       var elem=paper.rect(recx,last_row+20,recw,1);
     
       elem.attr({opacity:1});
       fields[id].row.push(elem);
     
       if(prevrow==0)
       savedata(fields[id]);
        }
        else
            alert('not enough space');
        }

    this.deletecolumn = function()
    {
        var elem =fields[id].col.pop();
        elem.remove();
        var last_col=fields[id].recs.attr('x');
       
        for(var j = 0; j < fields[id].col.length; j++) 
        {
            
            if(fields[id].col[j].attr('x')>last_col)
            last_col= fields[id].col[j].attr('x');
        }
        
       fields[id].addcol.attr({x:last_col,y:fields[id].recs.attr('y')});
       fields[id].delcol.attr({x:last_col,y:fields[id].recs.attr('y')+20});
       fields[id].field.pop();
       fields[id].showdetails();
        }
    this.deleterow = function()
    {
        var elem =fields[id].row.pop();
        elem.remove();
        var last_row=fields[id].recs.attr('y');
        for(var j = 0; j < fields[id].row.length; j++) 
        {
            if(fields[id].row[j].attr('y')>last_row)
                
            last_row= fields[id].row[j].attr('y');
        }
       fields[id].addrow.attr({x:fields[id].recs.attr('x')+3,y:last_row});
       fields[id].delrow.attr({x:fields[id].recs.attr('x')+23,y:last_row});

        }


    
   
    
    this.recs.click(function () {
        showdata(fields[id],fields[id].field);
        savedata(fields[id]);
       });
 
   


 
    
}
//field object
function normalfield (arr,ratio,flag) {
	var zr=ratio;
    var resx = arr["coordinates"].split(",");
    this.recs=paper.rect((resx[0].trim())/zr,(resx[1].trim())/zr,((resx[2])/zr)-(resx[0]/zr),((resx[3]/zr))-(resx[1]/zr));
    this.recs.attr("fill", "BLUE");
    this.recs.attr("fill-opacity", "0.10");
    this.istable=arr["istable"];
    this.recid=arr["id"];
    this.field=[];
    var f=new fielddetails(arr);
    this.field.push(f);
  
    var id=count;
   
  
    this.recs.click(function () {
    	showdata(fields[id],fields[id].field);
    	savedata(fields[id]);
       });
  
    this.showdetails = function(){
    	showdata(fields[id],fields[id].field);
        }



}