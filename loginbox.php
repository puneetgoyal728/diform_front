<div id="login" class="modalDialog">
	<div class="r1" id="modalin">
		<a href="javascript:void(null);" class="close">x</a>
		<div id="loginerror" style="display: none; color: red">Wrong Email or
			Password</div>
		<form id="loginform" class="form" method="post" action="login.php"
			onsubmit="/*return login_func(event);*/">
			<ul>
				<li><input type="text" id="emailid" name="emailid"
					placeholder="Email" />
				</li>

				<li><input type="password" placeholder="Password" id="password"
					name="password"
					style="width: 290px; padding: 10px 5px; border: 1px #cccccc solid; font-size: 15px; border-radius: 3px;" />
				</li>
				<li><input type="checkbox" id="remember" name ="flag" value="1">Remember me</li>
				<input type="hidden" name="status" value="0">

				<li><input type="submit" id="submit" value="Login" /> <img
					src="images/loading.gif" id="loading" height="42" width="300"
					style="display: none;">
				</li>
			</ul>

			<a href="forgotpass.php">Forgot Password?</a>

			<!-- <p>
				Dont have an account ? <a href="signup.php">Sign Up</a>
			</p> -->
		</form>
		<!----end of loginform---->

	</div>
	<!----end of r1---->
</div>
<!----end of modal1---->
<script type="text/javascript">
		function login_func(e)
		{
			e = e || window.event;
		    e.preventDefault();
		    $('#submit').hide();
		     $('#loading').show();
			var emailid=$('#emailid').val();
			var password=$('#password').val();
		    var rflag=0;
			 if(document.getElementById("remember").checked)
				  rflag=1;
			var status="1";
			$.ajax({
			    type: "POST",
			    url: "login.php",
			    data: { 'emailid': emailid ,'password' : password,'status': status,'flag':rflag}
			}).done(function(datafromlogin) {
			    
			    if(datafromlogin==1)
			    {
			        window.location.href='jobs.php';
			    }
			    else if(datafromlogin==0)
			    {
                     $('#loginerror').show();
                     $('#loginerror').text('Wrong email or password');
                     $('#submit').show();
                     $('#loading').hide();
			    }
			    else
			    {
			    	$('#loginerror').show();
			    	$('#loginerror').text('verify your email address');
			    	$('#submit').show();
		             $('#loading').hide();
			    }
			    });
		    return false;
		}

		$(".close").click(function(){
			$('.modalDialog').fadeTo(400,0,function(){$('.modalDialog').hide();});
			});
		$(document).keyup(function(e) 
		{
		  
		  if(e.keyCode==27)
		  {
		  $('.modalDialog').fadeTo(400,0,function(){$('.modalDialog').hide();});
		  }
		});
		</script>
