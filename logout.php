<?php
session_start();
include_once 'config.php';
include_once 'scrypt.php';
$uid=$_SESSION["uid"];
$hash = Password::hash($uid);
$ip = getenv('HTTP_CLIENT_IP') ? : getenv('HTTP_X_FORWARDED_FOR')?: getenv('HTTP_X_FORWARDED')?: getenv('HTTP_FORWARDED_FOR')?: getenv('HTTP_FORWARDED')?: getenv('REMOTE_ADDR');
$url=$dashboard_basepath.'usersC/addUserActivityLog/'.urlencode($hash).'/'.$uid.'/'.$ip.'/logout/logout';
//$rt=file_get_contents($url);
session_unset(); 
session_destroy();
setcookie( "uid", "", time()- 60, "/","", 0);
setcookie( "hash", "", time()- 60, "/","", 0);
setcookie( "username", "", time()- 60, "/","", 0); 
header("location:index.php");
?>