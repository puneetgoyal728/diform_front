<?php 
session_start();
include_once('common_func.php');
checksession();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css"/>
<link href="css/googlefonts.css" rel='stylesheet' type='text/css'/>
<style>
#loadinglogx{
    display: none;
   
} 
</style>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">  
      $(document).keyup(function(e) 
{
  
  if(e.keyCode==27)
  {
  document.location="#"; 
  }
});
</script>
<title>11 TechSquare</title>
</head>
<body>
	<div id="header3">
		<?php include('headertwo.php'); ?>
	</div><!--end of header--->
	
	<div id="container">
		<div class="center">
			<div id="login">
				
					<form id="forgotpass" action="login.php" method="post" onsubmit="document.getElementById('signinlogx').style.display='none'; document.getElementById('loadinglogx').style.display='block';">
					     <h3 class="headnew">Login</h3>
                                <div id ="loginerror"  style="<?php echo (isset($_GET['ef']) && $_GET['ef'] == 1)?"":"display: none;" ?> color:red">Wrong Email or Password</div>
						<ul>
												
							<li>							    							    
							    <input type="text" placeholder="Enter your email" class="inp" name="emailid"/>
							</li>
												
							<li>							    
							    <input type="password" placeholder="Password"  name="password" class="inp"/>
							</li>							
							
							<li>
                                <input type="submit" id ="signinlogx" value="Submit" class="submit"/>
                                <img src="images/loading.gif" id="loadinglogx" height="42" width="500" >
                            </li>
                            
                            
                            <li style="margin-top: 20px;" class="acnt">
                                <p>Dont have an account ?</p>
                                
                                <a href="signup.php">Sign Up</a>
                            </li>
						</ul>
						
						
					
					</form>												
				
				
			</div><!---end of login--->
		</div><!---end of container center--->
	</div><!---end of container--->
	
	<div id="footer">
		<?php include('footer.php'); ?>
	</div><!---end of footer--->
</body>
</html>
