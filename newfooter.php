<div id="footer">
		<div class="center">
			<ul id="social">
				<li>
					<a href="#" target="_blank">
						<img src="images/fb-white.png" alt="facebook"/>
					</a>
				</li>
				
				<li>
					<a href="#" target="_blank">
						<img src="images/twitter-white.png" alt="twitter"/>
					</a>
				</li>
				
				<li>
					<a href="#" target="_blank">
						<img src="images/Google Plus-128.png" alt="googleplus"/>
					</a>
				</li>
			</ul>
			<div class="clear"></div>
			
			<p>Contact Us: hello@11techsquare.com</p>
			
			<div id="buttons">
				<a href="#">Terms of Service</a>
				<a href="#">Privacy Policy</a>
			</div>
			
			<p>Copyright &copy; 11 techsquare Private Limited. All Rights Reserved</p>
		</div><!---end of footer center--->
</div>