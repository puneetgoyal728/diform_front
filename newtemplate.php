<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
checksession();
$uid=$_SESSION['uid'];
foreach( $conn->dbh->query("SELECT max(form_id) as 'maxform' FROM tbl_image_master where user_id=$uid") as $row) {
    $g =$row['maxform']+1;
}
?>
<html lang="en">
  <meta charset="UTF-8" />
<head>
  <title>Multiple File upload with PHP</title>
</head>
<link type="text/css" rel="stylesheet" href="<?php echo auto_version('/css/default.css');?>" />

<style>
#fileItem
{
border:2px dotted #0B85A1;
width:400px;
color:#92AAB0;
text-align:left;vertical-align:middle;
padding:10px 10px 10 10px;
margin-bottom:10px;
font-size:200%;

}

form.pos_fixed {


    width: 400px;
    color: #92AAB0;
    position: fixed;
    top: 30%;
    left: 40%;
    background:#D0D0D0 ;
    
   
}
#popup{
z-index:9;
opacity:0.92;
position: absolute;
top: 0px;
left: 0px;
height: 100%;
width: 100%;
background:#F0F0F0 ;
display: none;
}

#dropbox {
float:left;
margin-left:20px;
width:110px;
height:170px;
background: #f5f5f5;
border-style: dotted;
border-width: 2px;
cursor: pointer;
border-color: #aaa;
}

.dragCall {
    font-size: 15px;
    margin: 35px 0;
    text-align: center;
}

.clickCall {
    font-size: 8px;
    margin-top: 20px;
    text-align: center;
}
</style>
<body>
<?php 
include 'jobsheader.php';
?>
<form id="data" style="background-color: #eeeeee;">
    <input id ="clickk" name="profileImg[]" type="file" accept="image/x-png, image/gif, image/jpeg,image/tiff, application/pdf"  multiple="multiple" style="visibility:hidden; display:none;"/><br />
</form>
<div style="background-color: #eeeeee;" >
    <div class="center" style="text-align: right;">
        <button class="button" onclick=finish();>finish</button>
    </div>
    <div class="right">
    </div>
</div>
<div id="popup" style="background-color: #eeeeee;">
    <form class="pos_fixed" action="#" id="contact">
        <h3>Enter Template Name</h3>
        <textarea  id="c_az" ></textarea>
        <img src="images/close.png" height="40" width="40"  id="cancel" style="position:relative;left: 44px;top:-150px" />
        <input type="button" id="ok" value="Ok"/>
    </form>
</div>
<div id="container" style="background-color: #eeeeee;">
<div id="uploadedfiles"></div>
   <div id="dropbox" >
        <div style = "opacity:1;">
            <p class="dragCall">Drag <br /> page <span id="countervalue">1</span><br /> here</p>
            <p class="clickCall">Or click to browse</p>
        </div>
   </div>
</div>
<script src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>
<script src="<?php echo auto_version('/js/raphael.min.js');?>"></script>
<script src="<?php echo auto_version('/js/newtemplate.js');?>"></script>
<script>
//variables
var formid=0;
var counter=0;
var netc=0;
var dropbox = document.getElementById("dropbox");
var pdfimage ="images/pdf.png";
var testvar;

$("#popup #cancel").click(function() {
    $(this).parent().parent().hide();
});


$("#popup #ok").click(function() {
    var n= $('#c_az').val();
    $.ajax({
        type: "POST",
        url: "templatename.php",
        data: { 'name': n ,'logentry':'1','form':formid}
    }).done(function() {
        window.location.href='createtemplate.php?g='+formid;
    });
});

function finish()
{
    if(netc!=0){
    	document.getElementById("c_az").value ="template "+<?php echo $g;?>;
        $("#popup").css("display", "block");
    }
}

//Adding events to Dropbox

dropbox.addEventListener("dragenter", dragenter, false);
dropbox.addEventListener("dragover", dragover, false);
dropbox.addEventListener("drop", drop, false);
dropbox.addEventListener("click", oninput, true);

//files by clicking basket
$("#data").change(function(event){
     event.stopPropagation();
      event.preventDefault();
      var filesArray = document.getElementById('clickk').files;
        sendFile(filesArray,0);

});


</script>
</body>
<?php //include_once('footer.php')?>
</html>