<?php

include_once 'config.php';
include_once 'db_conn.php';
include_once 'referenceArrays.php';
include_once 'common_func.php';

function JobIdMapper($fieldType, $fieldSubType) {
    global $maptojobmap;
    if(isset($maptojobmap[$fieldType][$fieldSubType])){
        return $maptojobmap[$fieldType][$fieldSubType];
    }
    else{
        return 1;
    }
}

function generateTSPDEntriesForTSM($tsm_id,$no_gen){
    global $conn,$useCrowd,$crowdJobToSquadrunJob;

    $queryForFieldtype = "SELECT tsm.tsm_id snippet_id, ttm.field_name fldname, ttm.field_type fldtype, ttm.sub_field_type sfldtype
    FROM tbl_snippets_master tsm join tbl_job_details tjd join tbl_template_master ttm join tbl_image_master tim
    ON tsm.job_id=tjd.job_id and tsm.page_no = tjd.page_no and tjd.form_id = ttm.form_id
    and tjd.form_page_no = ttm.page_no and tjd.form_id = tim.form_id and tjd.form_page_no = tim.page_no
    where tsm.field_id = ttm.field_id and tsm.tsm_id=$tsm_id";
    // 	echo $queryForFieldtype."<br />";
    $fieldtypeResult = $conn->dbh->query($queryForFieldtype);
    $results = $fieldtypeResult->fetchAll(PDO::FETCH_ASSOC);
    if(count($results)==1){
        $tcj_Id = JobIdMapper($results[0]['fldtype'], $results[0]['sfldtype']);
        // 		echo $tcj_Id;exit;
        if(strcmp($useCrowd,"11TECH")==0){
            for ($i=0;$i<$no_gen;$i++){
                $tspdInsertQry = "Insert into tbl_snippet_process_data (tsm_id, tspd_status, tcj_id, RecAddDate, RecAddTime)
                Values ($tsm_id,0,$tcj_Id,CURDATE(),CURTIME())";
                $insertResult = $conn->dbh->query($tspdInsertQry);
            }
        }else if(strcmp($useCrowd,"SQUAD")==0){
            do{
                $trxnId = createRandom(12);
                $tspdInsertQry = "Insert into tbl_squadrun_tasks 
                (tsm_id,tst_trxn_id, tst_status, serve_copies_no, tcj_id, as_mission_id, RecAddDate, RecAddTime)
                Values ($tsm_id, '".$trxnId."',0, $no_gen, $tcj_Id, '".$crowdJobToSquadrunJob[$tcj_id]."', CURDATE(), CURTIME())";
                $tspdInsertPrep = $conn->dbh->prepare($tspdInsertQry);
                $insertResult = $tspdInsertPrep->execute();
            }while(!$insertResult);
            
            return isset($crowdJobToSquadrunJob[$tcj_id])?$crowdJobToSquadrunJob[$tcj_id]:$crowdJobToSquadrunJob[1];
        }
        return $tcj_Id;
    }else{
        return false;
    }
}

function grantPayment($tcm_id, $tspd_id, $tcj_id){
    global $conn;
    $updateTSPDForCorrRespQry = "update tbl_snippet_process_data set iscorrect = 1 where tspd_id = $tspd_id";
    //     echo $updateTSPDForCorrRespQry;
    $updateTSPDForCorrRespRes = $conn->dbh->query($updateTSPDForCorrRespQry);

    $getPriceJIDQry = "select price_per_job from tbl_crowd_jobs where tcj_id = $tcj_id";
    $priceResponse = $conn->dbh->query($getPriceJIDQry);
    $priceResult = $priceResponse->fetchAll(PDO::FETCH_NUM);
    if(count($priceResult)==1){
        $priceperjob = $priceResult[0][0];
        $tcpInsertQry = "insert into tbl_crowd_payment (tspd_id,tcj_id,proposed_price) values ($tspd_id,$tcj_id,$priceperjob)";
        $tcpinsertResult = $conn->dbh->query($tcpInsertQry);
        $tcp_id = $conn->dbh->lastInsertId();

        $crowdWalletLog = "insert into tbl_crowd_wallet_log (tcp_id, tcm_id, amount, tcwl_status, RecAddDate, RecAddTime)
        VALUES ($tcp_id,$tcm_id,$priceperjob,0,CURDATE(),CURTIME())";
        $crowdwalletResult = $conn->dbh->query($crowdWalletLog);
    }
}


function denyPayment($tcm_id, $tspd_id, $tcj_id){
    global $conn;
    $updateTSPDForCorrRespQry = "update tbl_snippet_process_data set iscorrect = 0 where tspd_id = $tspd_id";
    $updateTSPDForCorrRespRes = $conn->dbh->query($updateTSPDForCorrRespQry);
}


function getMinVerifyForJob($job_id){
    global $conn;
    $minimumPassQuery = "select verifyPass from tbl_job_master where job_id =".$job_id;
    $minimumPassRes = $conn->dbh->query($minimumPassQuery);
    $minimumPassResult = $minimumPassRes->fetchAll(PDO::FETCH_ASSOC);
    $minimumPassVal = 0;
    if(count($minimumPassResult)==1){
        $minimumPassVal = $minimumPassResult[0]['verifyPass'];
    }
    return $minimumPassVal;
}


function setCorrectValue($tsm_id,$val){
    global $conn;
    $updateCorrectValQry = "update tbl_snippets_master set processed=1, verified_data='".$val."' where tsm_id=$tsm_id";
    //     echo $updateCorrectValQry;exit;
    $updateCorrectRes = $conn->dbh->query($updateCorrectValQry);
}


function isAllResponseFilled($tsm_id){
    global $conn;
    $responseFilledQry = "Select count(*) from tbl_snippet_process_data where tsm_id = $tsm_id and (tspd_status = 0 or tspd_status = 1)";
    $responseFilledRes = $conn->dbh->query($responseFilledQry);
    $responseFilledResult = $responseFilledRes->fetch(PDO::FETCH_NUM);
    if($responseFilledResult[0]==0){
        return true;
    }else{
        return false;
    }
}

function getAllResponsesForTSM($tsm_id) {
    global $conn;
    $getResponsesQry = "Select tspd_id, tcm_id, data_entered from tbl_snippet_process_data where tsm_id = $tsm_id and tspd_status = 2 and data_entered is not null";
    //    echo $getResponsesQry;exit;
    $getResponsesRes = $conn->dbh->query($getResponsesQry);
    $getResponsesResult = $getResponsesRes->fetchAll(PDO::FETCH_ASSOC);
    $returnVal = array();
    if(count($getResponsesResult)>0){
        $returnVal['status'] = 1;
        $returnVal['data'] = $getResponsesResult;
    }else{
        $returnVal['status'] = 0;
    }
    return $returnVal;
}


function getSquadrunRespForTSM($tsm_id,$tst_trxn_id) {
    global $conn;
    $getResponsesQry = "select tsr_id tspd_id, worker_id tcm_id, value_returned data_entered
    from tbl_squadrun_response
    where tsm_id = '".$tsm_id."' and tst_trxn_id = '".$tst_trxn_id."' and value_returned is not null";
    //    echo $getResponsesQry;exit;
    $getResponsesRes = $conn->dbh->query($getResponsesQry);
    $getResponsesResult = $getResponsesRes->fetchAll(PDO::FETCH_ASSOC);
    $returnVal = array();
    if(count($getResponsesResult)>0){
        $returnVal['status'] = 1;
        $returnVal['data'] = $getResponsesResult;
    }else{
        $returnVal['status'] = 0;
    }
    return $returnVal;
}

function evaluateResponses ($responseData,$minimumPassVal) {
    // array('correctVal'=> value, 'correct' =>array(array(tspd_id, tcm_id),array(tspd_id, tcm_id)), 'wrong'=> array(array(tspd_id, tcm_id),array(tspd_id, tcm_id)))
    //         print_r($responseData);
    //         echo $minimumPassVal;exit;
    $valueCountArray = array();
    $returnValue = array();
    $correctKey = false;
    foreach ($responseData as $response) {
        $valueEntered = strtolower($response['data_entered']);
        $pattern = '/\s*[;,\/\.]\s*/';
        $replace = ';';
        $valueEntered = preg_replace($pattern, $replace, $valueEntered);
        
        if(isset($valueCountArray[$valueEntered]['count'])){
            $valueCountArray[$valueEntered]['count']++;
            array_push($valueCountArray[$valueEntered]['tspdid'],$response['tspd_id']);
            array_push($valueCountArray[$valueEntered]['tcmid'], $response['tcm_id']);
            if($valueCountArray[$valueEntered]['count']>=$minimumPassVal){
                $returnValue['correctVal'] = $response['data_entered'];
                $correctKey = $valueEntered;
            }
        }else{
            $valueCountArray[$valueEntered] = array('count'=>1,'tspdid'=>array($response['tspd_id']),'tcmid'=>array($response['tcm_id']));
        }
    }
    $returnValue['correct'] = array();
    $returnValue['wrong'] = array();
    if($correctKey){
        $pushAs = '';
        foreach ($valueCountArray as $lowerVal=>$arrValue){
            if(strcmp($lowerVal, $correctKey)==0){
                $pushAs = 'correct';
            }else{
                $pushAs = 'wrong';
            }
            foreach ($arrValue['tspdid'] as $idNum=>$tVal){
                array_push($returnValue[$pushAs],array($tVal,$arrValue['tcmid'][$idNum]));
            }
        }
    }else{
        $returnValue['correctVal'] = false;
    }

    return $returnValue;
}


function grantSquadrunPayment($tsr_id, $tcj_id){
    global $conn;
    $updateTSPDForCorrRespQry = "update tbl_squadrun_response set is_correct = 1 where tsr_id = $tsr_id";
    //     echo $updateTSPDForCorrRespQry;
    $updateTSPDForCorrRespRes = $conn->dbh->query($updateTSPDForCorrRespQry);

    $getPriceJIDQry = "select price_per_job from tbl_crowd_jobs where tcj_id = $tcj_id";
    $priceResponse = $conn->dbh->query($getPriceJIDQry);
    $priceResult = $priceResponse->fetch(PDO::FETCH_NUM);
    if($priceResult){
        $priceperjob = $priceResult[0];
        $tcpInsertQry = "insert into tbl_squadrun_payment (tsr_id,tcj_id,proposed_price, payment_status, RecAddDate, RecAddTime)
        values ($tsr_id,$tcj_id,$priceperjob, 0,CURDATE(),CURTIME())";
        $tcpinsertResult = $conn->dbh->query($tcpInsertQry);
    }
}

function denySquadrunPayment($tsr_id) {
    global $conn;
    $updateTSPDForCorrRespQry = "update tbl_squadrun_response set is_correct = 0 where tsr_id = $tsr_id";
    //     echo $updateTSPDForCorrRespQry;
    $updateTSPDForCorrRespRes = $conn->dbh->query($updateTSPDForCorrRespQry);
}

function isAllSquadrunResponseReceived($tsm_id,$trxn_id){
    global $conn;
    $requestQry = "Select serve_copies_no from tbl_squadrun_tasks where tsm_id = $tsm_id and tst_trxn_id = '".$trxn_id."'";
    $requestRes = $conn->dbh->query($requestQry);
    $requestResult = $requestRes->fetch(PDO::FETCH_NUM);

    $responseQuery = "Select count(*) from tbl_squadrun_response where tsm_id = $tsm_id and tst_trxn_id = '".$trxn_id."'";
    $responseRes = $conn->dbh->query($responseQuery);
    $responseResult = $responseRes->fetch(PDO::FETCH_NUM);

    if($responseResult[0]==$requestResult[0]){
        return true;
    }else{
        return false;
    }
}

function evaluateSquadrunResp($txnId, $val, $workerId){
    global $conn;
    $infoFetchQry = "select tst.tst_status status, tst.img_serve_name serve_img, tst.tcj_id tcj_id, tsm.tsm_id tsm_id, tjm.verifyPass pass
    from tbl_squadrun_tasks tst join tbl_snippets_master tsm join tbl_job_master tjm
    on tst.tsm_id = tsm.tsm_id and tsm.job_id = tjm.job_id where tst.tst_trxn_id = '".$txnId."' and tst.tst_status != 4";
    $infoFetchRes = $conn->dbh->query($infoFetchQry);
    $infoFetch = $infoFetchRes->fetch(PDO::FETCH_ASSOC);
    $returnVal = array();
    $returnVal['trxn_id'] = $txnId;
    if(!$infoFetch){
        $returnVal['status'] = -2; //this transaction id does not exist
        $returnVal['msg'] = "transaction id does not exist or already processed";
        return $returnVal;
    }

    $respEnter = $conn->dbh->prepare("insert into tbl_squadrun_response (tsm_id, tst_trxn_id, worker_id, value_returned, RecAddDate, RecAddTime)
            values ('".$infoFetch['tsm_id']."','".$txnId."','".$workerId."','".$val."',CURDATE(), CURTIME())");
    $respEnter->execute();
    $tsr_id = $conn->dbh->lastInsertId();

    if(strcmp($infoFetch['pass'],"1")==0){
        $returnVal['status'] = 4;
        $returnVal['msg'] = "successful";
        $returnVal['correct_responses'] = array();
        array_push($returnVal['correct_responses'],array('worker'=>$workerId,'trxn_id'=>$txnId));
        $tstUpdateAcceptedQry = $conn->dbh->prepare("update tbl_squadrun_tasks set tst_status = 4 where tst_trxn_id = '".$txnId."'");
        $tstUpdateRes = $tstUpdateAcceptedQry->execute();
        setCorrectValue($infoFetch['tsm_id'],$val);
        grantSquadrunPayment($tsr_id,$infoFetch['tcj_id']);
        @unlink($infoFetch['serve_img']);
    }else{
        if(isAllSquadrunResponseReceived($infoFetch['tsm_id'],$txnId)){
            $dataEntered = getSquadrunRespForTSM($infoFetch['tsm_id'],$txnId);
            if($dataEntered['status']==1){
                $evaluatedData = evaluateResponses($dataEntered['data'],$infoFetch['pass']);
                if($evaluatedData['correctVal']){
                    setCorrectValue($infoFetch['tsm_id'],$evaluatedData['correctVal']);
                    $returnVal['correct_responses'] = array();
                    $returnVal['incorrect_responses'] = array();
                    foreach ($evaluatedData['correct'] as $correctResponses){
                        array_push($returnVal['correct_responses'],array('worker'=>$correctResponses[1],'trxn_id'=>$txnId));
                        grantSquadrunPayment($correctResponses[0], $infoFetch['tcj_id']);
                    }
                    foreach ($evaluatedData['wrong'] as $wrongResponses){
                        array_push($returnVal['incorrect_responses'],array('worker'=>$wrongResponses[1],'trxn_id'=>$txnId));
                        denySquadrunPayment($wrongResponses[0], $infoFetch['tcj_id']);
                    }
                    @unlink($infoFetch['serve_img']);
                    $returnVal['status'] = 4;
                    $returnVal['msg'] = "successful";
                    @unlink($infoFetch['serve_img']);
                    $updateQry = "update tbl_squadrun_tasks set tst_status = 4 where tsm_id = '".$infoFetch['tsm_id']."' and tst_trxn_id = '".$txnId."'";
                    $conn->dbh->query($updateQry);
                }else{
                    //                 generateTSPDEntriesForTSM($getTsmResult[0]['tsm_id'], 1);
                    $returnVal['status'] = 3;
                    $returnVal['msg'] = "needs to be given to one more user";
                    $updateQry = "update tbl_squadrun_tasks set tst_status = 3, serve_copies_no = serve_copies_no+1 where tsm_id = '".$infoFetch['tsm_id']."' and tst_trxn_id = '".$txnId."'";
                    $conn->dbh->query($updateQry);
                }
            }else{
                $returnVal['status'] = -3;//unknown error
                $returnVal['msg'] = "error";
            }
        }else{
            $returnVal['status'] = 2;
            $returnVal['msg'] = "not enough data points to compare";
            $updateQry = "update tbl_squadrun_tasks set tst_status = 2 where tsm_id = '".$infoFetch['tsm_id']."' and tst_trxn_id = '".$txnId."'";
            $conn->dbh->query($updateQry);
        }
    }
    return $returnVal;
}