<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'/>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">  
      $(document).keyup(function(e) 
{
  
  if(e.keyCode==27)
  {
  document.location="#"; 
  }
});
</script>
<title>11 TechSquare</title>
</head>
<body>
	<div id="header3">
		<?php include('headertwo.php'); ?>
	</div><!--end of header--->
	
	<div id="pricing">
		<div class="center">
			<div id="vision">
				<h2 class="head">Choose your plan !</h2>			
				<p style="font-style:italic; font-size:18px;">
					Want to know more, got a feedback or just want to strike a converstation.
				</p>
			</div><!---end of vision--->
			
			<div id="pricein">
				<div class="price">
					<ul>
						<li>
							$ 5 / mo
						</li>
						
						<li>
							2 GB of space
						</li>
						
						<li>
							Safe, reliable backup
						</li>
						
						<li>
							Access from anywhere
						</li>
						
						<li>
							Simple file sharing
						</li>
					</ul>
					
					<a href="#">Sign Up</a>
				</div><!---end of class price--->
				
				<div class="price">
					<ul>
						<li>
							$ 5 / mo
						</li>
						
						<li>
							2 GB of space
						</li>
						
						<li>
							Safe, reliable backup
						</li>
						
						<li>
							Access from anywhere
						</li>
						
						<li>
							Simple file sharing
						</li>
					</ul>
					
					<a href="#">Sign Up</a>
				</div><!---end of class price--->
				
				<div class="price">
					<ul>
						<li>
							$ 5 / mo
						</li>
						
						<li>
							2 GB of space
						</li>
						
						<li>
							Safe, reliable backup
						</li>
						
						<li>
							Access from anywhere
						</li>
						
						<li>
							Simple file sharing
						</li>
					</ul>
					
					<a href="#">Sign Up</a>
				</div><!---end of class price--->
				<div class="clear"></div>
			</div><!---end of pricein--->
			
		</div><!---end of pricing center--->
	</div><!---end of pricing--->
	
	<div id="footer">
		<?php include("footer.php"); ?>
	</div><!---end of footer--->
</body>

</html>
