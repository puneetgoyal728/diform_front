<?php
$fieldtype = array("text"=>"Text","num"=>"Numeric","multiple"=>"Multiple Correct","pic"=>"Picture");
$subtype=array("small"=>"Small","large"=>"Large","selone"=>"Select One","selmany"=>"Select Many");
$fieldSubtypeMap = array("text"=>array("small","large"),"multiple"=>array("selone","selmany"));

$maptojobmap = array("text"=>array("small"=>1, "large"=>1),"multiple"=>array("selone"=>2,"selmany"=>3));

?>