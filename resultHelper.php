<?php

function rawDataToTabularFormat($rawData){
	if(count($rawData) == 0){
		return null;
	}
	$traversedOneForm = false;
	$formTraverseLastPage = 1;
	$prevPgNum = 1;
	$heading = array(array());
	$heading[0][0] = "FileName";
	$dataVals = array(array());
	$dataVals[0][0] = $rawData[0]['uin']; 
	foreach ($rawData as $jobData) {
		if($prevPgNum != $jobData['pgnum']){
			$dataVals[$jobData['pgnum']-1][0] = $jobData['uin'];
			if ($formTraverseLastPage>=$jobData['fpgnum']){
				$traversedOneForm = true;
			}
		}
		if(!$traversedOneForm){
			$heading[$jobData['pgnum']-1][$jobData['fldid']] = $jobData['fldname'];
		}
		$dataVals[$jobData['pgnum']-1][$jobData['fldid']] = $jobData['val'];
// 		$dataVals[$jobData['pgnum']-1][$jobData['']]
	}
	return array('heading'=>$heading, 'data'=>$dataVals);
}


function tabularDataToCSVFormat($tabularData) {
    $csvData = array();
    $heading = $tabularData['heading'];
    $values = $tabularData['data'];
    $formPageLen = count($heading);
    $currFormPg = 0;
    $rowCount = 0;
    $csvData[$rowCount] = array();
    while ($currFormPg < $formPageLen) {
        foreach ($heading[$currFormPg] as $fieldLbl){
            array_push($csvData[$rowCount], $fieldLbl);
        }
        $currFormPg++;
    }
    $currInpPg = 0;
    
    $arrKeys = array_keys($values);
    
    while ($currInpPg <= $arrKeys[count($arrKeys)-1]){
        if(($currInpPg%$currFormPg) == 0){
            $rowCount++;
            $csvData[$rowCount] = array();
        }
        foreach ($values[$currInpPg] as $fieldId=>$fieldVal){
            if(($currInpPg%$currFormPg) == 0 || $fieldId != 0){
                array_push($csvData[$rowCount],html_entity_decode($fieldVal,ENT_QUOTES));
            }
        }
        $currInpPg++;
    }
    return $csvData;
}
?>