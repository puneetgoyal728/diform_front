<?php

session_start();

include_once("config.php");
include_once ('db_conn.php');
include_once('common_func.php');
checksession();
$t=0;
if(isset($_GET['name']))
{
    $t= $_GET['name'];
}
$currentHead = "TEMPLATE";
?>

<html>

<link type="text/css" rel="stylesheet" href="css/default.css" />
<link href="css/googlefonts.css" rel='stylesheet' type='text/css' />

<style>
div.fg {
	background-color: lightgrey;
	width: 1000px;
	padding: 25px;
	border: 5px solid navy;
	margin: 25px;
}

button.clr {
	color: red;
}
</style>



<div style="background-color:rgba(0,0,0,0.9); width:100%; height:100%; position:relative;">


	<div id="container" style="position:fixed; z-index:999999; top:0; left:0; right:0; bottom:0; padding:20px 0;">
		<div class="center">

			<div id="templates" style="background-color:#eeeeee; padding:20px; position:relative; top:0px; width:960px; border-radius:3px; min-height:630px; overflow:auto;">
<div id="arrows">
	<div class="center">
		<ul>
			<li>
                <a href="#">
                <span class="pos"><img src="images/step1small.png" alt="step1"/> <b>Jobs</b></span>
                <span class="arrow-right"></span>
                </a>
            </li>
            
            <li>
                <a href="#">
                <span class="pos"> <img src="images/step2small.png" alt="step1"/><b>Templates</b></span>
                <span class="arrow-right"></span>
                </a>
            </li>
            
            <li>
                <a href="#" style="opacity:0.2;">
                <span class="pos"><img src="images/step3small.png" alt="step1"/><b>Inbox</b></span>
                <span class="arrow-right"></span>
                </a>
            </li>
		</ul>
		<div class="clear"></div>
		
		<a href="#" class="close"> x</a>
	</div><!---end of arrows center--->
</div><!---end of arrows--->

				<div id="templatecon" style="margin-top:100px;">
					<div id="temp_one">
						<h2 class="headnewbig" style="font-size:35px; color:#FF942F;">Sit back and relax, We'r on it</h2>				
					</div>									
					
					<div id="temp_two">
						<a class='button' href="inbox.php" style="margin:50px 0 20px 10px;">View Job Status</a>
						
						<a href="jobs.php" class="anco" style="text-decoration:underline;">Start a New Job</a>
					</div>
					<div class="clear"></div>
	
				</div><!-- end of templatecon-->
			</div><!-- end of templates -->
		</div>
		<!-- end of container center -->
	</div>
	<!-- end of container -->



</div>
</html>
