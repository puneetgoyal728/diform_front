<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('jobclass.php');
include_once('common_func.php');
checksession();
$uid =$_SESSION["uid"];
$currentHead = "JOBS";
include("jobsheader.php");
$ark=array();
$flag=1;
if(isset($_GET["jid"]))
{
    $flag=0;
    $jobid=$_GET["jid"];
    $listFormsQuery = "select * from tbl_job_upload_details where job_id=$jobid";
    foreach($conn->dbh->query($listFormsQuery) as $row) {
    $id=$row["upload_order"];
    $img=$jobs_path.$jobid."/".$row["upload_name"];
    $data=array("id"=>$row["upload_order"],"path"=>$jobs_path.$jobid."/".$row["upload_name"],"name"=>$row["upload_name"]); 
    array_push($ark, $data)  ;
    }
    $deg = json_encode( $ark );
}
?>
<html>

<link type="text/css" rel="stylesheet" href="css/default.css" />
<link href="css/googlefonts.css" rel='stylesheet' type='text/css' />

<style>
div.fg {
	background-color: lightgrey;
	width: 1000px;
	padding: 25px;
	border: 5px solid navy;
	margin: 25px;
}

button.clr {
	color: red;
}
#cancelpopup {
    z-index: 9;
    opacity: 0.97;
    position: absolute;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    background: #F0F0F0;
    display: none;
}

#cancelmessage {
    width: 400px;
    height:150px;
    color: #92AAB0;
    position: fixed;
    top: 30%;
    left: 30%;
   background:#F0F0F0 ;
    border-radius: 4px;
        box-shadow: 0 0 5px rgba(0,0,0,0.9);
}
.formbuttons {
    padding:10px;
    background-color:#FB8C2D;
    color:white;
    font-size:14px;
    font-weight:500;
    border:none;
    cursor:pointer;
    border-radius:3px;
    

}
</style>



<div style="background-color:rgba(0,0,0,0.9); width:100%; height:100%; position:relative;">


	<?php include('new_header_two.php');?>

	<div id="container" style="position:fixed; z-index:999999; top:0; left:0; right:0; bottom:0; padding:20px 0;">
		<div class="center">

			<div id="templates" style="background-color:#eeeeee; padding:20px; position:relative; top:0px; width:960px; border-radius:3px; min-height:630px; overflow:auto;">
<div id="arrows">
	<div class="center">
		<ul>
			<li>
				<a href="jobs.php">
				<span class="pos"><img src="images/step1small.png" alt="step1"/> <b>Jobs</b></span>
				<span class="arrow-right"></span>
				</a>
			</li>
			
			<li>
				<a href="sessiontemplate.php?name=<?php echo $_GET['jid'];?>" style="opacity:0.2;">
				<span class="pos"> <img src="images/step2small.png" alt="step1"/><b>Templates</b></span>
				<span class="arrow-right"></span>
				</a>
			</li>
			
			<li>
				<a href="inbox2.php" style="opacity:0.2;">
				<span class="pos"><img src="images/step3small.png" alt="step1"/><b>Inbox</b></span>
				<span class="arrow-right"></span>
				</a>
			</li>
		</ul>
		<div class="clear"></div>
		
		<a href="#" class="close" id="closeicon"> x</a>
	</div><!---end of arrows center--->
</div><!---end of arrows--->
<link type="text/css" rel="stylesheet" href="<?php echo auto_version('/css/default.css');?>"/>
<script type="text/javascript" src="<?php echo auto_version('/js/raphael.min.js');?>"></script>
<script type="text/javascript" src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>


<body>
<form id="data" style="background-color: #eeeeee;"><input id="clickk"
 name="profileImg[]" type="file" multiple="multiple"
 accept="image/x-png, image/gif, image/jpeg, application/pdf"
 style="visibility: hidden; display: none;" /><br />
</form>
<div id="cancelpopup">
    <div id="cancelmessage" style="background-color: #eeeeee;">
    <br/>
    <h3>You are about to cancel the process</h3>
    <img src="images/close.png" height="40" width="40"  id="cancel" style="position:relative;left: 260px;top:-64px" />
    <input type="button" id="ok" value="ok" class="formbuttons" style="position:relative;left: -20px;top:24px;width:100px"/><br />
    </div>
</div>
<div id="container" style="background-color: #eeeeee; padding: 0px 0;">
    <div id="job">
        <div id="heading">
           
            <div class="right">
                <button class="button" onclick="deleteselected()">delete selected</button>
                    <button class="button" onclick="deleteall()">delete all</button>
            </div>
            <div class="clear"></div>
        </div>
        <div id="jobin">
            <div class="left">
                <div id="der" style="border: 1px black dotted; margin-top: 10px; overflow: auto; height: 430px; width:800px">
                <img src="images/drag.jpg" id="files" height="150" width="600"  style="position:relative;left: 20px;">
                    <div id="uploadedfiles"></div>
                </div>
            </div>
            <div class="right">

<button class="next" onclick="sendtemp();">
    <span class="pos" style="position:relative;left: -60px;">Next</span>
    <span class="arrow-next" style="position:relative;left: -60px;"></span>
</button>
</div>
            <div class="clear"></div>
        </div>
    </div>
  
</div>
<script type="text/javascript">

var counter=0;
var totalcount=0;
var selected=[];
var jobname;
var pdfimage ="images/pdf.png";
var jay= <?php echo json_encode($ark ); ?>;
for(i in jay)
{
    prev_data(jay[i]);
    ++totalcount;
}
function getExtension(filename){
    var fileNameArr = filename.split(".");
    var extensionIdx = fileNameArr.length -1;
    return fileNameArr[extensionIdx].toLowerCase();
}

function isAllowedToUpload(fileName){ 
    var allowedFiles = ["jpeg","jpg","png","bmp","pdf"];
    if($.inArray(getExtension(fileName), allowedFiles)>-1){
        return true;
    }else{
        return false;
    }
}

$("#cancel").click(function() {
    $(this).parent().parent().hide();
});

$("#ok").click(function() {
    
    window.location.href='jobs.php';
   
});
$("#closeicon").click(function() {

$('#cancelpopup').show();   
});

function sendtemp()
{

        window.location.href='sessiontemplate.php?name='+$("#setjobId").val();
}
var dropbox = document.getElementById("files");
dropbox.addEventListener("dragenter", dragenter, false);
dropbox.addEventListener("dragover", dragover, false);
dropbox.addEventListener("drop", drop, false);
dropbox.addEventListener("click", oninput, true);

function oninput(){
    document.getElementById("clickk").click();
}
window.addEventListener("dragover",function(e){
 e = e || event;
 e.preventDefault();
},false);
window.addEventListener("drop",function(e){
  e = e || event;
  e.preventDefault();
},false);
function dragenter(e) {
  e.stopPropagation();
  e.preventDefault();
}
function dragover(e) {
  e.stopPropagation();
  e.preventDefault();
}
function drop(e) {
    e.stopPropagation();
    e.preventDefault();
    var filesArray = e.dataTransfer.files;
    sendFile(filesArray,0);
}

$("#data").change(function(e){

    e.stopPropagation();
    e.preventDefault();
    var filesArray = document.getElementById('clickk').files;
    sendFile(filesArray,0);
});

function sendFile(fileArray,idx) {
	 if(totalcount<5){
    var uri = "upjobs.php";
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    if(isAllowedToUpload(fileArray[idx].name)){
        showUploadingImage(fileArray[idx]);
        xhr.open("POST", uri, true);
        xhr.onreadystatechange = (function(fileArray,idx) {
            return function(){
                if (xhr.readyState == 4 && xhr.status == 200) {
                   
                    if(xhr.responseText=='0')
                    {
                       window.location.href='logx.php';
                    } 
                    var im =xhr.responseText;
                    im = im.trim();
                    im = JSON.parse(im);
                    jobname=im.count;
                    $("#setjobId").val(im.jobid);
                    if((fileArray.length-1) > idx){
                      sendFile(fileArray,++idx);
                    }
                }
            };
        })(fileArray,idx);
        fd.append('myFile', fileArray[idx]);
        var jid = <?php echo $jobid?>;
        fd.append('jid', jid);
        fd.append('flag',<?php echo $flag;?>);
        xhr.send(fd);
    }
	 }
}

function prev_data(file){
    ++counter;
    $("#uploadedfiles").append('<div id="border'+counter+'"  style="float:left;width: 130px; margin-left: 5px;  height: 180px;"><br/><img id="img'+counter+'" src="'+file.path+'" height="130" width="90"  onclick="select('+counter+');"><p id="name'+counter+'" style=" font-size: 15px;">'+file.name+'</p><img src="images/close.png" height="20" width="20"  style="position:relative;left: 40px;top: -160px;" onclick="deleteJob('+counter+');"></div>');
    if(getExtension(file.name)=="pdf")
        $("#img"+counter).attr({'src':pdfimage});
 }

       
function showUploadingImage(file){
 ++counter;
 $("#uploadedfiles").append('<div id="border'+counter+'"  style="float:left;width: 130px; margin-left: 5px;  height: 180px;"><br/><img id="img'+counter+'" src="images/loader.gif" height="130" width="90"  onclick="select('+counter+');"><p  id="name'+counter+'" style=" font-size: 15px;">'+file.name+'</p><img src="images/close.png" height="20" width="20"  style="position:relative;left: 40px;top: -160px;" onclick="deleteJob('+counter+');"></div>');

     
   

     if(getExtension(file.name)!="pdf"){
          var reader = new FileReader();
             reader.onload = (function (currloop){
                 var loopIdx = currloop;
                 return  function (e) {
                     src= e.target.result;
                     
                     $("#img"+counter).attr({'src':src});
                    
                 };
             })(counter);
             reader.readAsDataURL(file);
     }else{
         $("#img"+counter).attr({'src':pdfimage});
     }
 
}

function select(id)
{
 if($('#img'+id).css('opacity')==1)
 {
     $('#img'+id).css('opacity','0.3');
  selected.push(id);
 }
 else
 {
     $('#img'+id).css('opacity','1');
     var index = selected.indexOf(id);
     selected.splice(index, 1);
 }

     }

function deleteselected() { 
 for (var i= 0; i < selected.length; i++) {
      if('#border'+selected[i]!=undefined)
          deleteJob(selected[i])
 }
     
}

function deleteJob(id)
{
    var name=$('#name'+id).text();
    var jobid=$("#setjobId").val();
    $.ajax({
        url: "deletejob.php",
        type: "POST",
        data: { 'name': name,'jobid':jobid }
    });
$('#border'+id).remove();
--totalcount;
     }

</script>
<div class="jobData"><input type="hidden" id="setjobId"
 value="<?php if (isset($_GET['jid'])) {
    echo $_GET['jid'];
}else{
    echo "-1";
}?>" /></div>
</div>
</html>
