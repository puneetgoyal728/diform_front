<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once 'common_func.php';
if(!isset($_SESSION["uid"])){
    header("location:logx.php");
}
$uid=$_SESSION['uid'];
foreach( $conn->dbh->query("SELECT max(form_id) as 'maxform' FROM tbl_image_master where user_id=$uid") as $row) {
    $g =$row['maxform']+1;
}
$jobid= $_GET['f'];
$formid=0;
$filesarr=array();
if(isset($_GET['p'])){
      $pages=$_GET['p'];
      $formname="template ".$g;
      $jobquery="INSERT INTO tbl_template_details(name) VALUES ('$formname')";
      $sth = $conn->dbh->prepare($jobquery);
      $sth->execute() ;
      $formid = $conn->dbh->lastInsertId();
      $p=1;
      $curpage=$p;
      
      
      foreach( $conn->dbh->query("SELECT upload_name FROM tbl_job_upload_details where job_id=$jobid limit $pages") as $row) {
          $filename=$jobs_path.$jobid.'/'.$row["upload_name"];
          $fileExtn = getExtension($filename);
          if($fileExtn == 'pdf'){
              $lengthcmnd="pdfinfo $filename | grep Pages: | awk '{print $2}'";
              $length=trim(exec($lengthcmnd));
              $execCmd="convert -quality 00 -density 300x300 -scene $p $filename ".$upload_path.$uid."_".$formid."_%d.jpg";
              exec($execCmd);
              for($i=0;$i<$length;++$i){
                  $imgname=$uid.'_'.$formid.'_'.($i+$p).".jpg";
                  $upfilename=getfilename($row["upload_name"]);
                  $upfilename=$upfilename."_".($i+1).".jpg";
                  array_push($filesarr,array('img'=>$upload_path.$imgname,'page'=>($i+$p),'uploaded'=>$upfilename));
                  ++$curpage;
                  list($width, $height, $type, $attr) = getimagesize($upload_path.$imgname);
                  $query="INSERT INTO tbl_image_master(user_id,form_id,page_no,user_image_name,data_img_name,width,height,active) VALUES (:uid,:f,:p,:filename,:kl,:width,:height,1)";
                  $sth = $conn->dbh->prepare($query);
                  $sth->execute(array(':uid'=>$uid,':f'=>$formid,':p'=>($i+$p),':filename'=>$upfilename,':kl'=>$upload_path.$imgname,':width'=>$width,':height'=>$height)) or die(var_dump($sth->errorInfo()));
      
              }
          }
          else {
              $imgname=$uid.'_'.$formid.'_'.($curpage).".jpg";
              $renamecmd= "cp $filename $upload_path".$imgname;
              exec($renamecmd);
              array_push($filesarr,array('img'=>$upload_path.$imgname,'page'=>($curpage),'uploaded'=>$row["upload_name"]));
              list($width, $height, $type, $attr) = getimagesize($upload_path.$imgname);
              $query="INSERT INTO tbl_image_master(user_id,form_id,page_no,user_image_name,data_img_name,width,height,active) VALUES (:uid,:f,:p,:filename,:kl,:width,:height,1)";
              $sth = $conn->dbh->prepare($query);
              $sth->execute(array(':uid'=>$uid,':f'=>$formid,':p'=>($curpage),':filename'=>$row["upload_name"],':kl'=>$upload_path.$imgname,':width'=>$width,':height'=>$height)) or die(var_dump($sth->errorInfo()));
              ++$curpage;
          }
      
      
      }
}

?>

<html>

<link type="text/css" rel="stylesheet" href="css/default.css" />
<link href="css/googlefonts.css" rel='stylesheet' type='text/css' />

<style>
div.fg {
	background-color: lightgrey;
	width: 1000px;
	padding: 25px;
	border: 5px solid navy;
	margin: 25px;
}

button.clr {
	color: red;
}

#popup {
	z-index: 9;
	opacity: 0.92;
	position: absolute;
	top: 0px;
	left: 0px;
	height: 100%;
	width: 100%;
	background: #F0F0F0;
	display: none;
}

form.pos_fixed {
	width: 400px;
	color: #92AAB0;
	position: fixed;
	top: 30%;
	left: 40%;
	background: #D0D0D0;
	border-radius: 4px;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.9);
}

#cancelpopup {
	z-index: 9;
	opacity: 0.97;
	position: absolute;
	top: 0px;
	left: 0px;
	height: 100%;
	width: 100%;
	background: #F0F0F0;
	display: none;
}

#cancelmessage {
	width: 400px;
	height: 150px;
	color: #92AAB0;
	position: fixed;
	top: 30%;
	left: 30%;
	background: #F0F0F0;
	border-radius: 4px;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.9);
}

.formbuttons {
	padding: 10px;
	background-color: #FB8C2D;
	color: white;
	font-size: 14px;
	font-weight: 500;
	border: none;
	cursor: pointer;
	border-radius: 3px;
}

#dropbox {
	float: left;
	margin-left: 20px;
	width: 110px;
	height: 170px;
	background: #f5f5f5;
	border-style: dotted;
	border-width: 2px;
	cursor: pointer;
	border-color: #aaa;
}
</style>



<div
	style="background-color: rgba(0, 0, 0, 0.9); width: 100%; height: 100%; position: relative;">


	<?php include('new_header_two.php');?>

	<div id="container"
		style="position: fixed; z-index: 999999; top: 0; left: 0; right: 0; bottom: 0; padding: 20px 0;">
		<div class="center">

			<div id="templates"
				style="background-color: #eeeeee; padding: 20px; position: relative; top: 0px; width: 960px; border-radius: 3px; min-height: 630px; overflow: auto;">
				<div id="arrows">
					<div class="center">
						<ul>
							<li><a href="sessionjobs.php?jid=<?php echo $jobid;?>"> <span
									class="pos"><img src="images/step1small.png" alt="step1" /> <b>Jobs</b>
								</span> <span class="arrow-right"></span>
							</a>
							</li>

							<li><a href="#"> <span class="pos"> <img
										src="images/step2small.png" alt="step1" /><b>Templates</b>
								</span> <span class="arrow-right"></span>
							</a>
							</li>

							<li><a href="#" style="opacity: 0.2;"> <span class="pos"><img
										src="images/step3small.png" alt="step1" /><b>Inbox</b> </span>
									<span class="arrow-right"></span>
							</a>
							</li>
						</ul>
						<div class="clear"></div>

						<a href="#" class="close" id="closeicon"> x</a>
					</div>
					<!---end of arrows center--->
				</div>
				<!---end of arrows--->

				<div id="templatecon">
					<div id="temp_one">
						<h2 class="headnewbig" style="font-size: 35px; color: #FF942F;">add
							new Template</h2>
						<input type="submit" id="submitbutt" value="Finish" class="sub"
							style="float: right;" onclick="finish();" />
					</div>

					<div class="clear"></div>
					<form id="data" style="background-color: #eeeeee;">
						<input id="clickk" name="profileImg[]" type="file"
							accept="image/x-png, image/gif, image/jpeg ,application/pdf"
							multiple="multiple" style="visibility: hidden; display: none;" /><br />
					</form>
					<div id="cancelpopup">
						<div id="cancelmessage" style="background-color: #eeeeee;">
							<br />
							<h3>You are about to cancel the process</h3>
							<img src="images/close.png" height="40" width="40" id="cancel"
								style="position: relative; left: 260px; top: -64px" /> <input
								type="button" id="ok" value="ok" class="formbuttons"
								style="position: relative; left: -20px; top: 24px; width: 100px" /><br />
						</div>
					</div>
					<div style="background-color: #eeeeee;">

						<div class="right">
							<div style="background-color: #eeeeee; display: none;">


								<progress id="progressBar" value="0" max="100"
									style="width: 150px;"></progress>
								<h3 id="status"></h3>
								<p id="loaded_n_total"></p>

							</div>
						</div>
					</div>
					<div id="popup" style="background-color: #eeeeee;">
						<form class="pos_fixed" action="#" id="contact">
							<h3>Enter Template Name</h3>
							<textarea id="c_az"></textarea>
							<img src="images/close.png" height="40" width="40" id="cancel"
								style="position: relative; left: 44px; top: -150px" /><br /> <input
								type="button" id="ok" value="Ok" />
						</form>
					</div>


					<div id="container" style="background-color: #eeeeee;">
						<div id="uploadedfiles"></div>
						<div id="dropbox">
							<div style="opacity: 1;">
								<p>
									Drag page <span id="countervalue">1</span> here
								</p>
								<p>Or click to browse</p>
							</div>
						</div>
					</div>

				</div>
				<!-- end of templatecon-->
			</div>
			<!-- end of templates -->
		</div>
		<!-- end of container center -->
	</div>
	<!-- end of container -->



</div>

<script src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>
<script src="<?php echo auto_version('/js/raphael.min.js');?>"></script>
<script src="<?php echo auto_version('/js/sessionnewtemplate.js');?>"></script>
<script>
//variables
var formid=<?php echo $formid;?>;
var counter=0;
var netc=0;
var dropbox = document.getElementById("dropbox");
var pdfimage ="images/pdf.png";
var testvar;
var g= <?php echo $g;?>;
document.getElementById("c_az").value ="template "+g;

$('#submitbutt').css('opacity','1');
//
if(formid!=0){
 var jobimages=<?php echo json_encode($filesarr)?>; 
for(i in jobimages){
 $("#uploadedfiles").append('<div id="file'+counter+'" style="float:left;width: 130px; margin-left: 5px;  height: 180px;"><br/><img id="img'+counter+'" src="'+jobimages[i].img+'" height="130" width="90" /><p id="pagefont'+counter+'" style=" color:black;opacity:0.5;font-size: 80px;position:relative;left: 20px;top:-104px">'+(netc+1)+'</p><p style=" font-size: 15px;position:relative;left: 5px;top: -110px;">'+jobimages[i].uploaded+'</p><img src="images/close.png" height="20" width="20"  style="position:relative;left: 40px;top: -270px;" onclick="deletepage('+counter+');"/></div>');
++counter;
++netc;  
$('#countervalue').text(netc+1);
}
}
$("#popup #cancel").click(function() {
    $(this).parent().parent().hide();
});
$("#cancelpopup #cancel").click(function() {
    $(this).parent().parent().hide();
});

$("#cancelpopup #ok").click(function() {
    
    window.location.href='jobs.php';
   
});
$("#closeicon").click(function() {

$('#cancelpopup').show();   
});

$("#popup #ok").click(function() {
    var n= $('#c_az').val();
    $.ajax({
        type: "POST",
        url: "templatename.php",
        data: { 'name': n ,'form':formid,'logentry':'1'}
    }).done(function() {
        window.location.href='createtemplate.php?g='+formid+"&j="+<?php echo $jobid?>;
    });
});


function finish()
{
    if(counter!=0)
        $("#popup").css("display", "block");
}

//Adding events to Dropbox

dropbox.addEventListener("dragenter", dragenter, false);
dropbox.addEventListener("dragover", dragover, false);
dropbox.addEventListener("drop", drop, false);
dropbox.addEventListener("click", oninput, true);

//files by clicking basket
$("#data").change(function(event){
     event.stopPropagation();
      event.preventDefault();
      var filesArray = document.getElementById('clickk').files;
        sendFile(filesArray,0);

});


</script>
</html>
