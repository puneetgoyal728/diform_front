<?php

session_start();

include_once("config.php");
include_once ('db_conn.php');
include_once('common_func.php');
checksession();
$t=0;
if(isset($_GET['name']))
{
    $t= $_GET['name'];
}
$currentHead = "TEMPLATE";
?>

<html>

<link type="text/css" rel="stylesheet" href="css/default.css" />
<link href="css/googlefonts.css" rel='stylesheet' type='text/css' />

<style>
div.fg {
	background-color: lightgrey;
	width: 1000px;
	padding: 25px;
	border: 5px solid navy;
	margin: 25px;
}

button.clr {
	color: red;
}
</style>



<div style="background-color:rgba(0,0,0,0.9); width:100%; height:100%; position:relative;">


	<?php include('new_header_two.php');?>

	<div id="container" style="position:fixed; z-index:999999; top:0; left:0; right:0; bottom:0; padding:20px 0;">
		<div class="center">

			<div id="templates" style="background-color:#eeeeee; padding:20px; position:relative; top:0px; width:960px; border-radius:3px; min-height:630px; overflow:auto;">
<div id="arrows">
	<div class="center">
		<ul>
			<li>
				<a href="sessionjobs.php?jid=<?php echo $t;?>">
				<span class="pos"><img src="images/step1small.png" alt="step1"/> <b>Jobs</b></span>
				<span class="arrow-right"></span>
				</a>
			</li>
			
			<li>
				<a href="#">
				<span class="pos"> <img src="images/step2small.png" alt="step1"/><b>Templates</b></span>
				<span class="arrow-right"></span>
				</a>
			</li>
			
			<li>
				<a href="inbox2.php" style="opacity:0.2;">
				<span class="pos"><img src="images/step3small.png" alt="step1"/><b>Inbox</b></span>
				<span class="arrow-right"></span>
				</a>
			</li>
		</ul>
		<div class="clear"></div>
		
		<a href="#" class="close" id="closeicon"> x</a>
	</div><!---end of arrows center--->
</div><!---end of arrows--->

				<div id="templatecon">
					<div id="temp_one">
						
						<a class='button' id="newtemplate"  style="margin:50px 0 20px 10px;">ADD NEW TEMPLATE</a>
					</div>
				
					<div id="or">
						
					</div>
					

					
					<div id="temp_two" style="margin-top:20px; text-align:center;">
						<h2 class="headnewbig" style="font-size:25px;">Select from Existing</h2>
						<input type="submit" id="submitbutt" value="Submit Job" onclick="submitconn();" class="sub" style="float:right;"/>
	
						<?php 
	
						$uid = $_SESSION['uid'];
	
						foreach( $conn->dbh->query("SELECT count(form_id) countfid FROM tbl_image_master where active = 1 and user_id=".$uid) as $row) {
						    $fid =$row['countfid'];
						}
						if($fid==0)
							echo '<div style="text-align:center; font-size:14px; width:300px; margin:auto;"></div>';
	                    include('showsessiontemplate.php');
						?>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
	
				</div><!-- end of templatecon-->
			</div><!-- end of templates -->
		</div>
		<!-- end of container center -->
	</div>
	<!-- end of container -->



</div>
</html>
