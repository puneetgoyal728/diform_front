<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css"/>
<link type="text/css" rel="stylesheet" href="css/googlefonts.css"/>
<title>Form Fly - Sign Up </title>
<style>
#login label{
    display: inline-block;
    width: 100px;
    text-align: right;
}
#login_submit{
    padding-left: 100px;
}
#login div{
    margin-top: 1em;
}

span .error{
    display: none;
    margin-left: 10px;
}      
 
.error_show{
    color: red;
    margin-left: 10px;
}
input.invalid, textarea.invalid{
    border: 2px solid red;
}
 
input.valid, textarea.valid{
    border: 2px solid green;
}

#loading{
    display: none;
   
} 
</style>
</head>
<body>
   
    <div id="header2">
        <div class="center">
            <a href="index.php">
                <img src="images/beta.png" alt="Form Fly - Sign Up"/>
            </a>                        
        </div><!---end of header2 center--->
        
        <div class="center" style = "margin-top: 55px;">
            <div id="form2">
                <div class="left" >
                    
                </div>
                
                <div class="right" style =  "border-width: 2px; border-radius: 15px; ">
                <form id="login" action="usersignupprocess.php" method="post" onsubmit="document.getElementById('signup').style.display='none'; document.getElementById('loading').style.display='block';" >
                    <ul>
                        <li>
                            <input type="text" placeholder="Name" class="inp" id="login_name" name="name"/>
                            
                        </li>
                            
                        <li>
                            <input type="text" placeholder="Email" class="inp" id="login_email" name="email"/>
                        </li>
                            
                        <li>
                            <input type="password" placeholder="Password" class="inp" id="login_password" name="password"/>
                        </li>
                        
                        <li>
                            <input type="text" placeholder="Contact No" class="inp" id="login_mobile" name="mobile"/>
                        </li>
                            
                        <li>
                            <button type="submit" value="Signup" class="submit" id="signup">Signup</button>
                            <img src="images/loading.gif" id="loading" height="42" width="420">
                        </li>
                    </ul>
                    </form>
                </div>
                <div class="clear"></div>
            </div><!---end of form2--->
        </div><!---end of header2 center--->
    </div><!---end of header2--->
    </div><!---end of background--->
    
   
    <div id="footer">
        <?php include('footer.php'); ?>
    </div><!---end of footer--->
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script>
var fe=0;
var fp=0;
var fn=0;
var fm=0;
$('.submit').attr("disabled", true);
$('.submit').css("opacity", 0.30);

$('#login_name').on('input', function() {
    
    var input=$(this);
    var regex=/^([-a-z0-9_-])+$/i;
    var is_name=input.val();
    var is_str=regex.test(is_name);
    if(is_name.length>=2 && is_str==true)
        {
          fn=1;
         
        input.removeClass("invalid").addClass("valid");
        }
    else
        {
        fn=0;
        input.removeClass("valid").addClass("invalid");
        }
    if(fe==0 || fp==0 ||fn==0 ||fm==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
    else{
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        }
});
$('#login_mobile').on('input', function() {
    
    var input=$(this);
    var is_name=input.val();
    var regex=/^([0-9\-+])+$/i;
    var is_mobile=regex.test(is_name);
    if(is_name.length>=10 && is_mobile==true)
        {
        fm=1;
      
        input.removeClass("invalid").addClass("valid");
        }
    else
        {
        fm=0;
        
        input.removeClass("valid").addClass("invalid");
        }
    if(fe==0 || fp==0 ||fn==0 ||fm==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
    else{
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        }
});

$('#login_password').on('input', function() {
    var input=$(this);
    var is_name=input.val();
    
    if(is_name.length>=6)
        {
        fp=1;
        input.removeClass("invalid").addClass("valid");
        }
    else{
        fp=0;
        input.removeClass("valid").addClass("invalid");
        }
    if(fe==0 || fp==0 ||fn==0 ||fm==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
    else{
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        }
});

$('#login_email').on('input', function() {
    var input=$(this);
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var is_email=re.test(input.val());
    var emailid=$('#login_email').val();
    var type="user";
    if(is_email)
    {
        
    $.ajax({
        type: "POST",
        url: "validateemail.php",
        data: { 'email': emailid,'type' : type}
      }).done(function(data) {
            
            if(data=="0")
            {
                fe=1;
            input.removeClass("invalid").addClass("valid");
            }
            else
            {
                fe=0;
                input.removeClass("valid").addClass("invalid");
            }
            
            });
    
    }
else{
    fe=0;
    input.removeClass("valid").addClass("invalid");
    }

    if(fe==0 || fp==0 ||fn==0 ||fm==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
    else{
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        }
});


</script>
</html>