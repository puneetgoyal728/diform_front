<?php
include_once 'config.php';
include_once 'paymentHelper.php';
if(isset($_GET['job_type']) && isset($_GET['salt'])&& isset($_GET['hash']) && isset($_GET['task_id']) && isset($_GET['data']) && isset($_GET['worker_id'])){
    $salt = $_GET['salt'];
    $jobType = $_GET['job_type'];
    $hash = $_GET['hash'];
    $txnId = $_GET['task_id'];
    $val = $_GET['data'];
    $worker_id = $_GET['worker_id'];
    $key = $squadTaskReturnKey.$salt;
    $data = $jobType."||".$txnId."||".$val."||".$worker_id;
    $genHash = hash_hmac ( "sha256" , $data , $key);
    if(strcmp($hash, $genHash) == 0){
        echo json_encode(evaluateSquadrunResp($txnId,$val,$worker_id));
    }else{
        echo json_encode(array('status'=>-1,'msg'=>'key mismatch'));
    }
}else{
    echo json_encode(array('status'=>-5,'msg'=>'param error'));
}