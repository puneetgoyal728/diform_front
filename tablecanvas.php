<html>
<div id="paper"></div>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/raphael.min.js"></script>
<script>
var paper = Raphael("paper", 1300, 1500);
var offx = $('#paper').offset().left;
var offy = $('#paper').offset().top;
var img1 = paper.image("images/scan0007.JPG",0,0,1300,1700);
$(img1.node).on('dragstart',function(event){event.preventDefault();});
var addimage ="images/add.png";
var delimage ="images/dele.png";
var tableObjs= new Array();

function tableObj(i,x,y,xx,yy){
     var rec_details;
     var col_details=[];
     var row_details=[];
     this.recs=paper.rect(x,y,xx,yy);
     this.col = paper.set();
     this.row = paper.set();
     this.addcol=paper.image(addimage,x,160,20,20);
     this.addrow=paper.image(addimage,x,190,20,20);
     this.delcol=paper.image(delimage,x,210,20,20);
     this.delrow=paper.image(delimage,x,240,20,20);

     this.recs.attr("fill", "BLUE");
     this.recs.attr("opacity", "1");
     this.recs.attr("fill-opacity", "0.10");
     this.recs.mousemove(changeCursor);
     this.recs.drag(dragMove, dragStart, dragEnd);
     
     this.addnewcolumn = function()
     {
         var recx = tableObjs[i].recs.attr('x');
         var recy = tableObjs[i].recs.attr('y');
         var recw = tableObjs[i].recs.attr('width');
         var rech = tableObjs[i].recs.attr('height');
         var last_col=recx;
         for(var j = 0; j < tableObjs[i].col.length; j++) 
         {
             tableObjs[i].col[j].attr({opacity:30});
             if(tableObjs[i].col[j].attr('x')>last_col)
             last_col= tableObjs[i].col[j].attr('x');
         }
         if(last_col+30<recx+recw-10)
         {
        var elem=paper.rect(last_col+30,recy,1,rech);
        elem.attr({opacity:1});
        elem.mousemove(changeCursorcol);
        elem.drag(dragMove, dragStart, dragEnd);
        tableObjs[i].col.push(elem);
        
        tableObjs[i].addcol.attr({x:last_col+30,y:recy});
         }
         else
             alert('not enough space');
    }

     this.addnewrow = function()
     {
         var recx = tableObjs[i].recs.attr('x');
         var recy = tableObjs[i].recs.attr('y');
         var recw = tableObjs[i].recs.attr('width');
         var rech = tableObjs[i].recs.attr('height');
         var last_row=recy;
         for(var j = 0; j < tableObjs[i].row.length; j++) 
         {
             tableObjs[i].row[j].attr({opacity:30});
             if(tableObjs[i].row[j].attr('y')>last_row)
                 
             last_row= tableObjs[i].row[j].attr('y');
         }
         if(last_row<recy+rech-20)
         {
        var elem=paper.rect(recx,last_row+20,recw,1);
        elem.attr({opacity:1});
        elem.mousemove(changeCursorrow);
        elem.drag(dragMove, dragStart, dragEnd);
        tableObjs[i].row.push(elem);
        tableObjs[i].addrow.attr({x:recx+3,y:last_row+20});
         }
         else
             alert('not enough space');
         }

     this.deletecolumn = function()
     {
         var elem =tableObjs[i].col.pop();
         elem.remove();
         var last_col=tableObjs[i].recs.attr('x');
         for(var j = 0; j < tableObjs[i].col.length; j++) 
         {
             if(tableObjs[i].col[j].attr('x')>last_col)
             last_col= tableObjs[i].col[j].attr('x');
         }
        
        tableObjs[i].addcol.attr({x:last_col,y:tableObjs[i].recs.attr('y')});
         }
     this.deleterow = function()
     {
         var elem =tableObjs[i].row.pop();
         elem.remove();
         var last_row=tableObjs[i].recs.attr('y');
         for(var j = 0; j < tableObjs[i].row.length; j++) 
         {
             if(tableObjs[i].row[j].attr('y')>last_row)
                 
             last_row= tableObjs[i].row[j].attr('y');
         }
        tableObjs[i].addrow.attr({x:tableObjs[i].recs.attr('x')+3,y:last_row});

         }
     
     this.addcol.click(function (event, a, b) {
         tableObjs[i].addnewcolumn();
      });
     this.addrow.click(function (event, a, b) {
         tableObjs[i].addnewrow();
      });
     this.delcol.click(function (event, a, b) {
         tableObjs[i].deletecolumn();
      });
     this.delrow.click(function (event, a, b) {
         tableObjs[i].deleterow();
      });


     var dragStart = function() {
         rec_details=[{'x' : tableObjs[i].recs.attr('x'),'y' : tableObjs[i].recs.attr('y'),'w' : tableObjs[i].recs.attr('width'),'h': tableObjs[i].recs.attr('height')}];
            for(var j = 0; j < tableObjs[i].col.length; j++) 
                    col_details[j]=[{'x' : tableObjs[i].col[j].attr('x'),'y' : tableObjs[i].col[j].attr('y')}];
                   
            for(var j = 0; j < tableObjs[i].row.length; j++) 
                    row_details[j]=[{'x' : tableObjs[i].row[j].attr('x'),'y' : tableObjs[i].row[j].attr('y')}];
            rox=this.attr('x');
            roy=this.attr('y');
            addcol_details=[{'x' :tableObjs[i].addcol.attr('x'),'y' :tableObjs[i].addcol.attr('y')}];
            addrow_details=[{'x' :tableObjs[i].addrow.attr('x'),'y' :tableObjs[i].addrow.attr('y')}];
        this.dragging = true;
     };


     function dragMove(dx, dy) {
         // Inspect cursor to determine which resize/move process to use
         switch (this.attr('cursor')) {

             case 'nw-resize' :
                 if(this.oh-dy > 20) 
                 this.attr({
                     y: this.oy + dy, 
                     height: this.oh - dy
                     });
                 if( this.ow -dx >20)
                     this.attr({
                         x: this.ox + dx, 
                         width: this.ow - dx
                     }); 
                 var wid =this.ow;
                 delicon.attr({
                     
                     x: this.ox -5+wid, 
                     y: this.oy + dy-15
                 });
                 
                 break;

             case 'ne-resize' :
                 if(this.ow +dx >20){ 
                 this.attr({
                     
                     
                     width: this.ow + dx 
                     
                 });
                 var wid =this.ow;
                 delicon.attr({
                     
                     x: this.ox + dx-5+wid
                 });
                 }
                 if(this.oh-dy > 20 ){
                     this.attr({
                         
                         y: this.oy + dy , 
                         
                         height: this.oh - dy
                     });
                 var wid =this.ow;
                 delicon.attr({
                     
                     
                     y: this.oy + dy-15
                 });

                 }
                 break;

             case 'se-resize' :
                 if(this.oh+dy > 20){
                     this.attr({
                         height: this.oh + dy
                     });}
                 if(this.ow +dx >20)
                     this.attr({
                         width: this.ow + dx
                     });
                     var wid =this.ow;
                     delicon.attr({
                         
                         x: this.ox + dx-5+wid
                     });

                     
                 break;

             case 'sw-resize' :
                 if( this.ow -dx >20) 
                        this.attr({
                     x: this.ox + dx, 
                     width: this.ow - dx
                     
                 });
                 if(this.oh+dy > 20)
                     this.attr({
                         height: this.oh + dy
                     });
                 var wid =this.ow;
                 delicon.attr({
                     
                     x: this.ox -5+wid
                 });
                 break;

              case 's-resize' :
                 if(this.oh+dy > 20){
                 this.attr({ 
                   height:  this.oh+dy
                     });
                 }
                 var wid =this.ow;
                 delicon.attr({
                     
                     x: this.ox -5+wid,
                     y:this.oy-15
                 });
                 break;
                         
              case 'n-resize' :
                  if(this.oh-dy > 20){
                     this.attr({ 
                          
                         y: this.oy + dy, 
                         height: this.oh - dy
                     });
                     var wid =this.ow;
                     delicon.attr({
                         y: this.oy + dy-15,
                         x: this.ox -5+wid
                     });
                     }
                     break;

              case 'w-resize' :
                  if(this.ow-dx > 20){
                      this.attr({ 
                         x: this.ox + dx,
                         width: this.ow - dx
                 });
                     var wid =this.ow;
                     delicon.attr({
                         
                         x: this.ox -5+wid,
                         y:this.oy-15
                     });

                     }
                 
                 break;
                 
               case 'e-resize' :
                   var wid =this.ow;
                  if( this.ow +dx >20){
                 this.attr({ 
                 width: this.ow + dx
                 });
                 delicon.attr({
                     y: this.oy -15,
                     x: this.ox + dx-5+wid 
                    
                 });
                 }
                 break;

               case 'col-resize' :
                   this.attr({x:rox+dx}); 
                   if(this.attr('opacity')==1)
                   tableObjs[i].addcol.attr({x:addcol_details[0]['x']+dx}); 
                   
                 break;


               case 'row-resize' :
                  this.attr({y:roy+dy}); 
                  if(this.attr('opacity')==1)
                  tableObjs[i].addrow.attr({y:addrow_details[0]['y']+dy}); 
                  
                 break;
                 

              default :
                  
                  
                      
                  tableObjs[i].recs.attr({
                          x:rec_details[0]['x'] + dx, 
                          y:rec_details[0]['y'] + dy
                      });
              for(var j = 0; j < tableObjs[i].col.length; j++) 
              {
                  tableObjs[i].col[j].attr({
                      x:col_details[j][0]['x'] + dx, 
                      y:col_details[j][0]['y'] + dy
                  });

                  }
              for(var j = 0; j < tableObjs[i].row.length; j++) 
              {
                  tableObjs[i].row[j].attr({
                      x:row_details[j][0]['x'] + dx, 
                      y:row_details[j][0]['y'] + dy
                  });

                  }
              
              tableObjs[i].addcol.attr({x:addcol_details[0]['x']+dx,y:addcol_details[0]['y']+dy});   
              tableObjs[i].addrow.attr({x:addrow_details[0]['x']+dx,y:addrow_details[0]['y']+dy});   
                 
             
            
                

                 break;
             }
     }
     var dragEnd = function() {
         this.dragging = false;
        
     };

     var changeCursorcol = function(e, mouseX, mouseY) {
         this.attr('cursor', 'col-resize');
       };

     var changeCursorrow = function(e, mouseX, mouseY) {
         this.attr('cursor', 'row-resize');
     };

     var changeCursor = function(e, mouseX, mouseY) {
         
         // Don't change cursor during a drag operation
         if (this.dragging === true) {
             return;
         }

         // X,Y Coordinates relative to shape's orgin
         var relativeX = mouseX - $('#paper').offset().left - this.attr('x');
         var relativeY = mouseY - $('#paper').offset().top - this.attr('y');

         var shapeWidth = this.attr('width');
         var shapeHeight = this.attr('height');

         var resizeBorder = 5;

         // Change cursor
         if (relativeX < resizeBorder && relativeY < resizeBorder) { 
             this.attr('cursor', 'nw-resize');
                        }
                      
         else if (relativeX > shapeWidth-10 && relativeY < 10 )  
             this.attr('cursor', 'ne-resize');
         else if (relativeX < resizeBorder +5   && relativeY < resizeBorder+5) 
              this.attr('cursor', 'nw-resize');
         else if (relativeX > shapeWidth-resizeBorder -5 && relativeY > shapeHeight-4) { 
             this.attr('cursor', 'se-resize');
         } else if (relativeX < resizeBorder +5 && relativeY > shapeHeight-4) { 
             this.attr('cursor', 'sw-resize');
                      } else if (relativeY < resizeBorder &&  relativeX < shapeWidth-resizeBorder ) { 
             this.attr('cursor', 'n-resize');
                      } else if (relativeY > shapeHeight-resizeBorder ) { 
             this.attr('cursor', 's-resize');
                      } else if ( relativeX < resizeBorder) { 
             this.attr('cursor', 'w-resize');
                      } else if (relativeX > shapeWidth-resizeBorder && relativeY > 10 ) { 
             this.attr('cursor', 'e-resize');
         }else { 
             this.attr('cursor', 'move');
         }
     };


      
     this.recs.mousemove(changeCursor);
     this.recs.drag(dragMove, dragStart, dragEnd);
     
     
    }


var x,y,flag;
var count=-1;
img1.mousedown(function (event, a, b)
 {
    x=a;
    y=b;
    flag=1;
    tw=0;
  });
img1.mousemove(crtemp);

function crtemp (e,a,b){
    if(flag==1 && count<0)
    {
        if(x-a !=0 && y-b !=0 && a-x !=0 && b-y!=0 && x-a !=-1 && y-b !=-1 && a-x !=-1 && b-y!=-1 )
        {
            if(tw==1)
            req.remove();
            var t=a;
            var u=b;
            
            if(u>=y && t>=x)
                req = paper.rect(x-offx, y-offy, t-x-1, u-y-1);
            else if (t>x && u<y)
                req = paper.rect(x-offx, u-offy, t-x-1, y-u-1);
            else if (x>t && y>u)
                req = paper.rect(t-offx-2, u-offy-2, x-t+1, y-u+1);
            else if (t<x && u>y)
                req = paper.rect(t-offx, y-offy, x-t-2, u-y-2);
            tw=1;
        }
    }
}
img1.mouseup(createrect);

function createrect(event, a, b) 
{
 req.remove();
 flag=0;
    var c=a;
    var d=b;
    if((c-x > 10 || x-c > 10) && count<0)
    {
        ++count;
        var tablenew = new tableObj(count,x-offx,y-offy,c-x-1,d-y-1);
        tableObjs.push(tablenew);
        tablenew.addnewcolumn();
        tablenew.addnewrow();
              
       }
}



</script>
</html>