<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
include_once('common_func.php');
checksession();
$t=0;
if(isset($_GET['name']))
{
    $t= $_GET['name'];
}
$currentHead = "TEMPLATE";

?>
<html>
<head>
<title>
Templates Page
</title>
<link type="text/css" rel="stylesheet" href="<?php echo auto_version('/css/default.css');?>"/>
<script type="text/javascript" src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>

<style>
div.fg {
	background-color: lightgrey;
	width: 1000px;
	padding: 25px;
	border: 5px solid navy;
	margin: 25px;
}

button.clr {
	color: red;
}
</style>
</head>
<body>
<?php include("jobsheader.php");?>
<div style="background-color: #eeeeee;">
	<div id="container">
		<div class="center">

			<div id="templates">
				<div class="des_orcls">
					<p>or</p>
				</div>
				<div class="left" style="width: 499px;">

					<h2 class="headnewbig">Create New Template</h2>
					<a class='button' href="newtemplate.php"
						style="color: black; text-align: center; display: inline-block; margin-top: 50px;">ADD
						NEW TEMPLATE</a>
				</div>
				<div class="right"
					style="border-left: 2px black solid; width: 498px; ">
					<h2 class="headnewbig">Select from Existing</h2>
					<?php 
					$uid = $_SESSION['uid'];
					foreach( $conn->dbh->query("SELECT count(form_id) countfid FROM tbl_image_master where active = 1 and user_id=".$uid) as $row) {
					    $fid =$row['countfid'];
					}
					if($fid==0)
					    echo "you have 0 template active\n";
					//include('display.php');
					include('showtemplate.php');
					?>

				</div>
				<div class="clear"></div>
			</div>
			<!-- end of templates -->
		</div>
		<!-- end of container center -->
	</div>
	<!-- end of container -->

</div>
</body>
</html>
