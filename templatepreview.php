<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
include_once 'common_func.php';
include_once('referenceArrays.php');
checksession();
$formid= $_GET['g'];
if(isset($_GET['j']))
    $jobid=$_GET['j'];
else
    $jobid=0;
    
if(!isset($_GET["p"]))
{

 foreach( $conn->dbh->query("SELECT data_img_name ,min(page_no) page FROM tbl_image_master where form_id = $formid and active=1" ) as $row)
     {
         $wert =$row['page'];
         $l=$row['data_img_name'];
     }
}
else 
{
 $wert=$_GET["p"];
 
 foreach( $conn->dbh->query("SELECT data_img_name  FROM tbl_image_master where form_id = $formid and page_no=$wert" ) as $row)
     {
         $l=$row['data_img_name'];
     }
}

list($width, $height, $type, $attr) = getimagesize($l);
$pages[]=0;
$index=0;
$pageButtons = "<div class='center' style='padding:20px 0px 20px 0px; width:600px; text-align:right;'>";
foreach( $conn->dbh->query("SELECT *  FROM tbl_image_master where  form_id=$formid and active = 1") as $arr)
{
    $pages[$index]=$arr['page_no'];
    ++$index;
}

if($index==1)
{
    $pageButtons .= "<a class = 'button' style='opacity: 0.3; '>prev</a>";
    $pageButtons .= "<a class = 'button' style='opacity: 0.3; '>next</a>";

}
else {

    for($df=0;$df<$index;++$df)
    {
        if($pages[$df]==$wert)
        {
            if($df==0)
            {
                $next=$pages[$df+1];
                $prev=$pages[$df];
                $pageButtons .= "<a class = 'button' style='opacity: 0.3; ' >prev</a>";
                $pageButtons .= "<a class = 'button' href=templatepreview.php?g=".$formid."&p=".$next.">next</a>";
            }
            else if($df==$index-1)
            {
                $next=$pages[$df];
                $prev=$pages[$df-1];
                $pageButtons .= "<a class = 'button' href=templatepreview.php?g=".$formid."&p=".$prev.">prev</a>";
                $pageButtons .= "<a class = 'button' style='opacity: 0.3;'>next</a>";
            }
            else
            {
                $next=$pages[$df+1];
                $prev=$pages[$df-1];
                $pageButtons .= "<a class = 'button' href=templatepreview.php?g=".$formid."&p=".$prev.">prev</a>";
                $pageButtons .= "<a class = 'button' href=templatepreview.php?g=".$formid."&p=".$next.">next</a>";
            }
        }

    }
}

$pageButtons .= '<a class = "button" id ="finishbutton" href="template.php";>finish</a>';
$pageButtons .= "</div>";
//previous data from databaase
$ark=array();
foreach(  $conn->dbh->query("SELECT * FROM tbl_template_master where form_id =$formid and page_no=$wert") as $ro)
{

 $data=array("id"=>$ro['field_id'],"coordinates"=>$ro['field_cord'],"name"=>$ro['field_name'],"type"=>$ro['field_type'],"sub_type"=>$ro['sub_field_type'],"details"=>$ro['field_details'],"instructions"=>$ro['instructions'],"istable"=>$ro['istable']); 
 array_push($ark, $data)  ;
}
$currentHead = "TEMPLATE";


?>
<!doctype html>
<html lang="en" >
<head>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="<?php echo auto_version('/css/default.css');?>" />
<title>Movable and Re-sizable Raphael JS Shape</title>
</head>
<style>
#left_area {
    background-color: white;
    border: 1px #cccccc solid;
    width: 270px;
    position: fixed;
    right: 0;
    padding: 20px 10px;
    border-right: 0px;
    top: 208px;
    min-height: 300px;
    overflow-y: auto;
    height: 300px;
}

</style>
<body>
<?php include('jobsheader.php');  echo $pageButtons;?>
<div id="paper"></div>
<div id="left_area">
<h4>Template has been locked</h4>
<div id="result"></div></div>
<script src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>
<script src="<?php echo auto_version('/js/raphael.min.js');?>"></script>
<script src="js/templatepreview.js"></script>
<script src="<?php echo auto_version('/js/sidebar.js');?>"></script>
<script src="<?php echo auto_version('/js/jsrender.min.js');?>"></script>
<script id="theTmpl" type="text/x-jsrender">

<h3 id= "msg_pane">{{:title}}</h3>
        <div id = "buttons_pane">
            <input type="text" class="pos_fixed inp_side" placeholder="Enter Field Name" id="fname{{:id}}" disabled />
            <div id="dropdown{{:id}}" >
                <input id="dropselect{{:id}}" class="select_side_locked" disabled></input>
            </div>
            <div id="sub_title{{:id}}" >
                <input id="sub_title_option{{:id}}" class="select_side_locked" disabled></input>
                <div class="moptions multiple_options{{:id}}"></div>
            </div>
            <div id="specialinstructions{{:id}}" >
                <p>Special Instructions</p>

                <textarea rows="5" cols="22" id="instructions{{:id}}" disabled></textarea>

            </div>
        </div>
</script>
<script>
var fieldtype= <?php echo json_encode($fieldtype ); ?>;
var refsubtype= <?php echo json_encode($subtype ); ?>;
var fieldSubtypeMap= <?php echo json_encode($fieldSubtypeMap); ?>;

//variables
var delimage ="images/close.png";
var addcol ="images/add.png";
var delcol ="images/dele.png";
var count=1;
var tablecount=0;
var tableflag=0;
var cur;
var formid=<?php echo $formid?>;
var page=<?php echo $wert?>;
//Create drawing area
var paper = Raphael("paper", 915, 1500);
var offx = $('#paper').offset().left;
var offy = $('#paper').offset().top;
var php_var = "<?php echo $l; ?>";
var  imgwid = "<?php echo $width;?>"; 
var  imghei = "<?php echo $height;?>"; 
var zr=imageratio(imghei,imgwid);
var img1 = paper.image(php_var,0,0,imgwid,imghei);
$(img1.node).on('dragstart',function(event){event.preventDefault();});
var prev_data=<?php echo json_encode($ark ); ?>;
var fields=[];
fields.push('');

//starting panel
startpanel();

//draw previous fields
for(i in prev_data)
{
    if(prev_data[i].istable==1)
    {
        var g=JSON.parse(prev_data[i].coordinates);
        var cord=g.rec[0]+","+(g.rec[1])+","+g.rec[2]+","+(g.rec[3]);
        var  newfield=[{"prevflag":1,"id" : prev_data[i].id,"coordinates" :cord,"name" : prev_data[i].name,"type" : prev_data[i].type,"sub_type" :  prev_data[i].sub_type,"details" : prev_data[i].details,"instructions" : "","istable" : 1}];
        var f = new table(newfield[0],zr,1);
     fields.push(f);
     for(var j in g.col)
     fields[count].addnewcolumn((g.col[j])/zr);
     for(var j in g.row)
         fields[count].addnewrow((g.row[j])/zr);
     savedata(fields[count]);
     ++count;
     ++tablecount;
    }
    else{
        var resx = prev_data[i].coordinates.split(",");
        var cord=resx[0]+","+resx[1]+","+resx[2]+","+resx[3];
        var  newfield=[{"prevflag":1,"id" : prev_data[i].id,"coordinates" :cord,"name" : prev_data[i].name,"type" : prev_data[i].type,"sub_type" :  prev_data[i].sub_type,"details" : prev_data[i].details,"instructions" : "","istable" : 0}];
        var f = new normalfield(newfield[0],zr,1);
     fields.push(f);
     ++count;

        }
    
}



</script>
</body>
</html>
