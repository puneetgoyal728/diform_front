<?php 
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
include("jobsheader.php");
checksession();
$form=$_GET["f"];
$arr=array();
$listFormsQuery = "select tim.page_no page_no,tim.data_img_name db_img,tim.user_image_name usr_img ,ttd.name name,ttd.type type,ttd.rangefrom rf,ttd.rangeto rt from tbl_image_master tim join tbl_template_details ttd where tim.form_id =$form and ttd.form_id=$form and tim.active=1";

foreach($conn->dbh->query($listFormsQuery) as $row) {
    $type=$row['type'];
    $rf=$row['rf'];
    $rt=$row['rt'];
   $temp_name=$row['name'];
    $db_img =$row['db_img'];
    $usr_img=$row['usr_img'];
    $page_no=$row['page_no'];
    array_push($arr, array('thumb'=>$db_img,'name'=>$usr_img,'id'=>$page_no));
}
echo '<div id="templatecontainer" style="width: 430px;padding : 20px">';
$imgarr=array();
$index=0;
foreach ($arr as $value) {
 $img=$value["thumb"];
  $id=$value["id"];
  array_push($imgarr,array("id"=>$id,"img"=>$img));
  
    echo '<div id="border'.$value["id"].'"  style="float:left;width: 100px; margin-left: 5px;  height: 180px;"><br/>
    <img src="'.$value["thumb"].'" height="130" width="90" onclick="popup(\''.$index.'\');" /><p style=" font-size: 15px;">'.$value["name"].'</p>';
    echo '<img src="images/close.png" height="20" width="20"  style="position:relative;left: 79px;top: -160px;" onclick="deleteJob(\''.$img.'\',\''.$id.'\');"/></div>';
++$index;
}
echo '</div><div id="settings" style ="padding:50px 0px 0px 500px;"> 
template name :<input type="text" id ="templatename" name="temp_name" value="">
</br></br><input type="radio" id="splitall" name="type" onclick="splitall();" value="split" ' .(($type ==0) ?"checked" : "").'>Split all in single 
<br>
<input  type="radio" name="type" id="rangeradio" value="range" ' .(($type ==1) ?"checked" : "").'>range<form id ="range">
  pages between :
  <input type="number" id="rangefrom" name="quantity" min="1" max="20" value= '.(($type ==1) ?$rf : "").'><input type="number" id="rangeto" name="quantity" min="1" max="20" value= '.(($type ==1) ?$rt : "").'>
</form></div>';
?>
<html>
<link type="text/css" rel="stylesheet" href="css/default.css" />
<style>
.border {
    background-color: lightgrey;
    width: 300px;
    padding: 25px;
    border: 1px solid navy;
    margin: 25px;
}

#popup {
    z-index: 9;
    opacity: 0.92;
    position: absolute;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    background: #F0F0F0;
    display: none;
}

#main {
    width: 500px;
    height:630px;
    color: #92AAB0;
    position: fixed;
    top: 5%;
    left: 30%;
    background: #D0D0D0;
    border-radius: 4px;
    box-shadow: 0 0 5px rgba(0,0,0,0.9);
}
</style>
<div id="popup">
    <div id="main">
        <img src="images/close.png" height="40" width="40"  id="cancel" style="position:relative;left: 460px;top:-580px" />
       <img src="" height="600"   id="popupimg" style="position:relative;left: -20px;top:10px" />
        <img src="images/next.png" height="60" width="40"  id="next" style="position:relative;left:250px;top:-300px" />
         <img src="images/prev.png" height="60" width="40"  id="prev" style="position:relative;left: -250px;top:-300px" />
    </div>
</div>
<script src="js/jquery-1.11.1.min.js"></script>
<script>
var imgarr=<?php echo json_encode($imgarr);?>;
var nextid,previd;
var arrlength=imgarr.length;
function deleteJob (img,id) {
$.ajax({
     url: "deletepage.php",
     type: "POST",
     data: { "name": img }
 });
 $("#border"+id).remove();
}
$('#templatename').val("<?php echo $temp_name;?>");
$('#templatename').bind('input propertychange', function()
        {
    
    var data=$('#templatename').val();
	$.ajax({
        url: "templatename.php",
        type: "POST",
        data: { "name": data,"form" :"<?php echo $form;?>"}
    });
           });
$('#range').bind('input propertychange', function()
		{
	document.getElementById("rangeradio").checked=true;
var rangefrom=$('#rangefrom').val();
var rangeto=$('#rangeto').val();
$.ajax({
    url: "tempname.php",
    type: "POST",
    data: { "rangefrom": rangefrom,"rangeto" :rangeto,"form":"<?php echo $form;?>"}
}); 
});

function splitall()
{
	$('#rangefrom').val("");
	$('#rangeto').val("");
	$.ajax({
	    url: "tempname.php",
	    type: "POST",
	    data: { "type":"0","form":"<?php echo $form;?>"}
	}); 

     }
$("#cancel").click(function() {
    $(this).parent().parent().hide();
});

$("#next").click(function() {
 popup(nextid);
});
$("#prev").click(function() {
  popup(previd);
 });
function popup(id){
	$('#next').css("opacity",1); 
	$('#prev').css("opacity",1); 
    var src=imgarr[id].img;
    if(parseInt(id)<arrlength-1)
    nextid=parseInt(id)+1;
    else
    	$('#next').css("opacity",0.2);   
    if(parseInt(id)>0)
    previd=parseInt(id)-1;
    else
        $('#prev').css("opacity",0.2);  
    $('#popupimg').attr("src",src);
    $('#popup').show();
}
</script>
</html>
