

 <div id="testimonials">
  

      
<img src="images/prev.png" id="prv-testimonial" class="prevbtn" style = " border: 1px solid rgba(0, 0, 0, 0.48); border-radius: 50%; padding: 5px; width: 25px; margin-left: -40%; margin-top: 45px; position: absolute;">
 <img src="images/next.png" id="nxt-testimonial" class="nextbtn" style = " border: 1px solid rgba(0, 0, 0, 0.48); border-radius: 50%; padding: 5px; width: 25px; margin-left: 35%; margin-top: 50px; position: absolute;"> 

        <div class="carousel-wrap">


          <ul id="testimonial-list" class="clearfix">
            <li>
              <p class="context">"Their services are beyond anything I could have imagined. It gets my highest recommendation."</p>
            <p class="credits"> </p> 
            </li>
            <li>
              <p class="context">"Simple. Efficient. Quick production and easy to work with."</p>
              <p class="credits">  </p>
            </li>
            <li>
              <p class="context">"The marketing strategy is superb. These guys came through above and beyond expectations."</p>
              <p class="credits">  </p>
            </li>
            <li>
              <p class="context">"We needed a reliable team to help with some finishing touches."</p>
              <p class="credits">  </p>
            </li>
          </ul><!-- @end #testimonial-list -->
        </div><!-- @end .carousel-wrap -->
        
       
      </div><!-- @end #testimonials -->

   
