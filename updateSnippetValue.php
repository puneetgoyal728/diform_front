<?php
session_start();
include_once("config.php");
include_once ("db_conn.php");
include_once "jobResultClass.php";
include_once('common_func.php');
checksession();
if(isset($_SESSION["uid"])){
    $jrc = new JobResultClass;
	$uid = $_SESSION["uid"];
	$jid = $_REQUEST["jid"];
	$jpage = $_REQUEST["jpage"];
	$field = $_REQUEST["fid"];
	$val = $_REQUEST["val"];
	$isQueryValid = $jrc->isJobIdValid($jid,$uid);
	$status = false;
	if($isQueryValid["status"]==1){
	    if($field!=0){
    		$tsm_id = $jrc->getImageNameFromJobPg($jid,$jpage,$field);
    		$status = $jrc->updateValueOfSnippet($uid, $tsm_id, $val);
	    }
	}
	echo json_encode(array('status'=>$status,'data'=>$val));
}