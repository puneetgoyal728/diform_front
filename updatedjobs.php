<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('jobclass.php');
include_once('common_func.php');
checksession();
$uid =$_SESSION["uid"];
$currentHead = "JOBS";
?>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>Multiple File upload with PHP</title>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/raphael.min.js"></script>
</head>
<link type="text/css" rel="stylesheet" href="css/default.css" />
<style>
#fileItem {
	border: 2px dotted #0B85A1;
	width: 400px;
	color: #92AAB0;
	text-align: left;
	vertical-align: middle;
	padding: 10px 10px 10 10px;
	margin-bottom: 10px;
	font-size: 200%;
}

button.clr {
	color: red;
}

#popup {
	z-index: 9;
	opacity: 0.92;
	position: absolute;
	top: 0px;
	left: 0px;
	height: 100%;
	width: 100%;
	background: #F0F0F0;
	display: none;
}

form.pos_fixed {
	width: 400px;
	color: #92AAB0;
	position: fixed;
	top: 30%;
	left: 40%;
	background: #D0D0D0;
}
</style>

<body style="background-color:#eeeeee;">


<?php include("new_header.php"); ?>

<div id="inhead">
	<div class="center">
		<div class="left">
			<h3>Start New Job</h3>
			<a href="draftjobs2.php" class="right orgbutton">View Draft Jobs</a>
		</div>
		
		<div class="right">
			
		</div>
		<div class="clear"></div>
	</div>
</div><!---end of inhead--->

<div id="popup" style="display: none; background-color: rgb(238, 238, 238);">

	<form class="pos_fixed" action="#" id="contact" style="text-align:center;">
		<h3>Enter Job Name</h3>
		<textarea id="c_az"></textarea> <br/>
		<input type="button" id="ok" value="Send"> 
		<input type="button" id="cancel" value="Cancel" alt=""/> <br />
	</form>
</div>


<form action="template.php" method="post" id="templateset" style="background-color: #eeeeee;">
<input name="name" id="mytext" style="visibility: hidden; display:none;"/><br />
</form>

<div style="background-color: #eeeeee;">
<div class="center"></div>
</div>
<form id="data" style="background-color: #eeeeee;"><input id="clickk"
 name="profileImg[]" type="file" multiple="multiple"
 accept="image/x-png, image/gif, image/jpeg, application/pdf"
 style="visibility: hidden; display: none;" /><br />
</form>

<!-- <div style="background-color: #eeeeee; display: none;">
		<div class="center">
			<progress id="progressBar" value="0" max="100" style="width: 150px;"></progress>
			<h3 id="status"></h3>
			<p id="loaded_n_total"></p>
		</div>
	</div> -->


<div id="container" style="background-color: #eeeeee; padding: 0px 0;">
<div id="headcon">
</div>
<!---end of headcon--->
<div id="job">
<div id="heading">

<div class="left">

</div>

<div class="right">
<button class="button" onclick="deleteselected()">Delete selected</button>

<button class="button" onclick="deleteall()">Delete all</button>


</div>
<div class="clear"></div>
</div>

<div id="jobin">
<div class="left">
<div id="der"
 style="border: 1px black dotted; margin-top: 10px; overflow: auto; height: 430px;">
</div>
</div>

<div class="right">

<button class="next" onclick="sendtemp();">
	<span class="pos">Next</span>
	<span class="arrow-next"></span>
</button>
</div>



<div class="clear"></div>
</div>
</div>
<!---end of job--->

<!---end of container--->


<script type="text/javascript">

var sendno;
var count=0;

var loop =0;
var allcount=0;
var rt;
var next=0;


var reim = "<?php echo $images_path ?>drag.png";
var paper = Raphael("der",'100%','200');

var  recimg = paper.image(reim,51,42,550,150);
var t=paper.rect(48, 50, 800, 150).attr({fill: "white","stroke-dasharray":"---"}).node.setAttribute('id', 'fileItem');


document.getElementById("fileItem").setAttribute("stroke-dasharray", 2, 8, 5);
document.getElementById("fileItem").setAttribute("stroke-width",5);
document.getElementById("fileItem").setAttribute("stroke","black" );
document.getElementById("fileItem").setAttribute("opacity",0.3);  
var img =[];
var img1 =[];
var t =[];
var recs = [];
var page = []; 
var imgn =[];
var imgname =[];
var del =[];
var idel=0;
var cirmage ="images/close.png";
var pdfimage ="images/pdf.png";
var loader ="images/loader.gif";
var index =[];
var xas,yax;
var sele = [];
var loading;
function getExtension(filename){
	var fileNameArr = filename.split(".");
    var extensionIdx = fileNameArr.length -1;
    return fileNameArr[extensionIdx].toLowerCase();
}

function isAllowedToUpload(fileName){ 
    var allowedFiles = ["jpeg","jpg","png","bmp","pdf"];
    if($.inArray(getExtension(fileName), allowedFiles)>-1){
        return true;
    }else{
        return false;
    }
}

 var draghere;

var dropbox;

dropbox = document.getElementById("fileItem");
dropbox.addEventListener("dragenter", dragenter, false);
dropbox.addEventListener("dragover", dragover, false);
dropbox.addEventListener("drop", drop, false);
dropbox.addEventListener("click", oninput, true);

function oninput(){
    document.getElementById("clickk").click();
}


window.addEventListener("dragover",function(e){

  e = e || event;
  e.preventDefault();

},false);
window.addEventListener("drop",function(e){
  e = e || event;
  e.preventDefault();
},false);



function dragenter(e) {
  e.stopPropagation();
  e.preventDefault();
}

function dragover(e) {
  e.stopPropagation();
  e.preventDefault();
}

function drop(e) {
    e.stopPropagation();
    e.preventDefault();
    var filesArray = e.dataTransfer.files;
    sendFile(filesArray,0);
}



function sendFile(fileArray,idx) {
    var uri = "upjobs.php";
    var xhr = new XMLHttpRequest();
    var fd = new FormData();
    if(isAllowedToUpload(fileArray[idx].name)){
    	showUploadingImage(fileArray[idx]);
        xhr.open("POST", uri, true);
        xhr.onreadystatechange = (function(fileArray,idx) {
            return function(){
                if (xhr.readyState == 4 && xhr.status == 200) {
                    // Handle response.
                   // alert(xhr.responseText);
                    if(xhr.responseText=='0')
                    {
                       window.location.href='logx.php';
                    } 
                    var im =xhr.responseText;
                    im = im.trim();
                    im = JSON.parse(im);
                    if(im.value=="-1"){
                        //alert(im.name+" is not valid file");
                        ;
                    }else{   
                    	
                    	    $("#setjobId").val(im.jobid);
                    	    del[loop].show();
                    	    loading.remove();
                    	    ++loop;++idel; ++count; 

                    	
            	    }
                    if((fileArray.length-1) > idx){
                    	   sendFile(fileArray,++idx);
                    }
                }
            };
        })(fileArray,idx);
        fd.append('myFile', fileArray[idx]);
        var jid = $("#setjobId").val();
        fd.append('jid', jid);
        xhr.send(fd);
    }
}
$("#popup #cancel").click(function() {
    $(this).parent().parent().hide();
});

$("#popup #ok").click(function() {
    var n= $('#c_az').val();
    $.ajax({
     type: "POST",
      url: "jobname.php",
      data: { 'name': n }
    }).done(function(data) {
    var elem = document.getElementById("mytext");
    
    window.location.href='template.php?name='+$("#setjobId").val();
    });
});

function sendtemp()
{
	if(count!=0)
	{
		$("#popup").css("display", "block");
		document.getElementById("c_az").value ="job"+$("#setjobId").val();
        
	}
}


/*function progressHandler(event){
_("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
var percent = (event.loaded / event.total) * 100;

_("progressBar").value = Math.round(percent);
_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
}*/

function applySelectionToImg(idx) {
	   img[idx].click(function()
            {
                        var x=  this.attr('x');
                       
                        var y=  this.attr('y')+10;
                        var ty;
                    
                        for(ty=0;ty <count;++ty)
                        {
                            var zz= recs[ty].attr('x')+2
                            var token = index[ty];
                            
                             
                            if(zz==x)
                            {
                                if(img[ty].attr('opacity')==0.2) 
                                {
                                    for(var i = sele.length - 1; i >= 0; i--) {
                                        if(sele[i] === ty) {
                                            sele.splice(i, 1);
                                        }
                                    }
                                    img[ty].attr("opacity", "1")
                                }
                                else
                                {
                                    img[ty].attr("opacity", "0.2");
                                    sele.push(ty);
                                    break;
                            }}
                        }
                     });
	 
}

function showUploadingImage(file){
    var src;
    yas =(50*( ((Math.floor(count/6))*5)+1))+200;
    xas = (150*(count%6))+10;
    paper.canvas.setAttribute("height",yas+400 );
    imgname[loop]=file.name;
    recs[loop]= paper.rect(xas,yas,110,170);
    t[loop] = paper.text(xas+40, yas+160,(file.name));
    del[loop] = paper.image(cirmage,xas+100,yas-10,20,20);
    del[loop].hide();
    loading= paper.image(loader,xas+10,yas+40,70,70);
    loading.attr({ "opacity": 0.4});
    
    
    del[loop].click(function()
         {
        var x=  this.attr('x')-100;
        var y=  this.attr('y')+10;
        var ty;
        for(ty=0;ty <count;++ty)
            {
            var zz= recs[ty].attr('x');
            var token = index[ty];
            if(zz==x)
                {
                shiftx(ty);
                break;
                }
            }
        });
    img[loop] = paper.image("",xas+2,yas+10,95,145);
    if(getExtension(file.name)!="pdf"){
                
        var reader = new FileReader();
        reader.onload = (function (currloop){
            var loopIdx = currloop;
            return  function (e) {
                src= e.target.result;
                imgn[loopIdx] = src;
                img[loopIdx].attr({'src':src});
                loading.toFront();
                applySelectionToImg(loopIdx);
            };
        })(loop);
        reader.readAsDataURL(file);
    }else{
        imgn[loop] = pdfimage;
        img[loop].attr({'src':pdfimage});
        applySelectionToImg(loop);
    }
    
}

   

function deleteselected()
{
	var fr =sele.length;
	for(var w=0;w<fr;++w)
	{
		shiftx(sele[w] - w);
	}
	sele =[];
}

function deleteall()
{
    for(var i=count-1;i>=0;--i)
    shiftx(i);
}

function shiftx(xy)
{
    var ty,hl,xs,ys;
    var jobid=$("#setjobId").val();
    $.ajax({
        url: "deletejob.php",
        type: "POST",
        data: { 'name': imgname[xy],'jobid':jobid }
    });
    for( ty =xy;ty<count-1;++ty)
    {
    
     xs = img[ty].attr('x');
     ys = img[ty].attr('y');
    img[ty].remove();
    t[ty].remove();
    
    
    t[ty] = paper.text(xs+38, ys+150,imgname[ty+1]);
    imgn[ty]=imgn[ty+1];
    imgname[ty]=imgname[ty+1];
    img[ty] = paper.image(imgn[ty+1],xs,ys,95,145);
    img[ty].attr("opacity", "1");
    //img1[ty].toFront();
    
    
    img[ty].click(function()
    {
        var x=  this.attr('x');
        
        var y=  this.attr('y')+10;
        var ty;
        
        for(tg=0;tg <count;++tg)
        {
            var zz= recs[tg].attr('x')+2;
            var token = index[tg];
        	
            if(zz==x)
        	{
                if(img[tg].attr('opacity')==0.2) 
                {
                    for(var i = sele.length - 1; i >= 0; i--) {
                        if(sele[i] === tg) {
                            sele.splice(i, 1);
                        }
                    }
                    img[tg].attr("opacity", "1");
                }
                else
                {
                    img[tg].attr("opacity", "0.4");
                    sele.push(tg);
                    break;
            	}
            }
    	}
     });
    }
    recs[count-1].remove();
    img[count-1].remove();
    del[count-1].remove();
    t[count-1].remove();
    --loop;
    --count;
    var yax =(50*( ((Math.floor(count/6))*5)+1))+200;
    paper.canvas.setAttribute("height",yax+400 );

}

function _(el){
	return document.getElementById(el);
}




//Program a custom submit function for the form
$("#data").change(function(e){

	e.stopPropagation();
    e.preventDefault();
    var filesArray = document.getElementById('clickk').files;
    sendFile(filesArray,0);

    });

</script>
<div class="jobData"><input type="hidden" id="setjobId"
 value="<?php if (isset($_GET['jid'])) {
	echo $_GET['jid'];
}else{
    echo "-1";
}?>" /></div>



</body>
</html>
