<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once ('common_func.php');
include_once('jobclass.php');
checksession();
$allowedExtn = array('jpeg','jpg','png','bmp','pdf');
if(!isset($_SESSION["uid"])){
    echo '0';exit;
}

$myFile = $_FILES['myFile'];
$jid = $_POST['jid'];
$flag = $_POST['flag'];
$uploadname=$_FILES["myFile"]['name'];
$fileExtn = getExtension($uploadname);
if(!in_array($fileExtn, $allowedExtn))
{
    $returnArr = array('msg'=>$_FILES['myFile']['name']."not valid file",'value'=>'-1');
    echo json_encode($returnArr);
    exit;
}
$uid=0;

$p=0;
$uid=$_SESSION["uid"];
$uploadFolder =$jobs_path;

$job = new jobclass;
if($jid=="-1"){
    $p = 1;
}else{
    $p = $job->getnewpageno($jid);
}
$pages=0;
foreach( $conn->dbh->query("SELECT no_pages FROM tbl_job_master where job_id = $jid")  as $row) {
            $pages =$row['no_pages'];
        }
if($pages<$jobpagelimit){
    $f=$job->updatejobs ($uploadname,$uid,$jid,$p,$flag);

    if(!is_dir($uploadFolder.$f))
    {
        mkdir($uploadFolder.$f);
    }
    
    $savePath = $uploadFolder.$f."/".$uploadname;
    move_uploaded_file($_FILES["myFile"]["tmp_name"],$savePath);
    foreach( $conn->dbh->query("SELECT count(*) as 'count' FROM tbl_job_master where usr_id=$uid")  as $row) {$count =$row['count'];}
    $returnArr = array('name'=>$_FILES["myFile"]['name'],'path'=>$savePath, 'jobid'=>$f,'value'=>'1','count'=>$count,'flag'=>$flag);
  
}
else 
$returnArr = array('msg'=>'limit reached','value'=>'-1');
echo json_encode($returnArr);

?>