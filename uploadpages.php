<?php
// continue the session
session_start();
// retrieve session data
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
checksession();
if(!isset($_SESSION["uid"])){
    return false;
}


$pdfflag=0;
$filename = $_FILES["myFile"]['name'];
$allowedExtn = array('jpeg','jpg','png','bmp','pdf');
$fileExtn = getExtension($filename);
if(!in_array($fileExtn, $allowedExtn))
{
    $returnArr = array('name'=>$_FILES["myFile"]['name'],'value'=>'-1');
    echo json_encode($returnArr);
    exit;
}
$uid=0;
$p=0;
$uid=$_SESSION['uid'];
$f=$_POST["formid"];
if($f==0){
    foreach( $conn->dbh->query("SELECT max(form_id) as 'maxform' FROM tbl_image_master where user_id = $uid")  as $row) {
        $g =$row['maxform']+1;
    }
    $formname="template ".$g;
      $jobquery="INSERT INTO tbl_template_details(name) VALUES ('$formname')";
      $sth = $conn->dbh->prepare($jobquery);
      $sth->execute() ;
      $f = $conn->dbh->lastInsertId();
}

foreach( $conn->dbh->query("SELECT MAX(page_no)  FROM tbl_image_master where form_id = $f")  as $row) {
    $p =$row['MAX(page_no)']+1;
}
$arrimages=array();
if($fileExtn == 'pdf'){
    $pdfflag=1;
    $pdffile=$upload_path.$f.'_pdf.'.$fileExtn;
    move_uploaded_file($_FILES["myFile"]["tmp_name"],$pdffile);
    $length=trim(exec("pdfinfo $pdffile | grep Pages: | awk '{print $2}'"));
    $execCmd="convert -quality 00 -density 300x300 -scene $p $pdffile ".$upload_path.$uid."_".$f."_%d.jpg";
    exec($execCmd);
    for($i=0;$i<$length;++$i){
        $imgname=$uid.'_'.$f.'_'.($i+$p).".jpg";
        $upfilename=getfilename($filename);
        array_push($arrimages,array('img'=>$imgname,'page'=>($i+$p),'uploaded'=>$upfilename."_".($i+1).".jpg"));
    }
     
}
else {
    $imgname=$uid.'_'.$f.'_'.$p.".".$fileExtn;
    move_uploaded_file($_FILES["myFile"]["tmp_name"],$upload_path.$imgname);
    array_push($arrimages,array('img'=>$imgname,'page'=>$p,'uploaded'=>$filename));
}
$imagesArr=array();
foreach($arrimages as $images){
    $img=$upload_path.$images["img"];
    list($width, $height, $type, $attr) = getimagesize($img);
    $count=0;
    $query="INSERT INTO tbl_image_master(user_id,form_id,page_no,user_image_name,data_img_name,width,height,active) VALUES (:uid,:f,:p,:filename,:kl,:width,:height,1)";
    $sth = $conn->dbh->prepare($query);
    $sth->execute(array(':uid'=>$uid,':f'=>$f,':p'=>$images["page"],':filename'=>$images["uploaded"],':kl'=>$upload_path.$images["img"],':width'=>$width,':height'=>$height)) or die(var_dump($sth->errorInfo()));
    foreach( $conn->dbh->query("SELECT count(*) as 'count' FROM tbl_template_details where form_id=$f ")  as $row) {
        $count =$row['count'];
    }
    
    array_push($imagesArr,array('dbimg'=>$upload_path.$images["img"],'userimg'=>$images["uploaded"]));
}
$returnArr=array('images'=>$imagesArr,'formid'=>$f,'value'=>1,'ispdf'=>$pdfflag);
echo json_encode($returnArr);
?>