<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
include_once('common_func.php');
checksession();
$userid = $_SESSION["uid"];

foreach( $conn->dbh->query("SELECT * from tbl_user_master where usr_id=$userid" ) as $row)
{
    $name=$row["name"];
    $mobile=$row["mobile"];
    $emailid=$row["email_id"];

}
$query="SELECT amount FROM tbl_client_wallet where user_id=$userid ";
$querydata = $conn->dbh->query($query);
$queryresult = $querydata->fetch(PDO::FETCH_ASSOC);
$amount = $queryresult['amount'];

$countquery="SELECT count(*) cnt FROM tbl_client_wallet_log where user_id=$userid";
$countquerydata = $conn->dbh->query($countquery);
$countqueryresult = $countquerydata->fetch(PDO::FETCH_ASSOC);
$count = $countqueryresult['cnt'];

$limit;
$details=array();
$test=0;
foreach( $conn->dbh->query("SELECT * from tbl_client_wallet_log where user_id=$userid order by tcwl_id desc limit 5" ) as $row)
{
    $test=json_decode($row['trans_details']);$cmt=0;$damt=0;
    if($row['type']=='Credit') $camt=$row["amount"];
    else if($row['type']=='Debit')  $damt=$row["amount"];
    array_push($details,array('credit'=>$camt,'debit'=>$damt,'date'=>$row["rec_add_date"],'time'=>$row["rec_add_time"],'desc'=>$test->mode,'net_balance'=>$row["net_balance"]));
    $limit=$row['tcwl_id'];
}

?>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title>Multiple File upload with PHP</title>
<link type="text/css" rel="stylesheet"
	href="<?php echo auto_version('/css/default.css');?>" />
<script type="text/javascript"
	src="<?php echo auto_version('/js/jquery-1.11.1.min.js');?>"></script>
<script src="js/hdsw.js"></script>
<script src="js/pagination.js"></script>
<script src="js/datetimeformat.js"></script>
</head>

<style>
#fileItem {
	border: 2px dotted #0B85A1;
	width: 400px;
	color: #92AAB0;
	text-align: left;
	vertical-align: middle;
	padding: 10px 10px 10 10px;
	margin-bottom: 10px;
	font-size: 200%;
}

button.clr {
	color: red;
}

#popup {
	z-index: 9;
	opacity: 0.92;
	position: absolute;
	top: 0px;
	left: 0px;
	height: 100%;
	width: 100%;
	background: #F0F0F0;
	display: none;
}

form.pos_fixed {
	width: 400px;
	color: #92AAB0;
	position: fixed;
	top: 30%;
	left: 40%;
	background: #D0D0D0;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
}
</style>

<body style="background-color: #eeeeee;">
	<?php include("jobsheader.php"); ?>
	<div id="container">
		<div class="center">
			<h5>Account</h5>
			<div id="tabs">
				<ul>
					<li><a href="javascript:void(null);" id="tabpro">Profile</a>
					</li>

					<li><a href="javascript:void(null);" id="tabacc">Wallet</a>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<!----end of tabs--->

			<div id="tabscon">
				<div id="profile">
					<div id="details">
						<ul>
							<li><?php echo $name;?>
							</li>

							<li><?php echo $emailid;?>
							</li>

							<li><?php echo $mobile;?>
							</li>

							<li><a onclick="showform();">change password</a>
							</li>
							<li><a onclick="editdetails();">Edit details</a>
							</li>

						</ul>
					</div>



					<div id="passwordpopup">
						<form action="javascript:void(null);" id="askpass"
							onsubmit="return checkPassCorr();">
						<h4>Enter Your Password</h4>
						<ul>
							<li><input type="password" placeholder="Previous password"
								class="inp" id="mypassword" name="name" />
							</li>
						</ul>
						<input id="passok" type="submit" class="formbuttons" value="Done">
						<input id="passcancel" type="reset" class="formbuttons" value="Cancel">
						<br />
						</form>
					</div>
					<div id="popup">
						<form action="javascript:void(null);" id="passchange"
							onsubmit="return changePassfnc();">
							<ul>
								<li><input type="password" placeholder="Previous password"
									class="inp" id="prevpass" name="name" />
								</li>
								<li><input type="password" placeholder="New password"
									class="inp" id="curpass" name="email" />
								</li>
								<li><input type="password" placeholder="Re enter password"
									class="inp" id="repass" name="password" />
								</li>
							</ul>
							<input id="ok" type="submit" class="formbuttons" value="Done">
							<input id="cancel" type="reset" class="formbuttons" value = "Cancel">
							<br />
						</form>
					</div>
					<div id="editdetails">
						<form action="javascript:void(null);" id="detailschange"
							onsubmit="return changeDetailsFnc();">
							<ul>
								<li>Name:<input type="text" placeholder="name" class="inp"
									id="editname" name="name" value="<?php echo $name;?>" />
								</li>

								<li>Mobile:<input type="numeric" placeholder="mobile"
									class="inp" id="editmobile" name="password" value="<?php echo $mobile;?>" />
								</li>
							</ul>
							<input id="detailsok" type="submit" class="formbuttons" value="Done">
							<input id="detailscancel" type="reset" class="formbuttons" value="Cancel">
							<br />
						</form>
					</div>
				</div>
				<!----end of profile--->
				<div id="account">
					<ul>
						<li>Amount Available</li>
                        <li>
							<p><?php echo $amount;?></p>
						</li>
                        <li>History</li>
                        <li>
                        <table style="width:100%" id="historytable">
                            <tr>
                                <th>Date</th>
                                <th>Description</th>       
                                <th>Credit</th>
                                <th>Debit</th>
                                <th>Net balance</th>
                            </tr>
                         </table>
                        </li>
                        <li id="pagelinks"></li>
					</ul>
				</div>
				<!----end of account--->
			</div>
			<!----end of tabscon--->


		</div>
		<!----end of container center--->
	</div>
	<!----end of container--->
	<style>
#popup {
	padding: 20px 20px;
	display: none;
}

#editdetails {
	padding: 20px 20px;
	display: none;
}

#passwordpopup {
	padding: 20px 20px;
	display: none;
}

.formbuttons {
	padding: 10px;
	background-color: #FB8C2D;
	color: white;
	font-size: 14px;
	font-weight: 500;
	border: none;
	cursor: pointer;
	border-radius: 3px;
}

input.invalid {
	border: 2px solid red;
}

input.valid {
	border: 2px solid green;
}
</style>
<script>
var historydetails=<?php echo json_encode($details)?>;
var limit=<?php echo $limit?>;
var pagecount=Math.floor(<?php echo $count?>/5);
var curidx=1;
showhistory(historydetails);
function showhistory(arr){
    for (var i in arr){
        var thisDate = new Date(arr[i].date+"T"+arr[i].time);
        
        $('#historytable').append(' <tr class="historyrow"><td>'+thisDate.format("fullDate")+'</td><td>'+arr[i].desc+'</td><td>'+arr[i].credit+'</td><td>'+arr[i].debit+'</td><td>'+arr[i].net_balance+'</td></tr>');
        }
    }

pagination(pagecount,1);
function createpagelinks(pageidx){
 $('.numlinks').remove();
 $('#pagelinks').append('<a class="numlinks" onclick="genlinks('+(curidx-1)+')">prev</a>');
 for(var idx in pageidx){
  if(pageidx[idx]==0)
	  $('#pagelinks').append('<a class="numlinks" >...,</a>');
  else if(pageidx[idx]==curidx)
$('#pagelinks').append('<a class="numlinks" style="color:red;"  onclick="genlinks('+(pageidx[idx])+')">'+pageidx[idx]+'  </a>');
  else
  $('#pagelinks').append('<a class="numlinks" id="linkno'+pageidx[idx]+'" onclick="genlinks('+(pageidx[idx])+')">'+pageidx[idx]+'  </a>');
	  }
 $('#pagelinks').append('<a class="numlinks" onclick="genlinks('+(curidx+1)+')">next</a>');
}

function showform()
{
    $('#details').hide();
    $('#popup').show();
    $('#passchange')[0].reset();
    
}
function editdetails()
{
    $('#details').hide();
    $('#passwordpopup').show();
    $('#askpass')[0].reset();
    

}
$("#detailscancel").click(function() {
    $(this).parent().hide();
    $('#details').show();
});
function changeDetailsFnc() {
    var name=$('#editname').val();
    var mobile=$('#editmobile').val();
    $.ajax({
      type: "POST",
       url: "changepasswordprocess.php",
       data: { 'name': name,'mobile':mobile }
     });
    $('#editdetails').hide();
    $('#details').show();
    $('#popup').hide();
    return false;
}
$("#cancel").click(function() {
	$('#popup').hide();
    $('#details').show();
});
$("#passcancel").click(function() {
	$('#passwordpopup').hide();
    $('#details').show();
});
function changePassfnc() {
    var oldpass=$('#prevpass').val();
    var curpass=$('#curpass').val();
    $.ajax({
     type: "POST",
      url: "changepasswordprocess.php",
      data: { 'oldpass': oldpass,'newpass':curpass }
    }).done(function(data) {
    if(data==1)
    {
        alert("password changed");
        $(this).parent().parent().hide();
        $('#details').show();
        $('#popup').hide();
    }
        
    else
    {
        alert('incorrect old password');
        $('#details').hide();
        $('#popup').show();
    }
    });
    return false;
}

function checkPassCorr() {
    var pass=$('#mypassword').val();
    $.ajax({
     type: "POST",
      url: "changepasswordprocess.php",
      data: { 'password': pass}
    }).done(function(data) {
    if(data==1)
    {
        $('#editdetails').show();
        $('#passwordpopup').hide();
    }
    else
    {
        alert('incorrect old password');
       
    }
    });
    return false;
}

$('#repass').on('input', function() {
    
   var repass=$('#repass').val();
    var curpass=$('#curpass').val();
    if(repass==curpass)
        {
         $(this).removeClass("invalid").addClass("valid");
         $('#ok').attr("disabled", false);
         $('#ok').css("opacity",1);
        }
    else
        {
        $(this).removeClass("valid").addClass("invalid");
        $('#ok').attr("disabled", true);
        $('#ok').css("opacity", 0.30);
        }
});

$('#curpass').on('input', function() {
    
    var repass=$('#repass').val();
     var curpass=$('#curpass').val();
     if(repass==curpass)
         {
          $('#repass').removeClass("invalid").addClass("valid");
          $('#ok').attr("disabled", false);
          $('#ok').css("opacity",1);
         }
     else
         {
         $('#repass').removeClass("valid").addClass("invalid");
         $('#ok').attr("disabled", true);
         $('#ok').css("opacity", 0.30);
         }
 });

function genlinks(idx) {
	  curidx=idx; 
   $('#linkno'+idx).css('color','red');
	 $.ajax({
	     type: "POST",
	      url: "walletdetails.php",
	      data: { 'id': idx}
	    }).done(function(data) {
	    if(data!=undefined)
	    {
	    	  $('.historyrow').remove();
	    	data=JSON.parse(data);
	   
            $('#nextdetails').hide();
	    	showhistory(data.details);
	    	pagination(pagecount,idx);
	    }
	    else
	    {
	        alert('incorrect old password');
	       
	    }
	    });
 }
</script>
</body>
</html>
