<html>
<link type="text/css" rel="stylesheet" href="css/default.css" />
<style>
#login label{
    display: inline-block;
    width: 100px;
    text-align: right;
}
#login_submit{
    padding-left: 100px;
}
#login div{
    margin-top: 1em;
}

.error{
    display: none;
    margin-left: 10px;
}      
 
.error_show{
    color: red;
    margin-left: 10px;
}
input.invalid, textarea.invalid{
    border: 2px solid red;
}
 
input.valid, textarea.valid{
    border: 2px solid green;
}
</style>
<body>
<div id="smallheader">
<div class="center">
<div class="left">
            <a href="index.php">
                <img src="images/beta.png" alt="11 TechSquare"/>
            </a>
        </div>
        
        <div class="clear"></div>
        </div>
</div>

<form id="login" action="usersignupprocess.php" method="post">
    <!-- Name -->
    <div>
        <label for="login_name">Name:</label>
        <input type="text" id="login_name" name="name"></input>
        <span class="error">This field is required</span>
    </div>
    <!-- Email -->
    <div>
        <label for="login_email">Email:</label>
        <input type="email" id="login_email" name="email"></input>
        <span class="error">A valid email address is required</span>               
    </div>                       
    <!--password -->
    <div>
        <label for="login_password">Password:</label>
        <input type="text" id="login_password" name="password"></input>
        <span class="error">A valid password is required</span>                             
    </div>                       
    <!-- mobile -->
    <div>
        <label for="login_mobile">mobile:</label>
        <input type="text" id="login_mobile" name="mobile"></input>
        <span class="error">This field is required</span>                                              
    </div>                   
    <!-- Submit Button -->
    <div id="login_submit">            
        <button type="submit">Submit</button>
    </div>
</form>
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script>


$(document).ready(function() {

	$('#login_name').on('input', function() {
	    var input=$(this);
	    var is_name=input.val();
	    if(is_name){input.removeClass("invalid").addClass("valid");}
	    else{input.removeClass("valid").addClass("invalid");}
	});
});
$('#login_email').on('input', function() {
    var input=$(this);
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var is_email=re.test(input.val());
    if(is_email)
        {
    	$.ajax({
            type: "POST",
            url: "validateemail.php",
            data: { 'email': input,'type' : "1"'}
          }).done(function(data) {
        	    
        	    if(data==true)
        	    input.removeClass("invalid").addClass("valid");
        	    else{input.removeClass("valid").addClass("invalid");}
        	    });
        
        }
    else{input.removeClass("valid").addClass("invalid");}
});
$('#login_mobile').on('input', function() {
    var input=$(this);
    var is_name=input.val();
    if(is_name){input.removeClass("invalid").addClass("valid");}
    else{input.removeClass("valid").addClass("invalid");}
});
$('#login_password').on('input', function() {
    var input=$(this);
    var is_name=input.val();
    if(is_name){input.removeClass("invalid").addClass("valid");}
    else{input.removeClass("valid").addClass("invalid");}
});
$("#login_submit button").click(function(event){
    var form_data=$("#login").serializeArray();
    var error_free=true;
    for (var input in form_data){
        var element=$("#login_"+form_data[input]['name']);
        var valid=element.hasClass("valid");
        var error_element=$("span", element.parent());
        if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}
        else{error_element.removeClass("error_show").addClass("error");}
    }
    if (!error_free){
        event.preventDefault();
    }
   
});
</script>
</html>
