<?php
include_once('config.php');
include_once 'db_conn.php';
include 'scrypt.php';
include_once('common_func.php');
if(isset($_POST['email']))
{
    
	$emailid= $_POST['email'];
	$password=$_POST['password'];
	$mobile= $_POST['mobile'];
	$name=$_POST['name'];
	$flag=0;
	if(valid_email($emailid) && alpha_dash($name) && min_length($password, 6) && alpha($name))
    $flag=1;
	if($flag==1){
   	    $hashpwd = Password::hash($password);
   	    $salt=createRandom(10);
   	    $skey=$key.$salt;
   	    $today = date("Y-m-d"); 
   	    $text=$password."||".$salt."||".$today;
   	    $activationlink=encode($salt,$text);
   	    $userCreateQry = "INSERT INTO tbl_user_master(email_id,pwd,name,mobile,userlevel,activationlink,rc_add_date,rc_add_time) VALUES (:emailid,:hashpwd,:name,:mobile,0,:activationlink,CURDATE(),CURTIME())";
        $sth = $conn->dbh->prepare($userCreateQry);
        $sth->execute(array(':emailid'=>$emailid,':hashpwd'=>$hashpwd,':name'=>$name,':mobile'=>$mobile,'activationlink'=>$activationlink)) /*or die(var_dump($sth->errorInfo()))*/;
   	    $message= $basePath."validate_user_account.php?email=".$emailid."&t=".$salt."&s=".urlencode($activationlink); 
   	    $subject = "Verify your email";
   	    $body = "Dear ".$name."<br /><br />Thank you for signing up with us. Your username is ".$emailid."
   	    Please proceed to<a href='".$message."'>".$message."</a> to activate your account. <br /><br />
   	    Yours Sincerely<br /><br />
   	    11TechSquare Team";
        sendmail($emailid,0,$body,$subject);
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css"/>
<link type="text/css" rel="stylesheet" href="css/googlefonts.css"/>
<title>11 TechSquare</title>

</head>
<body>
    <div id="background">
    <div id="header2">
        <div class="center">
            <a href="index.php">
                <img src="images/11TechSquare.png" alt="11 Tech Square"/>
            </a>                        
        </div><!---end of header2 center--->
        
        <div class="center">
            <div id="form2">
               
                
                <div class="center">
                
                <form id="login" action="forgotpasswordprocess.php" method="post">
                  <h3 > <?php if(isset($emailid) && $flag==1) echo "Thanks for signing up!
Please check your email and click Activate Account in the message we just sent to ".$emailid;else 
echo "Invalid Selection";
?></h3><br/>
            
                    </form>
                </div>
                <div class="clear"></div>
            </div><!---end of form2--->
        </div><!---end of header2 center--->
    </div><!---end of header2--->
    </div><!---end of background--->
    
  
</body>
<div id="footer">
        <?php include("footer.php"); ?>
    </div><!---end of footer--->
</html>
